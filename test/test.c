#include "test_fw.h"

#define SAF_PLATFORM_NULL
/* #include "../source-to-test.c" */

TESTSUITE

TEST(framework_is_up)
{
  ASSERT_INTEGER_EQUALS(0, 0);
  return 1;
}
ENDTEST(framework_is_up)

TEST(more_than_one_test_per_testsuite)
{
  ASSERT_INTEGER_EQUALS(0, 0);
  return 1;
}
ENDTEST(more_than_one_test_per_testsuite)

ENDTESTSUITE

int
main()
{
  run_testsuite();
}
