#define RUNNING_TESTS

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef int (*test_function_p)(void);

typedef struct {
  char *name;
  test_function_p function;
} Test;

typedef struct _listel {
  void *data;
  struct _listel *next;
} listel;

typedef listel *List;

static void
list_init(List *list);
static void
list_append(List *list, void *data);
static void
list_traverse(List list, void (*walk_function)(listel *));
static int
list_count(List list);

static void
list_init(List *list)
{
  *list = NULL;
}

static void
list_append(List *list, void *data)
{
  listel *next, *cursor;
  next = cursor = *list;
  listel *new_element = malloc(sizeof(listel));
  *new_element = (listel){
    .data = data,
    .next = NULL
  };
  if (next == NULL) {
    *list = new_element;
  } else {
    while (next != NULL) {
      cursor = next;
      next = cursor->next;
    }
    cursor->next = new_element;
  }
}

static void
list_traverse(List list, void (*walk_function)(listel *))
{
  listel *cursor = list;
  while (cursor!=NULL) {
    walk_function(cursor);
    cursor = cursor->next;
  }
}

static int
list_count(List list)
{
  listel *cursor = list;
  int nelem = 0;
  while (cursor != NULL) {
    nelem++;
    cursor = cursor->next;
  }
  return nelem;
}

static void
run_testsuite(void);

static void
register_test(
    List *tests,
    const char *test_name,
    test_function_p test_function);

#define TESTSUITE List tests; void run_testsuite(void) {list_init(&tests);
#define ENDTESTSUITE run_tests(tests);}

#define TEST(function_name) int test_ ## function_name ( void )
#define ENDTEST(function_name) ; \
  register_test(&tests, #function_name , test_ ## function_name);

#define ASSERT(boolean_expression) \
  if (!((boolean_expression) || \
        printf("*** %s ***", #boolean_expression) && 0)) return 0;
#define ASSERT_INTEGER_EQUALS(integer_expression_1, integer_expression_2) \
  if (!(((integer_expression_1) == (integer_expression_2)) || \
        printf( \
          "*** %s != %s [%d != %d] ***", \
          #integer_expression_1, \
          #integer_expression_2, \
          (int32_t) integer_expression_1, \
          (int32_t) integer_expression_2) && 0)) return 0;
#define ASSERT_FLOAT_EQUALS(float_expression_1, float_expression_2) \
  if (!(((float_expression_1) == (float_expression_2)) || \
        printf( \
          "*** %s != %s [%lf != %lf] ***", \
          #float_expression_1, \
          #float_expression_2, \
          (double) float_expression_1, \
          (double) float_expression_2) && 0)) return 0;

static void
register_test(
    List *tests, const char *test_name, test_function_p test_function)
{
  char *name = malloc(strlen(test_name) + 1);
  sprintf(name, "%s", test_name);
  Test *new_test = malloc(sizeof(Test));
  *new_test = (Test) {
    .name = name,
    .function = test_function
  };
  list_append(tests, new_test);
}

static void
run_single_test(listel *el)
{
  Test *test = (Test *)(el->data);
  printf("%s ... ", test->name);
  int result = test->function();
  if (result) {
    puts("\tpassed");
  } else {
    puts("\tfailed");
  }
}

static void
run_tests(List tests)
{
  printf("Running %d tests\n", list_count(tests));
  list_traverse(tests, run_single_test);
}

/* Example:

TESTSUITE

TEST(test_identifier)
{
  [... whatever_code... ]
  ASSERT(boolean_expression);
  ASSERT(boolean_expression);
  return 1;
}
ENDTEST(test_identifier)

...

ENDTESTSUITE

*/
