/*
   A-Mazed

   Copyright (C) 2023  Julio Sangrador-Paton

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
   */

#define MAX_TILE_WIDTH (56)
#define MAX_TILE_HEIGHT (56)

#define MAX_LEVEL_WITH_TORCHES (10)

#define PIECE_SCORING (100)
#define MAP_SCORING (200)
#define AMULET_SCORING (300)
#define RING_SCORING (1000)
#define PETAGOLON_SCORING (5000)

int8_t cx = 0;
int8_t cy = 0;

uint8_t floor_texture[N_AREAS][2 + (MAX_TILE_WIDTH + WALK_SEQUENCE_WIDTH) * (MAX_TILE_HEIGHT + WALK_SEQUENCE_HEIGHT)];
uint8_t wall_texture[2 + MAX_TILE_WIDTH * MAX_TILE_HEIGHT];
uint8_t void_texture[2 + MAX_TILE_WIDTH * MAX_TILE_HEIGHT];

uint8_t (*next_loop)(void) = closeup_loop;

#define THANKS_ANIM_FRAMES (SAF_FPS)
typedef struct {
  uint8_t x;
  uint8_t y;
  uint8_t frames;
} thanks_anim_t;

thanks_anim_t thanks_anim = { 0 };

void
prepare_floor_textures(void) {
  for (uint8_t j = 0; j < N_AREAS; j++) {
    floor_texture[j][0] = maze.tile_width + WALK_SEQUENCE_WIDTH;
    floor_texture[j][1] = maze.tile_height + WALK_SEQUENCE_HEIGHT;
    for (uint16_t i = 0; i < (maze.tile_width + WALK_SEQUENCE_WIDTH) * (maze.tile_height + WALK_SEQUENCE_HEIGHT); i++) {
      floor_texture[j][2 + i] =
        SAF_random() < (248 - j * 16) ? FLOOR_COLOR : FLOOR_HIGHLIGHT_COLOR;
    }
  }
}

void
prepare_wall_texture(void) {
  wall_texture[0] = maze.tile_width;
  wall_texture[1] = maze.tile_height;

  for (uint16_t i = 0; i < maze.tile_width * maze.tile_height; i++) {
    wall_texture[2 + i] =
      SAF_random() < 192 ? SAF_COLOR_BLACK : SAF_COLOR_GRAY_DARK;
  }
}

void
prepare_void_texture(void) {
  void_texture[0] = maze.tile_width;
  void_texture[1] = maze.tile_height;

  for (uint16_t i = 0; i < maze.tile_width * maze.tile_height; i++) {
    void_texture[2 + i] = SAF_random() < 248 ? SAF_COLOR_BLACK : SAF_COLOR_GRAY_DARK;
  }

  void_texture[2 + 0] = void_texture[2 + 1] = void_texture[2 + maze.tile_width - 1] =
    void_texture[2 + maze.tile_width] = void_texture[2 + (maze.tile_height - 1) * maze.tile_width] =
    SAF_COLOR_GRAY_DARK;
}

void
prepare_closeup(void) {
  prepare_floor_textures();
  prepare_wall_texture();
  prepare_void_texture();
}

void
draw_tile_ornaments(int8_t tx, int8_t ty, int8_t ox, int8_t oy) {
  if (maze.info[tx][ty].ornament != ORNAMENT_NOTHING) {
    uint8_t *ornament_image = ornament_images[maze.info[tx][ty].ornament - 1];
    if (
        ((ox + maze.tile_width + ornament_image[0] / 2) < 127)
        && ((oy + maze.tile_height + ornament_image[1] / 2) < 127)
       ) {
      SAF_drawImage1Bit(
          ornament_image,
          ox + (maze.tile_width - ornament_image[0]) / 2,
          oy + (maze.tile_height - ornament_image[1]) / 2,
          ornament_image,
          ornament_type_color[maze.info[tx][ty].ornament],
          ornament_type_color[maze.info[tx][ty].ornament],
          transforms_table[(tx + ty) % 4]);
    }
  }
}

void
draw_tile_items(uint8_t tx, uint8_t ty, int8_t ox, int8_t oy) {
  if (has_stair_down(tx, ty)) {
    SAF_drawImage1Bit(
        stair_down_image,
        ox + (maze.tile_width - stair_down_image[0]) / 2,
        oy + (maze.tile_height - stair_down_image[1]) / 2,
        0,
        FLOOR_COLOR,
        FLOOR_DARK_COLOR,
        transforms_table[maze.stair_down_direction]);
  }
  if (has_stair_up(tx, ty)) {
    uint8_t *image = maze.tile_width < stair_up_image[0] ? scale_up_image : stair_up_image;
    uint8_t *mask =  maze.tile_width < stair_up_image[0] ? scale_up_mask : stair_up_mask;
    SAF_drawImage1Bit(
        image,
        ox + (maze.tile_width - image[0]) / 2,
        oy + (maze.tile_height - image[1]) / 2,
        mask,
        FLOOR_COLOR,
        FLOOR_DARK_COLOR,
        transforms_table[maze.stair_up_direction]);
  }
  if (maze.info[tx][ty].contained_item == ITEM_LEVEL_MAP) {
    SAF_drawImage1Bit(
      level_map_image,
      ox + (maze.tile_width - level_map_image[0]) / 2,
      oy + (maze.tile_height - level_map_image[1]) / 2,
      level_map_mask,
      LEVEL_MAP_FILL_COLOR,
      LEVEL_MAP_BORDER_COLOR,
      SAF_TRANSFORM_NONE);
  }
  if (maze.info[tx][ty].contained_item == ITEM_AMULET) {
    SAF_drawImage1Bit(
      amulet_image,
      ox + (maze.tile_width - amulet_image[0]) / 2,
      oy + (maze.tile_height - amulet_image[1]) / 2,
      amulet_mask,
      AMULET_FILL_COLOR,
      AMULET_BORDER_COLOR,
      SAF_TRANSFORM_NONE);
  }
  if (maze.info[tx][ty].contained_item == ITEM_RING) {
    SAF_drawImage1Bit(
      ring_image,
      ox + (maze.tile_width - ring_image[0]) / 2,
      oy + (maze.tile_height - ring_image[1]) / 2,
      ring_mask,
      RING_FILL_COLOR,
      RING_BORDER_COLOR,
      SAF_TRANSFORM_NONE);
  }
  if (maze.info[tx][ty].contained_item == ITEM_MOSAIC_PIECE) {
    SAF_drawImage1Bit(
      pieces[player.depth],
      ox + (maze.tile_width - pieces[player.depth][0]) / 2,
      oy + (maze.tile_height - pieces[player.depth][1]) / 2,
      0,
      PIECE_FILL_COLOR,
      PIECE_BORDER_COLOR,
      SAF_TRANSFORM_NONE);
  }
  if (maze.info[tx][ty].contained_item == ITEM_DOLMEN) {
    SAF_drawImage1Bit(
      dolmen_image,
      ox + (maze.tile_width - dolmen_image[0]) / 2,
      oy + (maze.tile_height - dolmen_image[1]) / 2,
      dolmen_mask,
      DOLMEN_FILL_COLOR,
      DOLMEN_BORDER_COLOR,
      transforms_table[maze.dolmen_direction]);
  }
  if (maze.info[tx][ty].contained_item == ITEM_CORPSE) {
    direction_t d = (tx + ty) % 4;
    uint8_t img_w = corpse_image[0] * !!moves_table[d].dy + corpse_image[1] * !!moves_table[d].dx;
    uint8_t img_h = corpse_image[1] * !!moves_table[d].dy + corpse_image[0] * !!moves_table[d].dx;
    SAF_drawImage1Bit(
      corpse_image,
      ox + (maze.tile_width - img_w) / 2,
      oy + (maze.tile_height - img_h) / 2,
      corpse_mask,
      CORPSE_FILL_COLOR,
      CORPSE_BORDER_COLOR,
      transforms_table[d]);
  }
}

#define FRAMES_ON_GROUND (SAF_FPS)
#define FRAMES_RECOVERING (SAF_FPS)

void
draw_petagolon(void) {

  int8_t ox = cx + (SAF_SCREEN_WIDTH / 2 - 1) - player.puppet.closeup.x + (petagolon.puppet.x - player.puppet.x) * maze.tile_width;
  int8_t oy = cy + (SAF_SCREEN_HEIGHT / 2 - 1) - player.puppet.closeup.y + (petagolon.puppet.y - player.puppet.y) * maze.tile_height;
  static uint8_t show_left;

  if (petagolon.remaining_frames_on_ground) {
    if (petagolon.remaining_frames_on_ground == FRAMES_RECOVERING + FRAMES_ON_GROUND) {
      SAF_clearScreen(SAF_COLOR_BLACK);
    }
  } else {
    show_left = ((petagolon.puppet.x * (maze.tile_width + 1) + petagolon.puppet.closeup.x) <= ((maze.width * maze.tile_width) / 2));
  }

  SAF_drawImage1Bit(
    footprint_shadow,
    ox + petagolon.puppet.closeup.x - footprint_shadow[0] / 2,
    oy + petagolon.puppet.closeup.y - footprint_shadow[1] / 2,
    footprint_shadow,
    SAF_COLOR_GRAY_DARK,
    SAF_COLOR_GRAY_DARK,
    (show_left ? SAF_TRANSFORM_FLIP : SAF_TRANSFORM_NONE)
      );

  SAF_drawImage1Bit(
    right_leg,
    (-60 * show_left)
      + (1 - show_left) * petagolon.foot_height
      +      show_left  * (12 - petagolon.foot_height)
    + ox + petagolon.puppet.closeup.x - footprint_shadow[0] / 2,
    -10 + oy + petagolon.puppet.closeup.y - footprint_shadow[1] / 2,
    right_leg_mask,
    SAF_COLOR_WHITE,
    SAF_COLOR_BLACK,
    show_left ? SAF_TRANSFORM_FLIP : SAF_TRANSFORM_NONE
      );
}

uint8_t
bumped_into_object(void) {
  uint8_t *object_image;
  object_image =
    has_stair_down(player.puppet.x, player.puppet.y) ? stair_down_image :
      has_stair_up(player.puppet.x, player.puppet.y) ? stair_up_image :
        (maze.info[player.puppet.x][player.puppet.y].contained_item == ITEM_LEVEL_MAP) ? level_map_image :
          (maze.info[player.puppet.x][player.puppet.y].contained_item == ITEM_AMULET) ? amulet_image :
            (maze.info[player.puppet.x][player.puppet.y].contained_item == ITEM_RING) ? ring_image :
              (maze.info[player.puppet.x][player.puppet.y].contained_item == ITEM_MOSAIC_PIECE) ? sacred_mosaic[player.depth] :
                (maze.info[player.puppet.x][player.puppet.y].contained_item == ITEM_DOLMEN) ? dolmen_image :
                  (maze.info[player.puppet.x][player.puppet.y].contained_item == ITEM_CORPSE) ? corpse_image :
                0;
  if (object_image == 0) {
    return 0;
  }

  direction_t object_direction =
    has_stair_down(player.puppet.x, player.puppet.y) ? maze.stair_down_direction :
      has_stair_up(player.puppet.x, player.puppet.y) ? maze.stair_up_direction :
        (object_image == corpse_image) ? (direction_t)((player.puppet.x + player.puppet.y) % 4) :
          DOWN;

  int8_t pdx = walk_sequence[0][!!moves_table[player.puppet.direction].dx];
  int8_t pdy = walk_sequence[0][!!moves_table[player.puppet.direction].dy];

  int8_t odx = object_image[!!moves_table[object_direction].dx];
  int8_t ody = object_image[!!moves_table[object_direction].dy];

  if (odx >= (maze.tile_width - pdx)) {
    odx = maze.tile_width - pdx - 1;
  }

  if (ody >= (maze.tile_height - pdx)) {
    ody = maze.tile_height - pdx - 1;
  }

  return ((player.puppet.closeup.x + pdx / 2) > ((maze.tile_width - odx) / 2))
      && ((player.puppet.closeup.y + pdy / 2) > ((maze.tile_height - odx) / 2))
      && ((player.puppet.closeup.x - pdx / 2) < (maze.tile_width - (maze.tile_width - odx) / 2))
      && ((player.puppet.closeup.y - pdy / 2) < (maze.tile_height - (maze.tile_height - ody) / 2));
}

uint8_t
foe_overlap_other_character(uint8_t foe) {

  int16_t px = player.puppet.x * maze.tile_width + player.puppet.closeup.x;
  int16_t py = player.puppet.y * maze.tile_width + player.puppet.closeup.y;
  int16_t fx = foes[foe].puppet.x * maze.tile_width + foes[foe].puppet.closeup.x;
  int16_t fy = foes[foe].puppet.y * maze.tile_width + foes[foe].puppet.closeup.y;

  int16_t d = ((int16_t)foe_types[foes[foe].type].images[0][0] - 4) * ((int16_t)foe_types[foes[foe].type].images[0][0] - 4) / 4;
  if (foes[foe].puppet.hit_points_left
      && (((px - fx) * (px - fx) + (py - fy) * (py - fy)) < d)
     ) {
    return 1;
  }

  for (uint8_t i = N_WORMS; i < foe; i++) {
    if (
        (foes[i].puppet.hit_points_left > 0) &&
        collides(
          foe_types[foes[foe].type].images[0],
          foes[foe].puppet.x * maze.tile_width + foes[foe].puppet.closeup.x,
          foes[foe].puppet.y * maze.tile_height + foes[foe].puppet.closeup.y,
          foe_types[foes[i].type].images[0],
          foes[i].puppet.x * maze.tile_width + foes[i].puppet.closeup.x,
          foes[i].puppet.y * maze.tile_height + foes[i].puppet.closeup.y)
       ) {
      return 1;
    }
  }

  return 0;
}

#define CONTAINS(x,bit) ((x & (1 << bit)) != 0)

void
move_foe_by(uint8_t i, int8_t dx, int8_t dy) {
  if (foes[i].puppet.closeup.current_image_index < foe_types[foes[i].type].startup_images_count) {
    return;
  }

  int8_t speed = foe_types[foes[i].type].speed;
  int8_t tx = dx < 0 ? -1 : dx > 0 ? 1 : 0;
  int8_t ty = dy < 0 ? -1 : dy > 0 ? 1 : 0;

  /* Horizontal move */
  foes[i].puppet.closeup.x += dx * speed;
  if(
      ((dx < 0) && (foes[i].puppet.closeup.x < 0)) ||
      ((dx > 0) && (foes[i].puppet.closeup.x >= maze.tile_width))
    ) {
    if (is_wall(maze.underlying_structure, foes[i].puppet.x + tx, foes[i].puppet.y/* + dy*/)) {
      foes[i].puppet.closeup.x -= dx * speed;
    } else {
      foes[i].puppet.x += tx;
      foes[i].puppet.closeup.x -= maze.tile_width * tx;
    }
  }
  /* Vertical move */
  foes[i].puppet.closeup.y += dy * speed;
  if(
      ((dy < 0) && (foes[i].puppet.closeup.y < 0)) ||
      ((dy > 0) && (foes[i].puppet.closeup.y >= maze.tile_height))
    ) {
    if (is_wall(maze.underlying_structure, foes[i].puppet.x/* + dx*/, foes[i].puppet.y + ty)) {
      foes[i].puppet.closeup.y -= dy * speed;
    } else {
      foes[i].puppet.y += ty;
      foes[i].puppet.closeup.y -= maze.tile_height * ty;
    }
  }
}

void
move_foe(uint8_t i) {

  if (foes[i].puppet.closeup.current_image_index < foe_types[foes[i].type].startup_images_count) {
    return;
  }
  int16_t ddx =  (player.puppet.x * maze.tile_width  + player.puppet.closeup.x) - (foes[i].puppet.x * maze.tile_width  + foes[i].puppet.closeup.x);
  int16_t ddy =  (player.puppet.y * maze.tile_height + player.puppet.closeup.y) - (foes[i].puppet.y * maze.tile_height + foes[i].puppet.closeup.y);

  foes[i].dx = ddx > 0 ? 1 : ddx < 0 ? -1 : 0;
  foes[i].dy = ddy > 0 ? 1 : ddy < 0 ? -1 : 0;

  foes[i].puppet.direction = foes[i].dy == 1 ? DOWN : foes[i].dx == -1 ? LEFT : foes[i].dy == -1 ? UP : foes[i].dx == 1 ? RIGHT : DOWN;

  move_foe_by(i, foes[i].dx, foes[i].dy);
}

void
move_foes() {

  uint8_t range = calculate_range(maze.tile_width);

  for (uint8_t i = 0; i < N_FOES; i++) {
    for (int8_t yy = -range; yy <= range; yy++) {
      for (int8_t xx = -range; xx <= range; xx++) {
        if ((foes[i].puppet.x == (player.puppet.x + xx)) && (foes[i].puppet.y == (player.puppet.y + yy))) {
          if (
              foes[i].puppet.hit_points_left &&
              !foes[i].frames_resting &&
              ((SAF_frame() % foe_types[foes[i].type].slow_down_factor) == 0)
              ) {
            move_foe(i);
          }
        }
      }
    }
    foes[i].frames_resting -= foes[i].frames_resting > 0;
  }

  /* Mark all foes that collide, they'll undo their move below */
  for (uint8_t i = N_WORMS; i < N_FOES; i++) {
    /* This purposefully marks only some foes as colliding, otherwise
     * they'll all be paralysed */
    foes[i].is_colliding = foe_overlap_other_character(i);
  }

  /* Undo moves of colliding foes */
  for (uint8_t i = N_WORMS; i < N_FOES; i++) {
    if (foes[i].puppet.hit_points_left && foes[i].is_colliding) {

      foes[i].puppet.closeup.x -= foes[i].dx * foe_types[foes[i].type].speed;
      if ((foes[i].puppet.closeup.x < 0) || (foes[i].puppet.closeup.x >= maze.tile_width)) {
        foes[i].puppet.x -= foes[i].dx;
        foes[i].puppet.closeup.x -= -(maze.tile_width) * foes[i].dx - foe_types[foes[i].type].speed * foes[i].dx;
      }

      foes[i].puppet.closeup.y -= foes[i].dy * foe_types[foes[i].type].speed;
      if ((foes[i].puppet.closeup.y < 0) || (foes[i].puppet.closeup.y >= maze.tile_height)) {
        foes[i].puppet.y -= foes[i].dy;
        foes[i].puppet.closeup.y -= -(maze.tile_height) * foes[i].dy - foe_types[foes[i].type].speed * foes[i].dy;
      }
    }
  }
}

void
draw_tile_foe(int8_t ox, int8_t oy, foe_t *foe) {
  uint8_t border_color, fill_color;

  uint8_t *image = foe_types[foe->type].images[foe->puppet.closeup.current_image_index];
  uint8_t *mask = foe_types[foe->type].masks[foe->puppet.closeup.current_image_index];
  uint8_t image_half_width = image[!!moves_table[foe->puppet.direction].dx] / 2;
  uint8_t image_half_height = image[!!moves_table[foe->puppet.direction].dy] / 2;
  int16_t fx = ox + foe->puppet.closeup.x;
  int16_t fy = oy + foe->puppet.closeup.y;
  /* We are more restricted on the positive side, because 0,0 is top left not center */
  if (
      ((fx + image_half_width) >= 126)
      || ((fy + image_half_height) >= 126)
      || ((fx - image_half_width) < -126)
      || ((fy - image_half_height) < -126)
      ) {
    return;
  }
  if (foe->puppet.hit_points_left) {
    border_color = (SAF_frame() % 2) || (foe->puppet.hit_points_left >= (foe_types[foe->type].hit_points / 5)) ? foe_types[foe->type].border_color : SAF_COLOR_RED;
    fill_color = (SAF_frame() % 2) || (foe->puppet.hit_points_left >= (foe_types[foe->type].hit_points / 5)) ? foe_types[foe->type].fill_color : SAF_COLOR_ORANGE;
    border_color = foe->puppet.was_hit ? SAF_COLOR_RED : border_color;
    fill_color = foe->puppet.was_hit ? SAF_COLOR_RED : fill_color;
    foe->puppet.was_hit = 0;
  } else {
    border_color = SAF_COLOR_GRAY;
    fill_color = SAF_COLOR_GRAY_DARK;
  }
  SAF_drawImage1Bit(
      image,
      fx - image_half_width,
      fy - image_half_height,
      mask,
      border_color,
      fill_color,
      transforms_table[foe->puppet.direction]
      );
  if (foe->puppet.hit_points_left) {
    foe->puppet.closeup.current_image_index = foe->puppet.closeup.current_image_index + 1;
    if (foe->puppet.closeup.current_image_index >= (foe_types[foe->type].startup_images_count + foe_types[foe->type].images_count)) {
      foe->puppet.closeup.current_image_index = foe_types[foe->type].startup_images_count;
    }
  }
}

void
draw_tile_foes(uint8_t x, uint8_t y, int8_t ox, int8_t oy) {
  for (uint16_t i = 0; i < N_FOES; i++) {
    if (!(foes[i].puppet.hit_points_left) && (foes[i].puppet.x == x) && (foes[i].puppet.y == y)) {
      draw_tile_foe(ox, oy, &foes[i]);
    }
  }
  for (uint16_t i = 0; i < N_FOES; i++) {
    if ((foes[i].puppet.hit_points_left) && (foes[i].puppet.x == x) && (foes[i].puppet.y == y)) {
      draw_tile_foe(ox, oy, &foes[i]);
    }
  }
}

#undef CONTAINS

void
draw_thanks_animation(void) {
  if (thanks_anim.frames == THANKS_ANIM_FRAMES) {
    SAF_clearScreen(SAF_COLOR_GRAY_DARK);
    SAF_playSound(SOUND_BURIAL);
  }
  int8_t ox = cx + (SAF_SCREEN_WIDTH / 2 - 1) - player.puppet.closeup.x;
  int8_t oy = cy + (SAF_SCREEN_HEIGHT / 2 - 1) - player.puppet.closeup.y;

  uint8_t range = calculate_range(maze.tile_width);

  for (int8_t y = -range; y <= range; y++) {
    for (int8_t x = -range; x <= range; x++) {
      uint8_t tx = player.puppet.x + x;
      uint8_t ty = player.puppet.y + y;
      if ((thanks_anim.x == tx) && (thanks_anim.y == ty)) {
        drawOutlinedCenteredTextTighter(
            "thanks",
            ox + maze.tile_width * x + maze.tile_width / 2 + 5 * SAF_cos(thanks_anim.frames * 10) / 128,
            oy + maze.tile_width * y + maze.tile_height / 2 - 5 * SAF_sin(thanks_anim.frames * 10) / 128,
            SAF_COLOR_BLUE,
            SAF_COLOR_BLUE_DARK
            );
        --thanks_anim.frames;
      };
    }
  }
}

void
draw_closeup_screen(void) {
  SAF_clearScreen(SAF_COLOR_BLACK);
  int8_t ox = cx + (SAF_SCREEN_WIDTH / 2 - 1) - player.puppet.closeup.x;
  int8_t oy = cy + (SAF_SCREEN_HEIGHT / 2 - 1) - player.puppet.closeup.y;
  uint8_t *texture;

  uint8_t range = calculate_range(maze.tile_width);

  /* Walls first, since part of them will be covered after */

  for (int8_t y = -range; y <= range; y++) {
    for (int8_t x = -range; x <= range; x++) {
      if (get_pixel(maze.known_parts, player.puppet.x + x, player.puppet.y + y) == T_WALL) {
        texture = wall_texture;
        SAF_drawImage(
            texture,
            ox + x * maze.tile_width,
            oy + y * maze.tile_height,
            SAF_TRANSFORM_NONE,
            T_SHOW_BACKGROUND);
        /* Torches */
        if (player.depth <= MAX_LEVEL_WITH_TORCHES) {
          SAF_drawPixel(
              ox + x * maze.tile_width + texture[0] / 2 - (player.puppet.x * 8 / maze.tile_width)  * x,
              oy + y * maze.tile_height+ texture[1] / 2 - (player.puppet.y * 8 / maze.tile_height) * y,
              SAF_COLOR_YELLOW);
          SAF_drawPixel(
              ox + x * maze.tile_width + texture[0] / 2  - (player.puppet.x * 8 / maze.tile_width)  * x - 1 + rnd(default_rng) % 3,
              oy + y * maze.tile_height+ texture[1] / 2 - (player.puppet.y * 8 / maze.tile_height) * y - 1 + rnd(default_rng) % 3,
              SAF_COLOR_RED);
        } else if (player.depth == N_LEVELS) {
          SAF_drawPixel(
              ox + x * maze.tile_width + texture[0] / 2,
              oy + y * maze.tile_height+ texture[1] / 2,
              SAF_COLOR_YELLOW);
          SAF_drawPixel(
              ox + x * maze.tile_width + texture[0] / 2 - 1 + rnd(default_rng) % 3,
              oy + y * maze.tile_height+ texture[1] / 2 - 1 + rnd(default_rng) % 3,
              SAF_COLOR_RED);
        }
      }
    }
  }

  /* Then unknown parts and floors */

  for (int8_t y = -range; y <= range; y++) {
    for (int8_t x = -range; x <= range; x++) {
      if (get_pixel(maze.known_parts, player.puppet.x + x, player.puppet.y + y) == T_SHOW_BACKGROUND) {
        SAF_drawRect(
            ox + x * maze.tile_width - WALK_SEQUENCE_WIDTH / 2,
            oy + y * maze.tile_height - WALK_SEQUENCE_HEIGHT / 2,
            maze.tile_width + WALK_SEQUENCE_WIDTH / 2,
            maze.tile_height + WALK_SEQUENCE_HEIGHT / 2,
            SAF_COLOR_BLACK,
            1);
      }
    }
  }

  for (int8_t y = -range; y <= range; y++) {
    for (int8_t x = -range; x <= range; x++) {
      if (
          (get_pixel(maze.known_parts, player.puppet.x + x, player.puppet.y + y) != T_SHOW_BACKGROUND) &&
          (!is_wall(maze.underlying_structure, player.puppet.x + x, player.puppet.y + y) && (!is_void(maze.underlying_structure, player.puppet.x + x, player.puppet.y + y)))
         ) {
        texture = floor_texture[maze.info[player.puppet.x + x][player.puppet.y + y].area];
        SAF_drawImage(
            texture,
            ox + x * maze.tile_width - WALK_SEQUENCE_WIDTH / 2,
            oy + y * maze.tile_height - WALK_SEQUENCE_HEIGHT / 2,
            SAF_TRANSFORM_NONE,
            T_SHOW_BACKGROUND);
      }
    }
  }

  /* ... then the rest */

  for (int8_t y = -range; y <= range; y++) {
    for (int8_t x = -range; x <= range; x++) {
      if (
          (get_pixel(maze.known_parts, player.puppet.x + x, player.puppet.y + y) != T_SHOW_BACKGROUND) &&
          (!is_wall(maze.underlying_structure, player.puppet.x + x, player.puppet.y + y) && (!is_void(maze.underlying_structure, player.puppet.x + x, player.puppet.y + y)))
         ) {
        texture = floor_texture[maze.info[player.puppet.x + x][player.puppet.y + y].area];
        draw_tile_ornaments(
            player.puppet.x + x,
            player.puppet.y + y,
            ox + x * maze.tile_width,
            oy + y * maze.tile_height);
        draw_tile_items(
            player.puppet.x + x,
            player.puppet.y + y,
            ox + x * maze.tile_width,
            oy + y * maze.tile_height);
      }
    }
  }
  /* Voids */
  for (int8_t y = -range; y <= range; y++) {
    for (int8_t x = -range; x <= range; x++) {
      if (get_pixel(maze.known_parts, player.puppet.x + x, player.puppet.y + y) == T_VOID) {
        texture = void_texture;
        SAF_drawImage(
            texture,
            ox + x * maze.tile_width,
            oy + y * maze.tile_height,
            SAF_TRANSFORM_NONE,
            T_SHOW_BACKGROUND);
      }
    }
  }
#ifdef DEBUG
  if (show_debug_overlay) {
#ifdef SHOW_GRID
    for (int8_t y = -range; y <= range; y++) {
      for (int8_t x = -range; x <= range; x++) {
        SAF_drawRect(
            ox + x * maze.tile_width,
            oy + y * maze.tile_height,
            maze.tile_width,
            maze.tile_height,
            SAF_frame() % 2 ?
            SAF_COLOR_GRAY :
            get_pixel(maze.underlying_structure, player.puppet.x + x, player.puppet.y + y),
            0);
      }
    }
#endif
  }
#endif
  for (int8_t y = -range; y <= range; y++) {
    for (int8_t x = -range; x <= range; x++) {
      if (get_pixel(maze.known_parts, player.puppet.x + x, player.puppet.y + y) != T_SHOW_BACKGROUND) {
        draw_tile_foes(
            player.puppet.x + x,
            player.puppet.y + y,
            ox + x * maze.tile_width,
            oy + y * maze.tile_height);
      }
    }
  }
}

void
draw_huge_cobwebs(void) {
  int8_t ox = cx + (SAF_SCREEN_WIDTH / 2 - 1) - player.puppet.closeup.x;
  int8_t oy = cy + (SAF_SCREEN_HEIGHT / 2 - 1) - player.puppet.closeup.y;
  uint8_t range = calculate_range(maze.tile_width);
  /* Finally, cobwebs */
  for (int8_t y = -range; y <= range; y++) {
    for (int8_t x = -range; x <= range; x++) {
      if (get_pixel(maze.known_parts, player.puppet.x + x, player.puppet.y + y) == T_CORRIDOR) {
        if (
            (get_pixel(maze.known_parts, player.puppet.x + x - 1, player.puppet.y + y - 1) == T_WALL)
            && (get_pixel(maze.known_parts, player.puppet.x + x    , player.puppet.y + y - 1) == T_WALL)
            && (get_pixel(maze.known_parts, player.puppet.x + x - 1, player.puppet.y + y    ) == T_WALL)
            ) {
          SAF_drawImage1Bit(
              huge_cobweb_image,
              ox + x * maze.tile_width  - (maze.tile_width  +  WALK_SEQUENCE_WIDTH) / 2 + 12,
              oy + y * maze.tile_height - (maze.tile_height + WALK_SEQUENCE_HEIGHT) / 2 + 12,
              huge_cobweb_image,
              SAF_COLOR_WHITE,
              SAF_COLOR_WHITE,
              SAF_TRANSFORM_NONE);
        }
      }
    }
  }
}

void
draw_closeup_player(void) {

  uint8_t color[5] = {
    SAF_COLOR_WHITE,
    SAF_COLOR_WHITE,
    SAF_COLOR_GRAY,
    SAF_COLOR_GRAY_DARK,
    SAF_COLOR_BLACK,
  };

  int8_t ox = cx + (SAF_SCREEN_WIDTH / 2 - 1);
  int8_t oy = cy + (SAF_SCREEN_HEIGHT / 2 - 1);

  uint8_t *image, *mask;
  uint8_t flame_x, flame_y;

  if (player.is_slashing) {
    image = slash_images[player.is_slashing - 1];
    mask = slash_masks[player.is_slashing - 1];
    if (player.has_torch && is_torch_lit()) {
      flame_x = ox;
      flame_y = oy;
    }
  } else {
    image = walk_sequence[(player.puppet.closeup.current_image_index) % N_WALK_STEPS];
    mask = walk_sequence_mask[(player.puppet.closeup.current_image_index) % N_WALK_STEPS];
    if (player.has_torch && is_torch_lit()) {
      flame_x = ox + !!moves_table[player.puppet.direction].dy *
        (walk_torch_delta_x * moves_table[player.puppet.direction].dy
         - (player.puppet.direction == UP))
        + !!moves_table[player.puppet.direction].dx *
        (walk_torch_delta_y[player.puppet.closeup.current_image_index] * moves_table[player.puppet.direction].dx
         - (player.puppet.direction == LEFT));
      flame_y = oy + !!moves_table[player.puppet.direction].dy *
        (walk_torch_delta_y[player.puppet.closeup.current_image_index] * moves_table[player.puppet.direction].dy
         - (player.puppet.direction == UP))
        - !!moves_table[player.puppet.direction].dx *
        (walk_torch_delta_x * moves_table[player.puppet.direction].dx
         - (player.puppet.direction == LEFT) + 1);
    }
  }
  SAF_drawImage1Bit(
      image,
      ox - image[moves_table[player.puppet.direction].dx == 0 ? 0 : 1] / 2,
      oy - image[moves_table[player.puppet.direction].dy == 0 ? 0 : 1] / 2,
      mask,
      player.puppet.was_hit ? PLAYER_HIT_COLOR : PLAYER_BODY_COLOR,
      PLAYER_EYES_COLOR,
      SAF_TRANSFORM_NONE + player.puppet.direction);
  if (player.has_torch && is_torch_lit()) {
    SAF_drawPixel(flame_x, flame_y, SAF_COLOR_YELLOW);
    SAF_drawPixel(flame_x - 1 + SAF_random() % 3, flame_y - 1 + SAF_random() % 3, SAF_COLOR_RED);
  }
  if (player.is_interrupted) {
    SAF_drawText("!", ox  - 2 + player.is_interrupted, oy - 2 - player.is_interrupted, color[player.is_interrupted - 1], 1);
    player.is_interrupted = (player.is_interrupted + (SAF_frame() % 5 == 0)) % 5;
  }
  if (player.is_slashing) {
    player.is_slashing = (player.is_slashing + 1) % (N_SLASH_IMAGES + 1);
  }
}

void
draw_ceiling_monsters(void) {
  static  int8_t monster_on_the_ceiling = -1;
  static uint8_t current_image_index;
  static direction_t monster_direction;
  static int8_t monster_x;
  static int8_t monster_y;

  if (monster_on_the_ceiling != -1) {
    foe_type_t foe = foe_types[monster_on_the_ceiling];
    SAF_drawImage1Bit(
        foe.images[current_image_index],
        monster_x,
        monster_y,
        foe.masks[current_image_index],
        SAF_COLOR_BLACK,
        SAF_COLOR_BLACK,
        SAF_TRANSFORM_SCALE_4 | transforms_table[monster_direction]);
    monster_x += 2 * 4 * moves_table[monster_direction].dx;
    monster_y += 2 * 4 * moves_table[monster_direction].dy;
    current_image_index = (current_image_index + 1) % foe.images_count;
    if (
        (monster_x < (-4 * foe.images[0][0])) ||
        (monster_x > (SAF_SCREEN_WIDTH)) ||
        (monster_y < (-4 * foe.images[0][1])) ||
        (monster_y > (SAF_SCREEN_HEIGHT))
       ) {
      monster_on_the_ceiling = -1;
    }
  } else {
    if (rnd(default_rng) > 254) {
      monster_on_the_ceiling = rnd(default_rng) % 3 + FOE_BAT;
      monster_direction = rnd(default_rng) % 4;
      current_image_index = rnd(default_rng) % foe_types[monster_on_the_ceiling].images_count;
      int w = 4 * foe_types[monster_on_the_ceiling].images[0][0];
      int h = 4 * foe_types[monster_on_the_ceiling].images[0][1];
      monster_x =
        ((SAF_SCREEN_WIDTH  - w) / 2) - moves_table[monster_direction].dx * ((SAF_SCREEN_WIDTH  - w) / 2 + w) + moves_table[monster_direction].dy * (rnd(default_rng) % ((SAF_SCREEN_WIDTH  - w) / 2));
      monster_y =
        ((SAF_SCREEN_HEIGHT - h) / 2) - moves_table[monster_direction].dy * ((SAF_SCREEN_HEIGHT - h) / 2 + h) + moves_table[monster_direction].dx * (rnd(default_rng) % ((SAF_SCREEN_HEIGHT - h) / 2));
    }
  }
}

void
move_player_by(int8_t dx, int8_t dy) {

  int8_t tx = dx > 0 ? 1 : dx < 0 ? -1 : 0;
  int8_t ty = dy > 0 ? 1 : dy < 0 ? -1 : 0;

  player.puppet.closeup.x += dx;
  player.puppet.closeup.y += dy;

  if (
      ((dx < 0) && (player.puppet.closeup.x < 0)) ||
      ((dx > 0) && (player.puppet.closeup.x >= maze.tile_width)) ||
      ((dy < 0) && (player.puppet.closeup.y < 0)) ||
      ((dy > 0) && (player.puppet.closeup.y >= maze.tile_height))
     ) {
    if (
        is_wall(maze.underlying_structure, player.puppet.x + tx, player.puppet.y + ty)
        || is_void(maze.underlying_structure, player.puppet.x + tx, player.puppet.y + ty)
       ) {
      player.puppet.closeup.x -= dx;
      player.puppet.closeup.y -= dy;
    } else {
      player.puppet.x += tx;
      player.puppet.y += ty;
      player.puppet.closeup.x -= maze.tile_width  * tx;
      player.puppet.closeup.y -= maze.tile_height * ty;
    }
  } else if (bumped_into_object()) {
    if (has_stair_up(player.puppet.x, player.puppet.y) && (maze.stair_up_direction == player.puppet.direction)) {
      next_loop = ascend_loop;
    } else if (has_stair_down(player.puppet.x, player.puppet.y) && (maze.stair_down_direction == player.puppet.direction)) {
      next_loop = descend_loop;
    } else if (maze.info[player.puppet.x][player.puppet.y].contained_item == ITEM_LEVEL_MAP) {
      player.level_maps_carried |= (1 << player.depth);
      increase_score_by(MAP_SCORING);
      go_to_object_found_loop(level_map_image, level_map_mask, LEVEL_MAP_BORDER_COLOR, LEVEL_MAP_FILL_COLOR, "  A MAP! ");
    } else if (maze.info[player.puppet.x][player.puppet.y].contained_item == ITEM_AMULET) {
      player.has_amulet = 1;
      increase_score_by(AMULET_SCORING);
      go_to_object_found_loop(amulet_image, amulet_mask, AMULET_BORDER_COLOR, AMULET_FILL_COLOR, "AN AMULET!");
    } else if (maze.info[player.puppet.x][player.puppet.y].contained_item == ITEM_RING) {
      player.has_ring = 1;
      increase_score_by(RING_SCORING);
      go_to_object_found_loop(ring_image, ring_mask, RING_BORDER_COLOR, RING_FILL_COLOR, " A RING! ");
    } else if (maze.info[player.puppet.x][player.puppet.y].contained_item == ITEM_MOSAIC_PIECE) {
      player.pieces_carried |= (1 << player.depth);
      increase_score_by(PIECE_SCORING);
      go_to_object_found_loop(pieces[player.depth], 0, PIECE_BORDER_COLOR, PIECE_FILL_COLOR, " A PIECE! ");
    } else if (maze.info[player.puppet.x][player.puppet.y].contained_item == ITEM_DOLMEN) {
      transition_to_loop(dolmen_crossed_loop);
    } else if (maze.info[player.puppet.x][player.puppet.y].contained_item == ITEM_CORPSE) {
      if (player.corpses_buried == 0) {
        fade_to(burial_loop);
      } else {
        thanks_anim = (thanks_anim_t){
          .x = player.puppet.x,
          .y = player.puppet.y,
          .frames = THANKS_ANIM_FRAMES,
        };
        bury_corpse(player.puppet.x, player.puppet.y);
      }
    } else { /* Bump against wrong side of stair */
      player.puppet.closeup.x -= dx;
      player.puppet.closeup.y -= dy;
    } 
  }
}

uint8_t previous_directions_map = 0;

void
move_player(void) {
  uint8_t closeup_player_speed = CLOSEUP_PLAYER_SPEED;
  uint8_t directions_map;

  /* Check controls and maybe turn */
  if (!player.is_slashing && SAF_buttonJustPressed(SAF_BUTTON_B)) {
    player.is_slashing = 1;
  }

  directions_map = (!!SAF_buttonPressed(SAF_BUTTON_DOWN) << 0)
                 | (!!SAF_buttonPressed(SAF_BUTTON_LEFT) << 1)
                 | (!!SAF_buttonPressed(SAF_BUTTON_UP) << 2)
                 | (!!SAF_buttonPressed(SAF_BUTTON_RIGHT) << 3);

  if (directions_map != 0) {
    if (directions_map != previous_directions_map) {
      uint8_t overlap = previous_directions_map & directions_map;
      if (!overlap) {
        overlap = directions_map;
      }
      player.puppet.direction = DOWN;
      while ((player.puppet.direction <= RIGHT) && ((overlap & 1) == 0)) {
        player.puppet.direction++;
        overlap = overlap >> 1;
      }
    }
  }

  previous_directions_map  = directions_map;

  for (direction_t d = DOWN; d <= RIGHT; d++) {
    if (directions_map & (1 << d)) {
      /* Advance player */
      player.puppet.closeup.x += closeup_player_speed * moves_table[d].dx;
      player.puppet.closeup.y += closeup_player_speed * moves_table[d].dy;
      /* Account for tile change... */
      if (
          ((d == LEFT) && (player.puppet.closeup.x < 0)) ||
          ((d == RIGHT) && (player.puppet.closeup.x >= maze.tile_width)) ||
          ((d == UP) && (player.puppet.closeup.y < 0)) ||
          ((d == DOWN) && (player.puppet.closeup.y >= maze.tile_height))
         ) {
        if (
            is_wall(maze.underlying_structure, player.puppet.x + moves_table[d].dx, player.puppet.y + moves_table[d].dy)
            || is_void(maze.underlying_structure, player.puppet.x + moves_table[d].dx, player.puppet.y + moves_table[d].dy)
            ) {
          player.puppet.closeup.x -= closeup_player_speed * moves_table[d].dx;
          player.puppet.closeup.y -= closeup_player_speed * moves_table[d].dy;
          directions_map &= ~(1 << d);
        } else {
          player.puppet.x += moves_table[d].dx;
          player.puppet.y += moves_table[d].dy;
          player.puppet.closeup.x -= maze.tile_width  * moves_table[d].dx;
          player.puppet.closeup.y -= maze.tile_height * moves_table[d].dy;
        }
      } else if (bumped_into_object()) {
        if (has_stair_up(player.puppet.x, player.puppet.y) && (maze.stair_up_direction == player.puppet.direction)) {
          next_loop = ascend_loop;
        } else if (has_stair_down(player.puppet.x, player.puppet.y) && (maze.stair_down_direction == player.puppet.direction)) {
          next_loop = descend_loop;
        } else if (maze.info[player.puppet.x][player.puppet.y].contained_item == ITEM_LEVEL_MAP) {
          player.level_maps_carried |= (1 << player.depth);
          increase_score_by(MAP_SCORING);
          go_to_object_found_loop(level_map_image, level_map_mask, LEVEL_MAP_BORDER_COLOR, LEVEL_MAP_FILL_COLOR, "  A MAP! ");
        } else if (maze.info[player.puppet.x][player.puppet.y].contained_item == ITEM_AMULET) {
          player.has_amulet = 1;
          increase_score_by(AMULET_SCORING);
          go_to_object_found_loop(amulet_image, amulet_mask, AMULET_BORDER_COLOR, AMULET_FILL_COLOR, "AN AMULET!");
        } else if (maze.info[player.puppet.x][player.puppet.y].contained_item == ITEM_RING) {
          player.has_ring = 1;
          increase_score_by(RING_SCORING);
          go_to_object_found_loop(ring_image, ring_mask, RING_FILL_COLOR, RING_BORDER_COLOR, " A RING! ");
        } else if (maze.info[player.puppet.x][player.puppet.y].contained_item == ITEM_MOSAIC_PIECE) {
          player.pieces_carried |= (1 << player.depth);
          increase_score_by(PIECE_SCORING);
          go_to_object_found_loop(pieces[player.depth], 0, PIECE_BORDER_COLOR, PIECE_FILL_COLOR, " A PIECE! ");
        } else if (maze.info[player.puppet.x][player.puppet.y].contained_item == ITEM_DOLMEN) {
          transition_to_loop(dolmen_crossed_loop);
        } else if (maze.info[player.puppet.x][player.puppet.y].contained_item == ITEM_CORPSE) {
          if (player.corpses_buried == 0) {
            fade_to(burial_loop);
          } else {
            thanks_anim = (thanks_anim_t){
              .x = player.puppet.x,
                .y = player.puppet.y,
                .frames = THANKS_ANIM_FRAMES,
            };
            bury_corpse(player.puppet.x, player.puppet.y);
          }
        } else { /* Bump against wrong side of stair */
          player.puppet.closeup.x -= closeup_player_speed * moves_table[d].dx;
          player.puppet.closeup.y -= closeup_player_speed * moves_table[d].dy;
          directions_map &= ~(1 << d);
        } 
      }
    }
  }

  if (directions_map) {
    player.puppet.closeup.current_image_index = (player.puppet.closeup.current_image_index + 1) % N_WALK_STEPS;
    if((player.puppet.closeup.current_image_index == 0) || (player.puppet.closeup.current_image_index == 9)) {
      playSound(SOUND_STEP);
    }
    player.frames_still = 0;
  } else {
    player.puppet.closeup.current_image_index = 4;
    player.frames_still += (player.frames_still < 255);
  }

}

void
combat(uint16_t *idle_frames) {
  /* Combat */
  player.puppet.was_hit = 0;
  int16_t px = player.puppet.x * maze.tile_width + player.puppet.closeup.x;
  int16_t py = player.puppet.y * maze.tile_width + player.puppet.closeup.y;
  int8_t head_towards_foe = -1;
  for (uint8_t i = 0; i < N_FOES; i++) {
    int16_t fx = foes[i].puppet.x * maze.tile_width + foes[i].puppet.closeup.x;
    int16_t fy = foes[i].puppet.y * maze.tile_width + foes[i].puppet.closeup.y;
    if (foes[i].puppet.hit_points_left && collides(walk_sequence[0], px, py, foe_types[foes[i].type].images[0], fx, fy)) {

      if (player.is_slashing) {
        foes[i].puppet.hit_points_left--;
        foes[i].puppet.was_hit = 1;
        playSound(SOUND_FOE_WAS_HIT);
        move_foe_by(i, -foes[i].dx, -foes[i].dy);
        move_foe_by(i, -foes[i].dx, -foes[i].dy);
        if (foes[i].puppet.hit_points_left == 0) {
          increase_score_by(foe_types[foes[i].type].scoring);
          remaining_foes[player.depth][foes[i].type]--;
        }
      }
      if (
          ((fx - px) * (fx - px) + (fy - py) * (fy - py)) < (WALK_SEQUENCE_WIDTH * WALK_SEQUENCE_WIDTH / 4)
          && !foes[i].frames_resting
          ) { /* Player hit by the foe */
        foes[i].frames_resting = foe_types[foes[i].type].frames_resting;
        move_player_by(foes[i].dx, foes[i].dy);
        move_player_by(foes[i].dx, foes[i].dy);
        if ((player.puppet.hit_points_left - foe_types[foes[i].type].damage) > 0) {
          player.puppet.hit_points_left -= foe_types[foes[i].type].damage;
        } else {
          player.puppet.hit_points_left = 0;
        }
        player.puppet.was_hit = 1;
        if ((head_towards_foe == -1) || (foes[i].type > foes[head_towards_foe].type)) {
          /* head towards the most powerful of the attacking foes */
          head_towards_foe = i;
          int16_t dx = fx - px;
          int16_t dy = fy - py;
          if (SAF_sqrt(dx * dx) > SAF_sqrt(dy * dy)) {
            player.puppet.direction = (dx > 0) ? RIGHT : LEFT;
          } else {
            player.puppet.direction = (dy > 0) ? DOWN : UP;
          }
        }
        *idle_frames = 0;
        playSound(SOUND_PLAYER_WAS_HIT);
        if (!player.puppet.hit_points_left) {
          next_loop = death_loop;
        }
      }
    }
  }
}

#define FIRST_PART_FRAMES (22)
#define FALL_FRAMES (10 * SAF_FPS)

uint8_t
falling_petagolon_loop(void) {
  int8_t last_x = 32;
  int8_t last_y = 32;
  int8_t x = 32;
  int8_t y = 32;
  static uint8_t theta0 = 0;
  uint8_t k = 5;

  uint8_t walls_color[4] = {
    SAF_COLOR_BLACK,
    SAF_COLOR_GRAY_DARK,
    FLOOR_HIGHLIGHT_COLOR,
    FLOOR_COLOR,
  };
  uint8_t highlights_color[4] = {
    SAF_COLOR_BLACK,
    SAF_COLOR_GRAY_DARK,
    SAF_COLOR_GRAY,
    FLOOR_HIGHLIGHT_COLOR,
  };

  uint8_t feet_color[4] = {
    SAF_COLOR_GRAY_DARK,
    SAF_COLOR_GRAY,
    SAF_COLOR_WHITE,
    SAF_COLOR_WHITE,
  };

  static uint8_t remaining_frames_first_part = FIRST_PART_FRAMES;
  static uint8_t remaining_frames = FALL_FRAMES;

  if (remaining_frames_first_part) {
    cx = -2 + rnd(default_rng) % 5;
    cy = -2 + rnd(default_rng) % 5;
    draw_closeup_screen();
    draw_closeup_player();
    SAF_drawCircle(0 + (FIRST_PART_FRAMES - remaining_frames_first_part), 48 - (FIRST_PART_FRAMES - remaining_frames_first_part), 63 - (FIRST_PART_FRAMES - remaining_frames_first_part), SAF_COLOR_WHITE, 1);
    SAF_drawCircle(0 + (FIRST_PART_FRAMES - remaining_frames_first_part), 48 - (FIRST_PART_FRAMES - remaining_frames_first_part), 63 - (FIRST_PART_FRAMES - remaining_frames_first_part), SAF_COLOR_BLACK, 0);
    remaining_frames_first_part--;
    return 1;
  }

  cx  = cy = 0;

  SAF_clearScreen(walls_color[remaining_frames / 64]);

  for (uint8_t r = 0, theta = theta0; r < k * SAF_sqrt(32 * 32 + 32 * 32); r++, theta++) {
    x = 32 + ((r/k * SAF_cos(theta)) / 128);
    y = 32 + ((r/k * SAF_sin(theta)) / 128);
    SAF_drawLine(last_x, last_y, x, y, highlights_color[remaining_frames / 64]);
    last_x = x;
    last_y = y;
    theta+=2;
  }
  theta0 += 16;
  SAF_drawImage1Bit(
    right_leg,
    32 + (8 * SAF_cos(theta0)) / 128,
    32 + (4 * SAF_sin(theta0)) / 128,
    right_leg_mask,
    feet_color[remaining_frames / 64],
    SAF_COLOR_BLACK,
    SAF_TRANSFORM_NONE
      );
  SAF_drawImage1Bit(
    right_leg,
    -46 - (8 * SAF_cos(theta0)) / 128,
    31 - (4 * SAF_sin(theta0)) / 128,
    right_leg_mask,
    feet_color[remaining_frames / 64],
    SAF_COLOR_BLACK,
    SAF_TRANSFORM_FLIP
      );

  if (remaining_frames < 200) {
    seed_rnd(default_rng, 1);
    for (uint8_t i = 200 - remaining_frames; i > 0; i--) {
      for (uint8_t j = 0; j < 32; j++) {
        SAF_drawPixel(rnd(default_rng) % 64, rnd(default_rng) % 64, SAF_COLOR_BLACK);
      }
    }
  }

  remaining_frames --;
  if (SAF_buttonJustPressed(SAF_BUTTON_A) || (remaining_frames == 0)) {
    remaining_frames = FALL_FRAMES;
    remaining_frames_first_part = FIRST_PART_FRAMES;
    add_objects();
    current_loop = closeup_loop;
  }
  return 1;
}

uint8_t
is_close_to_wall(uint8_t x, uint8_t y) {
  return
    is_wall(maze.underlying_structure, x - 1, y) ||
    is_wall(maze.underlying_structure, x + 1, y) ||
    is_wall(maze.underlying_structure, x, y - 1) ||
    is_wall(maze.underlying_structure, x, y + 1);
}

uint8_t
petagolon_can_destroy_this_tile() {
  /* I need to prevent to be closing access to piece and stair */
  uint8_t next_to_void = 0;
  for (direction_t d = DOWN; d <= RIGHT; d++) {
    if (get_pixel(maze.underlying_structure, petagolon.puppet.x + moves_table[d].dx, petagolon.puppet.y + moves_table[d].dy) == T_VOID) {
      next_to_void = 1;
      break;
    }
  }
  next_to_void = next_to_void || !petagolon.destroyed_tiles;

  return
    next_to_void
    && (maze.info[petagolon.puppet.x][petagolon.puppet.y].contained_item != ITEM_MOSAIC_PIECE)
    && (maze.info[petagolon.puppet.x][petagolon.puppet.y].contained_item != ITEM_ENTRANCE)
    && !is_close_to_wall(petagolon.puppet.x, petagolon.puppet.y);
}

#define CONTAINS(x,bit) ((x & (1 << bit)) != 0)

void
move_petagolon(void) {

  petagolon.remaining_frames_on_ground -= (petagolon.remaining_frames_on_ground > 0);
  if (petagolon.remaining_frames_on_ground > FRAMES_RECOVERING) { /* Quiet on the ground */
    cx = -2 + rnd(default_rng) % 5;
    cy = -2 + rnd(default_rng) % 5;
    petagolon.foot_height = 0;
    return;
  } else if (petagolon.remaining_frames_on_ground > 0) { /* Recovering */
    petagolon.foot_height += (petagolon.foot_height < 12);
    cx = cy = 0;
  } else { /* In the air */
    cx = cy = 0;
#ifdef DEBUG
    if (petagolon.destroyed_tiles > 2) {
#else
    if (petagolon.destroyed_tiles > maze.tile_width * maze.tile_height / 20) {
#endif
      current_loop = falling_petagolon_loop;
      petagolon.is_dead = 1;
      increase_score_by(PETAGOLON_SCORING);
      return;
    }
  }

  uint8_t speed_x = 2;
  uint8_t speed_y = 3;
  const uint8_t skip_every = 2;
  uint8_t directions_map = 0;
  int16_t ddx =  (player.puppet.x * maze.tile_width  + player.puppet.closeup.x) - (petagolon.puppet.x * maze.tile_width  + petagolon.puppet.closeup.x);
  int16_t ddy =  (player.puppet.y * maze.tile_height + player.puppet.closeup.y) - (petagolon.puppet.y * maze.tile_height + petagolon.puppet.closeup.y);

  uint8_t distance = SAF_sqrt(ddx * ddx + ddy * ddy);
#define MIN_FOOT_DISTANCE (footprint_shadow[0] / 2 + 1)
  player.puppet.was_hit = 0;
  if (
      (player.frames_still && (distance < MIN_FOOT_DISTANCE))
      && !petagolon.remaining_frames_on_ground /* not in the middle of a step */
     ) {
      /* Stomp */
      petagolon.remaining_frames_on_ground = FRAMES_RECOVERING + FRAMES_ON_GROUND;
      if (petagolon_can_destroy_this_tile()) {
        petagolon.destroyed_tiles += get_pixel(maze.underlying_structure, petagolon.puppet.x, petagolon.puppet.y) == T_CORRIDOR;
        set_pixel(maze.underlying_structure, petagolon.puppet.x, petagolon.puppet.y, T_VOID);
      }
      uint8_t damage_inflicted = 5 + (MIN_FOOT_DISTANCE - distance);
      if ((player.puppet.hit_points_left - damage_inflicted) > 0) {
        player.puppet.hit_points_left -= damage_inflicted;
        player.puppet.was_hit = 1;
      } else {
        player.puppet.hit_points_left = 0;
        fade_to_loop(death_loop);
        return;
      }
    return;
  }

  if ((SAF_frame() % skip_every) == 0) {
    return;
  }

  int8_t dx = ddx > 0 ? 1 : ddx < 0 ? -1 : 0;
  int8_t dy = ddy > 0 ? 1 : ddy < 0 ? -1 : 0;

  if ((ddx >= 0) && (ddx < speed_x)) {
    speed_x = ddx;
  }

  if ((ddx < 0) && (ddx > -speed_x)) {
    speed_x = -ddx;
  }

  if ((ddy >= 0) && (ddy < speed_y)) {
    speed_y = ddy;
  }

  if ((ddy < 0) && (ddy > -speed_y)) {
    speed_y = -ddy;
  }

  directions_map |= dx < 0 ? (1 << LEFT ) : 0;
  directions_map |= dx > 0 ? (1 << RIGHT) : 0;
  directions_map |= dy < 0 ? (1 << UP   ) : 0;
  directions_map |= dy > 0 ? (1 << DOWN ) : 0;

  petagolon.puppet.closeup.x += speed_x * dx;
  petagolon.puppet.closeup.y += speed_y * dy;

  /* Horizontal move */
  if(
      (CONTAINS(directions_map, LEFT ) && (petagolon.puppet.closeup.x < 0)) ||
      (CONTAINS(directions_map, RIGHT) && (petagolon.puppet.closeup.x > maze.tile_width))
    ) {
    if (is_wall(maze.underlying_structure, petagolon.puppet.x + dx, petagolon.puppet.y + dy)) {
      petagolon.puppet.closeup.x -= speed_x * dx;
    } else {
      petagolon.puppet.x += dx;
      petagolon.puppet.closeup.x -= (maze.tile_width ) * dx;
    }
  }
  /* Vertical move */
  if(
      (CONTAINS(directions_map,    UP) && (petagolon.puppet.closeup.y < 0)) ||
      (CONTAINS(directions_map,  DOWN) && (petagolon.puppet.closeup.y > maze.tile_height))
    ) {
    if (is_wall(maze.underlying_structure, petagolon.puppet.x + dx, petagolon.puppet.y + dy)) {
      petagolon.puppet.closeup.y -= speed_y * dy;
    } else {
      petagolon.puppet.y += dy;
      petagolon.puppet.closeup.y -= (maze.tile_height) * dy;
    }
  }

}

#undef CONTAINS

#define MAX_RING_SIZE (12)
uint8_t
teleport_loop(void) {
  static int8_t i = 1;
  static int8_t d = 0;

  if ((d == 0) && (i == -1)) {
    d = 0;
    i = 1;
    current_loop = closeup_loop;
    return 1;
  }

  draw_closeup_screen();

  int8_t ox = cx + (SAF_SCREEN_WIDTH / 2 - 1);
  int8_t oy = cy + (SAF_SCREEN_HEIGHT / 2 - 1);

  SAF_drawCircle(ox, oy, d, SAF_frame() % 2 ? SAF_COLOR_ORANGE : SAF_COLOR_WHITE, 0);

  d+=i;
  if (d == MAX_RING_SIZE) {
    teleport_player();
    uncover_discovered_places();
    i = -1;
  }

  return 1;
}

void
draw_zzZ(int8_t x, int8_t y) {
  drawOutlinedCenteredTextTighter(
      "z",
      x + 4 + 2 * SAF_cos(SAF_frame() * 4 % 256) / 128,
      y - 8 - 2 * SAF_sin(SAF_frame() * 8 % 256) / 128,
      SAF_COLOR_GRAY,
      SAF_COLOR_GRAY_DARK);
  drawOutlinedCenteredTextTighter(
      "z",
      x + 8 + 2 * SAF_cos((24 + SAF_frame()) * 4 % 256) / 128,
      y - 12 - 2 * SAF_sin((24 + SAF_frame()) * 8 % 256) / 128,
      SAF_COLOR_GRAY,
      SAF_COLOR_GRAY_DARK);
  drawOutlinedCenteredTextTighter(
      "z",
      x + 14 + 2 * SAF_cos((48 + SAF_frame()) * 4 % 256) / 128,
      y - 18 - 2 * SAF_sin((48 + SAF_frame()) * 8 % 256) / 128,
      SAF_COLOR_GRAY,
      SAF_COLOR_GRAY_DARK);
}

void
draw_sleeping_player(void) {
  int8_t ox = cx + (SAF_SCREEN_WIDTH / 2 - 1);
  int8_t oy = cy + (SAF_SCREEN_HEIGHT / 2 - 1);

  SAF_drawImage1Bit(
      walk_sequence[(player.puppet.closeup.current_image_index) % N_WALK_STEPS],
      ox - walk_sequence[0][moves_table[player.puppet.direction].dx == 0 ? 0 : 1] / 2,
      oy - walk_sequence[0][moves_table[player.puppet.direction].dy == 0 ? 0 : 1] / 2,
      walk_sequence_mask[(player.puppet.closeup.current_image_index) % N_WALK_STEPS],
      PLAYER_EYES_COLOR,
      PLAYER_EYES_COLOR,
      SAF_TRANSFORM_NONE + player.puppet.direction);

  draw_zzZ(ox, oy);
}

void
draw_fog(void) {
  #define N_FOG_LINES (20)

  uint8_t is_fog_active = !(player.depth & 1) && (maze.info[player.puppet.x][player.puppet.y].area > 0);

  typedef struct {
    int32_t x1; /* fixed point */
    uint8_t length;
    int8_t y;
    uint8_t speed;
  } fog_line_t;

  static fog_line_t fog_lines[N_FOG_LINES];

  rng_state_t st;

  int32_t px = maze.tile_width * player.puppet.x + player.puppet.closeup.x;

  for (uint8_t i = 0; i < N_FOG_LINES; i++) {
    int32_t fx1 = fog_lines[i].x1 / 256;
    int32_t screen_fx1 = fx1 - px;
    int32_t screen_fx2 = screen_fx1 + fog_lines[i].length;

    if (
           (fog_lines[i].length == 0) /* Never initialized */
        || (screen_fx2 >= 122) /* Too much to the right, out of sight */
        || (screen_fx1 <= -123) /* Too much to the left, out of sight */
       ) {
      if (is_fog_active) {
        if (screen_fx1 <= -123) { /* too much to the left, reappear on the right */
          fog_lines[i].x1 = (px + 64) * 256;
        } else { /* Too much to the right (or uninit'd), reappear on the left */
          fog_lines[i].x1 = (px - 64) * 256;
        }

        fog_lines[i].length = 10 + rnd(default_rng) % 48;

        fog_lines[i].y = -24 + rnd(default_rng) % 48;
        fog_lines[i].speed = (1 << ((4 + rnd(default_rng) % 4)));

        fx1 = fog_lines[i].x1 / 256;
      }
      else {
        fog_lines[i].length = 0;
      }
      screen_fx1 = fx1 - px;
      screen_fx2 = screen_fx1 + fog_lines[i].length;
    }

    seed_rnd(&st, i);
    if (screen_fx2 > screen_fx1) { /* Existing line */
      for (int8_t x = screen_fx1; x < screen_fx2; x++) {
        if (rnd(&st) < 180) {
          SAF_drawPixel(
              cx + x,
              cy + (2ULL * fog_lines[i].y - (player.puppet.y * maze.tile_height + player.puppet.closeup.y)) % 64,
              COLOR332TO8BIT(6, 6, 2));
        }
      }
    }

    fog_lines[i].x1 += fog_lines[i].speed;
  }
}

void
draw_visibility_range(void) {
  if ((player.depth > DEEPEST_LEVEL_WITH_TORCHES) && (player.depth < N_LEVELS)) {
    if (player.has_torch && is_torch_lit()) {
      SAF_drawImage(illumination_image, cx - 2, cy - 2, SAF_TRANSFORM_NONE | (SAF_TRANSFORM_ROTATE_90 * ((SAF_random() % 10) > 6)), SAF_COLOR_WHITE);
    } else {
      SAF_drawImage(darkness_image, cx - 2, cy - 2, SAF_TRANSFORM_NONE, SAF_COLOR_WHITE);
    }
  }
}

#define MAX_IDLE_FRAMES_CLOSEUP (10 * SAF_FPS)

uint8_t
closeup_loop(void) {
  static rng_state_t earthquake_rng = { .rng8_a = 1 };
  static uint16_t idle_frames = 0;
  static uint8_t player_is_sleeping = 0;
  static uint8_t feeling_earthquake = 0;

  SAF_clearScreen(SAF_COLOR_BLACK);

  if (
      !petagolon.is_dead &&
      (player.depth < N_LEVELS) &&
      (player.depth > (N_LEVELS - 3)) &&
      !feeling_earthquake &&
      (rnd(&earthquake_rng) > 254)) {
    feeling_earthquake = SAF_FPS;
  }

  if (feeling_earthquake) {
    cx = -2 + rnd(default_rng) % 5;
    cy = -2 + rnd(default_rng) % 5;
    --feeling_earthquake;
  } else {
    cx = cy = 0;
  }

  if (!anyButtonPressed()) {
    idle_frames += (idle_frames < MAX_IDLE_FRAMES_CLOSEUP);
  } else {
    idle_frames = 0;
  }

  player_is_sleeping = idle_frames >= MAX_IDLE_FRAMES_CLOSEUP;

  /* --- */

  next_loop = closeup_loop;

  if (!player_is_sleeping && (rnd(default_rng) > 247)) { /* ~3% probability */
    add_worm();
  }
  move_player();
  uncover_discovered_places();
  if (player.depth == N_LEVELS) {
    if (!petagolon.is_dead) {
      move_petagolon();
    }
  } else {
    move_foes();
  }
  combat(&idle_frames);
  draw_closeup_screen();
  draw_closeup_player();
  if (player_is_sleeping) {
    player.puppet.hit_points_left += (player.puppet.hit_points_left < PLAYER_HIT_POINTS);
    draw_sleeping_player();
  }
  if (thanks_anim.frames) {
    draw_thanks_animation();
  }
  if (maze.tile_width == 48) {
    draw_huge_cobwebs();
  }
  if (petagolon.is_dead || (player.depth != N_LEVELS)) {
    draw_ceiling_monsters();
  }

  draw_fog();
  draw_visibility_range();

  draw_life_indicator();
  if (idle_frames < MAX_IDLE_FRAMES_CLOSEUP) {
    draw_other_indicators();
    draw_score();
  }

  if (!petagolon.is_dead && (player.depth == N_LEVELS)) {
    draw_petagolon();
  }

  if (next_loop != closeup_loop) {
    previous_directions_map = 0;
    current_loop = next_loop;
  }

  if (SAF_buttonJustPressed(SAF_BUTTON_C)) {
    if (player.has_ring) {
      previous_directions_map = 0;
      current_loop = teleport_loop;
      return 1;
    }
  } else {
    if ((player.depth != 15) && SAF_buttonJustPressed(SAF_BUTTON_A)) {
      previous_directions_map = 0;
      fade_to(overview_loop);
    }
  }

  return 1;
}
