#!/bin/bash

usage() {
  echo
  echo Usage:
  echo
  echo $0 appname executable [arguments]
  echo
}

if [ $# -lt 1 ]; then
  usage
  exit 1
fi

EXE="$1"
shift
ARGUMENTS="$@"

echo
echo Executable: ${EXE}
echo Arguments: ${ARGUMENTS}

if ! [ -x "${EXE}" ]; then
  echo Check your executable: can\'t execute it
  exit 2
fi

APPNAME=$(${EXE} -h | head -n 1 | sed -e 's/^\(.*\) version .*$/\1/')
VERSION=$(${EXE} -h | head -n 1 | sed -e 's/^.* version \(.*\)$/\1/')
EXEDIR=$(cd $(dirname ${EXE}) && pwd)
ABSEXE=$(echo ${EXEDIR}/$(basename ${EXE}))

echo Appname: ${APPNAME}
echo Version: ${VERSION}
echo Executable absolute path: ${ABSEXE}

TESTDIR=test-${VERSION}-$(date +%Y%m%d%H%M%S)

mkdir ${TESTDIR}

(cd $TESTDIR && ${ABSEXE} -s0 -r ${ARGUMENTS})

