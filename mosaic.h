
#define MOSAIC_SCORING (10000)

uint8_t mosaic[4][4];
int8_t hx, hy;
uint8_t is_mosaic_set_up = 0;


uint8_t
inside_puzzle(int8_t x, int8_t y) {
  return (x >= 0)
    && (x <= 3)
    && (y >= 0)
    && (y <= 3);
}

#ifdef DEBUG
#define SCRAMBLING_STEPS (3)
#else
#define SCRAMBLING_STEPS (255)
#endif

void
setup_mosaic(void) {
  direction_t d;

  for (uint8_t y = 0; y < 4; y++) {
    for (uint8_t x = 0; x < 4; x++) {
      mosaic[x][y] = 4 * y + x;
    }
  }
  int8_t x = 3;
  int8_t y = 3;
  int8_t nx, ny;
  for (uint8_t i = 0; i < SCRAMBLING_STEPS; i++) {
    do {
      d = rnd(default_rng) % 4;
      nx = x + moves_table[d].dx;
      ny = y + moves_table[d].dy;
    } while (!inside_puzzle(nx, ny));
    uint8_t aux = mosaic[nx][ny];
    mosaic[nx][ny] = mosaic[x][y];
    mosaic[x][y] = aux;
    x = nx;
    y = ny;
  }
  hx = x;
  hy = y;

  is_mosaic_set_up = 1;
}

void
draw_mosaic(uint8_t solved) {
  uint8_t ox = 1;
  uint8_t oy = 1;

  /* Draw the mosaic */
  for (uint8_t y = 0; y < 4; y++) {
    for (uint8_t x = 0; x < 4; x++) {
      if ((mosaic[x][y] == 15) && !solved) {
        SAF_drawRect(ox + 15 * x + 1, oy + 15 * y + 1, 14, 14, SAF_COLOR_GRAY_DARK, 0);
        SAF_drawRect(ox + 15 * x + 2, oy + 15 * y + 2, 13, 13, SAF_COLOR_GRAY, 1);
      } else {
        SAF_drawImageCompressed(sacred_mosaic[mosaic[x][y]], ox + 15 * x, oy + 15 * y, SAF_TRANSFORM_NONE, SAF_COLOR_BLUE);
      }
    }
  };
  if (!solved) {
    for (uint8_t y = 0; y < 5; y++) {
      SAF_drawLine(ox , oy + 15 * y, ox + 15 * 4, oy + 15 * y, SAF_COLOR_BLACK);
    }
    for (uint8_t x = 0; x < 5; x++) {
      SAF_drawLine(ox + 15 * x, oy, ox + 15 * x, oy + 15 * 4, SAF_COLOR_BLACK);
    }
  }
}

#define CELEBRATION_FRAMES (2 * SAF_FPS)

uint8_t
mosaic_solved_loop(void) {
  static uint8_t celebration_frames = CELEBRATION_FRAMES;

  if ((SAF_frame() % 2) == 0) {
    SAF_clearScreen(SAF_COLOR_WHITE);
  } else {
    draw_mosaic(1);
  }
  if (!--celebration_frames) {
    celebration_frames = CELEBRATION_FRAMES;
    increase_score_by(MOSAIC_SCORING);
    current_loop = success_loop;
  }

  return 1;
}

uint8_t
mosaic_loop(void) {
  static uint8_t is_mosaic_intro_shown = 0;
  static uint8_t intro_size = 2;

  if (!is_mosaic_set_up) {
#ifdef DEBUG
    seed_rnd(default_rng, 1);
#endif
    setup_mosaic();
  }

  if (!is_mosaic_intro_shown) {
    SAF_clearScreen(SAF_COLOR_BLACK);
    for (uint8_t y = 0; y < 4; y++) {
      for (uint8_t x = 0; x < 4; x++) {
        if (mosaic[x][y] == 15) {
          SAF_drawRect(
              31 - (4 * intro_size / 2) + intro_size * x,
              31 - (4 * intro_size / 2) + intro_size * y,
              14,
              14,
              SAF_COLOR_GRAY_DARK,
              0);
          SAF_drawRect(
              31 - (4 * intro_size / 2) + intro_size * x + 1,
              31 - (4 * intro_size / 2) + intro_size * y + 1,
              13,
              13,
              SAF_COLOR_GRAY,
              1);
        } else {
          SAF_drawImageCompressed(
              sacred_mosaic[mosaic[x][y]],
              31 - (4 * intro_size / 2) + intro_size * x,
              31 - (4 * intro_size / 2) + intro_size * y,
              SAF_TRANSFORM_NONE,
              SAF_COLOR_BLUE);
        }
      }
    }
    SAF_drawRect(
        32 + (4 * intro_size / 2),
        0,
        32,
        64,
        SAF_COLOR_BLACK,
        1);
    SAF_drawRect(
        0,
        32 + (4 * intro_size / 2),
        64,
        32,
        SAF_COLOR_BLACK,
        1);

    intro_size++;
    if(intro_size == 15) {
      is_mosaic_intro_shown = 1;
    }
    return 1;
  }

  draw_mosaic(0);

  uint8_t buttons[4] = {
    SAF_BUTTON_DOWN,
    SAF_BUTTON_LEFT,
    SAF_BUTTON_UP,
    SAF_BUTTON_RIGHT,
  };

  /* Check input */
  for (direction_t d = DOWN; d <= RIGHT; d++) {
    int8_t nx = hx - moves_table[d].dx;
    int8_t ny = hy - moves_table[d].dy;
    if (SAF_buttonJustPressed(buttons[d]) && inside_puzzle(nx, ny)) {
      uint8_t aux = mosaic[hx][hy];
      mosaic[hx][hy] = mosaic[nx][ny];
      mosaic[nx][ny] = aux;
      hx = nx;
      hy = ny;
    }
  }

  /* Check result */
  uint8_t solved = 1;
  for (uint8_t y = 0; y < 4; y++) {
    for (uint8_t x = 0; x < 4; x++) {
      if (mosaic[x][y] != 4 * y + x) {
        solved = 0;
      }
    }
  }

  if (
      solved
#ifdef DEBUG
      || SAF_buttonJustPressed(SAF_BUTTON_A)
#endif
     ) {
    is_mosaic_set_up = 0;
    is_mosaic_intro_shown = 0;
    intro_size = 2;
    current_loop = mosaic_solved_loop;
  }
  return 1;
}
