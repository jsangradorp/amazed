#ifdef DEBUG
void
debug_overlay(void) {
#ifdef SHOW_OVERLAY
  static uint32_t last_time = 0;
  uint32_t current_time = SAF_time();

  if (SAF_frame() % 2) {
    char depth[7] = "00(00)";
    depth[0] = '0' + (player.depth / 10) % 10;
    depth[1] = '0' + player.depth % 10;
    depth[3] = '0' + (maze.width / 10) % 10;
    depth[4] = '0' +  maze.width % 10;
    SAF_drawText(depth, 1, 1, SAF_COLOR_RED, 1);

    char islands[6] = "00/00";
    islands[0] = '0' + (maze.actual_islands / 10) % 10;
    islands[1] = '0' + maze.actual_islands % 10;
    islands[3] = '0' + (maze.n_islands / 10) % 10;
    islands[4] = '0' + maze.n_islands % 10;
    SAF_drawText(islands, 1, SAF_SCREEN_HEIGHT - 5, SAF_COLOR_RED, 1);

    float fps = 1.0/(current_time - last_time)*1000.0;
    char fps_string[23];
    SAF_floatToStr(fps, fps_string, 1);
    SAF_drawText(fps_string, 1, 7, SAF_COLOR_RED, 1);

    char tile_size_text[6] = "00x00";
    tile_size_text[0] = '0' + (maze.tile_width / 10) % 10;
    tile_size_text[1] = '0' + maze.tile_width % 10;
    tile_size_text[3] = '0' + (maze.tile_height / 10) % 10;
    tile_size_text[4] = '0' + maze.tile_height % 10;
    SAF_drawText(tile_size_text, 1, 13, SAF_COLOR_RED, 1);

    char coord_text_x[15] = "00.-00";
    coord_text_x[0] = '0' + (player.puppet.x / 10) % 10;
    coord_text_x[1] = '0' + (player.puppet.x /  1) % 10;
    coord_text_x[3] = player.puppet.closeup.x < 0 ? '-' : '+';
    coord_text_x[4] = '0' + (player.puppet.closeup.x / 10) % 10;
    coord_text_x[5] = '0' + (player.puppet.closeup.x /  1) % 10;
    SAF_drawText(coord_text_x, 1, 20, SAF_COLOR_RED, 1);

    char coord_text_y[15] = "00.-00";
    coord_text_y[0] = '0' + (player.puppet.y / 10) % 10;
    coord_text_y[1] = '0' + (player.puppet.y /  1) % 10;
    coord_text_y[3] = player.puppet.closeup.y < 0 ? '-' : '+';
    coord_text_y[4] = '0' + (player.puppet.closeup.y / 10) % 10;
    coord_text_y[5] = '0' + (player.puppet.closeup.y /  1) % 10;
    SAF_drawText(coord_text_y, 1, 26, SAF_COLOR_RED, 1);
  }
  last_time = current_time;
#endif
}

typedef struct {
  uint32_t width;
  uint32_t height;
  uint8_t data[MAX_MAZE_WIDTH * MAX_TILE_WIDTH * MAX_MAZE_HEIGHT * MAX_TILE_HEIGHT];
} canvas_t;

canvas_t canvas_;
canvas_t *canvas = &canvas_;


void
empty_canvas(canvas_t *canvas) {
  for (uint32_t i = 0; i < (canvas->width * canvas->height); i++) {
    canvas->data[i] = 0;
  }
}

void
drawPixelOnCanvas(canvas_t *canvas, int32_t x, int32_t y, uint8_t color) {
  *(canvas->data + canvas->width * y + x) = color;
}

#define __IMAGE_MODE_NORMAL 0
#define __IMAGE_MODE_COMPRESSED 1
#define __IMAGE_MODE_1BIT 2

struct
{
  uint8_t mode;
  const uint8_t *image;
  const uint8_t *binaryMask;
  uint8_t binaryLine;
  uint8_t binaryMaskLine;
  uint8_t binaryPosition;
  uint8_t binaryColor1;
  uint8_t binaryColor2;

  uint8_t transparentColor;
  uint8_t rleCount;
  uint8_t rleLastColor;
  const uint8_t *palette;
} __drawnImage;

uint8_t __getNextImagePixel()
{
  uint8_t result = 0;

  switch (__drawnImage.mode)
  {
    case __IMAGE_MODE_NORMAL:
      result = *__drawnImage.image;
      __drawnImage.image++;
      break;

    case __IMAGE_MODE_1BIT:
      if (__drawnImage.binaryPosition == 0)
      {
        __drawnImage.binaryLine = *__drawnImage.image;
        __drawnImage.image++;
        __drawnImage.binaryPosition = 8;

        if (__drawnImage.binaryMask != 0)
        {
          __drawnImage.binaryMaskLine = ~(*__drawnImage.binaryMask);
          /* We negate the mask because we want 1 to mean transparency; this
          allows to avoid an if-check before mask line shift later on. */

          __drawnImage.binaryMask++;
        }
      }
      
      result = ((__drawnImage.binaryMaskLine & 0x80) == 0) ?
        ( 
          (__drawnImage.binaryLine & 0x80) ? 
          __drawnImage.binaryColor1 : __drawnImage.binaryColor2
        ) : __drawnImage.transparentColor;

      __drawnImage.binaryPosition--;
      __drawnImage.binaryLine <<= 1;
      __drawnImage.binaryMaskLine <<= 1;

      break;

    case __IMAGE_MODE_COMPRESSED:
      if (__drawnImage.rleCount == 0)
      {
        uint8_t b = *__drawnImage.image;
        __drawnImage.rleLastColor = __drawnImage.palette[b & 0x0f];
        __drawnImage.rleCount = (b >> 4) + 1;
        __drawnImage.image++;
      }
 
      result = __drawnImage.rleLastColor;
      __drawnImage.rleCount--;

      break;

    default: break;
  }

  return result;
}

void drawImageGeneralOnCanvas(canvas_t *canvas, int32_t x, int32_t y, uint8_t transform)
{
  int8_t stepX = 1;
  int8_t stepY = 1;

  uint8_t invert = transform & SAF_TRANSFORM_INVERT;

  uint8_t scale = 1;

  switch (transform & 0x18)
  {
    case SAF_TRANSFORM_SCALE_2: scale = 2; break;
    case SAF_TRANSFORM_SCALE_3: scale = 3; break;
    case SAF_TRANSFORM_SCALE_4: scale = 4; break;
    default: break;
  } 

  uint8_t width = *(__drawnImage.image);
  __drawnImage.image++;

  uint8_t h = *(__drawnImage.image);
  __drawnImage.image++;

  if (__drawnImage.mode == __IMAGE_MODE_COMPRESSED)
    __drawnImage.image += 16; // skip the palette

  uint8_t scaledWidth = (width - 1) * scale;
  uint8_t scaledHeight = (h - 1) * scale;

  int32_t *drawX = &x;
  int32_t *drawY = &y;

  switch (transform & 0x07)
  {
    case SAF_TRANSFORM_ROTATE_90:
      drawY = &x; drawX = &y; stepY = -1; x += scaledHeight; break;

    case SAF_TRANSFORM_ROTATE_180:
      stepX = -1; stepY = -1; x += scaledWidth; y += scaledHeight; break;

    case SAF_TRANSFORM_ROTATE_270:
      drawY = &x; drawX = &y; stepX = -1; y += scaledWidth; break;

    case SAF_TRANSFORM_FLIP:
      stepX = -1; x += scaledWidth; break;

    case (SAF_TRANSFORM_ROTATE_90 | SAF_TRANSFORM_FLIP):
      drawY = &x; drawX = &y; stepX = -1; stepY = -1; x += scaledHeight; y += scaledWidth; break;

    case (SAF_TRANSFORM_ROTATE_180 | SAF_TRANSFORM_FLIP):
      stepY = -1; y += scaledHeight; break;

    case (SAF_TRANSFORM_ROTATE_270 | SAF_TRANSFORM_FLIP):
      drawY = &x; drawX = &y; break;
  
    default: break;
  }

  stepX *= scale;
  stepY *= scale;

  int8_t lineBack = -1 * stepX * width;

  while (h > 0)
  {
    uint8_t w = width;

    while (w > 0)
    {
      uint8_t pixel = __getNextImagePixel();

      if (pixel != __drawnImage.transparentColor)
        for (int32_t x2 = x; x2 < x + scale; ++x2)
          for (int32_t y2 = y; y2 < y + scale; ++y2)
            drawPixelOnCanvas(canvas, x2, y2, invert ? ~(pixel) : pixel);

      *drawX += stepX;
      w--;
    }

    *drawX += lineBack;

    h--;
    *drawY += stepY;
  }
}

void drawImageFastOnCanvas(canvas_t *canvas, const uint8_t *image, int32_t x, int32_t y, uint8_t flip,
  uint8_t transparentColor, uint8_t is1Bit, const uint8_t *mask, uint8_t color1,
  uint8_t color2)
{
  uint8_t width = *image;
  image++;

  uint8_t height = *image;
  image ++;

  int32_t x0 = x;
  int32_t xPlus = 1;

  if (flip)
  {
    x0 = x + width - 1;
    xPlus = -1;
  }

  #define loopStart\
    while (height != 0) {\
      x = x0;\
      for (uint8_t w = width; w != 0; --w, x += xPlus) {

  #define loopEnd\
    }\
    height--;\
    y++; }

   if (is1Bit)
   {
     uint8_t bitCount = 0, imgLine = 0, maskLine = 0;

     mask += mask != 0 ? 2 : 0; // skip width and height

     loopStart
       if (bitCount == 0)
       {
         imgLine = *image;
         bitCount = 8;
         image++;

         if (mask != 0)
         {
           maskLine = ~(*mask); // negation helps avoid branching later
           mask++;
         }
       }

       if ((maskLine & 0x80) == 0)
         drawPixelOnCanvas(canvas, x,y,(imgLine & 0x80) ? color1 : color2);

       bitCount--;
       imgLine <<= 1;
       maskLine <<= 1;
     loopEnd
   }
   else
   {
     loopStart
        uint8_t color = *image;

        if (color != transparentColor)
          drawPixelOnCanvas(canvas, x,y,*image);

        image++;
     loopEnd
   }

  #undef loopStart
  #undef loopEnd
}

void drawImageOnCanvas(canvas_t *canvas, const uint8_t *image, int32_t x, int32_t y, uint8_t transform,
  uint8_t transparentColor)
{
  if ((transform & ~SAF_TRANSFORM_FLIP) == 0)
  {
    drawImageFastOnCanvas(canvas, image,x,y,transform & SAF_TRANSFORM_FLIP,
      transparentColor,0,0,0,0);
    return;
  }

  __drawnImage.image = image;
  __drawnImage.mode = __IMAGE_MODE_NORMAL;
  __drawnImage.transparentColor = transparentColor;
  drawImageGeneralOnCanvas(canvas, x,y,transform);
}

void drawImageCompressedOnCanvas(canvas_t *canvas, const uint8_t *image, int32_t x, int32_t y,
  uint8_t transform, uint8_t transparentColor)
{
  __drawnImage.image = image;
  __drawnImage.mode = __IMAGE_MODE_COMPRESSED;
  __drawnImage.transparentColor = transparentColor;
  __drawnImage.rleCount = 0;
  __drawnImage.rleLastColor = 0;
  __drawnImage.palette = image + 2;
  drawImageGeneralOnCanvas(canvas, x,y,transform);
}

void drawImage1BitOnCanvas(canvas_t *canvas, const uint8_t *image, int32_t x, int32_t y,
  const uint8_t *mask, uint8_t color1, uint8_t color2, uint8_t transform)
{
  if ((transform & ~SAF_TRANSFORM_FLIP) == 0)
  {
    drawImageFastOnCanvas(canvas, image,x,y,transform & SAF_TRANSFORM_FLIP,0,1,mask,color1,
      color2);
    return;
  }

  __drawnImage.image = image;
  __drawnImage.binaryMask = (mask != 0) ? mask + 2 : 0; // skip width/height
  __drawnImage.binaryLine = 0;
  __drawnImage.binaryMaskLine = 0; // 0 will be negated
  __drawnImage.binaryPosition = 0;

  for (int i = 0; i < 3; ++i)
    if (i != color1 && i != color2)
    {
      __drawnImage.transparentColor = i;
      break;
    }

  __drawnImage.mode = __IMAGE_MODE_1BIT;
  __drawnImage.binaryColor1 = color1;
  __drawnImage.binaryColor2 = color2;
  drawImageGeneralOnCanvas(canvas, x,y,transform);
}

void
drawImageOnCanvasNaively(canvas_t *canvas, uint8_t *image, uint32_t cx, uint32_t cy) {
  uint32_t image_width = image[0];
  uint32_t image_height = image[1];
  uint8_t *image_data = &image[2];

  for (uint32_t y = 0; y < image_height; y++) {
    for (uint32_t x = 0; x < image_width; x++) {
      *(canvas->data + canvas->width * (cy + y) + cx + x) = *(image_data + image_width * y + x);
    }
  }
}

uint8_t
dump_ppm_file(char *filename, canvas_t *canvas) {
  FILE *f;
  if (!(f = fopen(filename, "w"))) {
    return 0;
  }

  uint8_t r, g, b;

  fprintf(f, "P3\n%u %u\n255\n\n", canvas->width, canvas->height);
  for (uint32_t y = 0; y < canvas->height; y++) {
    for (uint32_t x = 0; x < canvas->width; x++) {
      SAF_colorToRGB(*(canvas->data + canvas->width * y + x), &r, &g, &b);
      fprintf(f, "%u %u %u\n", r, g, b);
    }
    fprintf(f, "\n");
  }
  fclose(f);
  return 1;
}

uint8_t
find_tile(item_t item, uint8_t *ix, uint8_t *iy) {
  for (uint8_t y = 0; y < maze.height; y++) {
    for (uint8_t x = 0; x < maze.width; x++) {
      if(maze.info[x][y].contained_item == item) {
        *ix = x;
        *iy = y;
        return 1;
      }
    }
  }
  return 0;
}

uint8_t
dump_levels_images(void) {
  static uint8_t done = 0;
  if(done) {
    return 0;
  }
  init_foe_types();
  populate_all_levels();
  for(uint8_t level = 1; level < 15; level++) {
    player.depth = level;
    prepare_maze();
    prepare_closeup();
    canvas->width = maze.width * maze.tile_width;
    canvas->height = maze.height * maze.tile_height;
    empty_canvas(canvas);
    printf("Level %02u: %ux%u\n", level, canvas->width, canvas->height);
    fflush(NULL);
    for(uint8_t y = 0; y < maze.height; y++) {
      for(uint8_t x = 0; x < maze.width; x++) {
         if (is_wall(maze.underlying_structure, x, y)) {
           drawImageOnCanvas(canvas, wall_texture, maze.tile_width * x, maze.tile_height * y, SAF_TRANSFORM_NONE, SAF_COLOR_BLACK);
         } else if (is_void(maze.underlying_structure, x, y)) {
           drawImageOnCanvas(canvas, void_texture, maze.tile_width * x, maze.tile_height * y, SAF_TRANSFORM_NONE, SAF_COLOR_BLACK);
         }
      }
    }
    for(uint8_t y = 0; y < maze.height; y++) {
      for(uint8_t x = 0; x < maze.width; x++) {
        uint8_t tile_type = get_pixel(maze.underlying_structure, x, y);
        if (
            (tile_type != T_WALL)
            && (tile_type != T_VOID)
            && (tile_type != T_SHOW_BACKGROUND)
           ) {
           drawImageOnCanvas(canvas, floor_texture[maze.info[x][y].area], maze.tile_width * x - WALK_SEQUENCE_WIDTH / 2, maze.tile_height * y - WALK_SEQUENCE_HEIGHT / 2, SAF_TRANSFORM_NONE, SAF_COLOR_BLACK);
         }
      }
    }
    for(uint8_t y = 0; y < maze.height; y++) {
      for(uint8_t x = 0; x < maze.width; x++) {
           if (maze.info[x][y].ornament != ORNAMENT_NOTHING) {
             uint8_t *ornament_image = ornament_images[maze.info[x][y].ornament - 1];
             drawImage1BitOnCanvas(
                 canvas,
                 ornament_image,
                 maze.tile_width * x + (maze.tile_width - ornament_image[0]) / 2,
                 maze.tile_height *y + (maze.tile_height -ornament_image[1]) / 2,
                 ornament_image,
                 ornament_type_color[maze.info[x][y].ornament],
                 ornament_type_color[maze.info[x][y].ornament],
                 transforms_table[(x + y) % 4]);
           }
      }
    }
    drawImage1BitOnCanvas(canvas, stair_down_image, maze.xf * maze.tile_width + (maze.tile_width - stair_down_image[0]) / 2, maze.yf * maze.tile_height + (maze.tile_height - stair_down_image[1]) / 2, 0, FLOOR_COLOR, FLOOR_DARK_COLOR, maze.stair_down_direction);
    drawImage1BitOnCanvas(canvas, stair_up_image, maze.xs * maze.tile_width + (maze.tile_width - stair_up_image[0]) / 2, maze.ys * maze.tile_height + (maze.tile_height - stair_up_image[1]) / 2, 0, FLOOR_COLOR, FLOOR_DARK_COLOR, maze.stair_up_direction);

    uint8_t ix, iy;

    if (find_tile(ITEM_LEVEL_MAP, &ix, &iy)) {
      drawImage1BitOnCanvas(canvas, level_map_image, ix * maze.tile_width + (maze.tile_width - level_map_image[0]) / 2, iy * maze.tile_height + (maze.tile_height - level_map_image[1]) / 2, level_map_mask, LEVEL_MAP_FILL_COLOR, LEVEL_MAP_BORDER_COLOR, SAF_TRANSFORM_NONE);
    }

    if (find_tile(ITEM_AMULET, &ix, &iy)) {
      drawImage1BitOnCanvas(canvas, amulet_image, ix * maze.tile_width + (maze.tile_width - amulet_image[0]) / 2, iy * maze.tile_height + (maze.tile_height - amulet_image[1]) / 2, amulet_mask, AMULET_FILL_COLOR, AMULET_BORDER_COLOR, SAF_TRANSFORM_NONE);
    }

    if (find_tile(ITEM_MOSAIC_PIECE, &ix, &iy)) {
      drawImage1BitOnCanvas(canvas, pieces[level], ix * maze.tile_width + (maze.tile_width - pieces[level][0]) / 2, iy * maze.tile_height + (maze.tile_height - pieces[level][1]) / 2, 0, PIECE_FILL_COLOR, PIECE_BORDER_COLOR, SAF_TRANSFORM_NONE);
    }

    if (find_tile(ITEM_RING, &ix, &iy)) {
      drawImage1BitOnCanvas(canvas, ring_image, ix * maze.tile_width + (maze.tile_width - ring_image[0]) / 2, iy * maze.tile_height + (maze.tile_height - ring_image[1]) / 2, ring_mask, RING_FILL_COLOR, RING_BORDER_COLOR, SAF_TRANSFORM_NONE);
    }

    if (find_tile(ITEM_DOLMEN, &ix, &iy)) {
      drawImage1BitOnCanvas(canvas, dolmen_image, ix * maze.tile_width + (maze.tile_width - dolmen_image[0]) / 2, iy * maze.tile_height + (maze.tile_height - dolmen_image[1]) / 2, dolmen_mask, DOLMEN_FILL_COLOR, DOLMEN_BORDER_COLOR, SAF_TRANSFORM_NONE);
    }

    for (uint8_t foe = N_WORMS; foe < N_FOES; foe++) {
      if (foes[foe].puppet.hit_points_left) {
        foe_type_t foe_type = foe_types[foes[foe].type];
        drawImage1BitOnCanvas(canvas, foe_type.images[0], foes[foe].puppet.x * maze.tile_width + foes[foe].puppet.closeup.x - foe_type.images[0][0] / 2, foes[foe].puppet.y * maze.tile_height + foes[foe].puppet.closeup.y - foe_type.images[0][1] / 2, foe_type.masks[0], foe_type.border_color, foe_type.fill_color, foes[foe].puppet.direction);
      }
    }

    char filename[11];
    sprintf(filename, "dump%02u.ppm", level);
    dump_ppm_file(filename, canvas);
  }
  done = 1;
  return 0;
}

uint8_t
dump_level_info_loop(void) {
  static int done = 0;
  uint8_t range;
  current_loop = descend_loop;
  if(done)return 1;

  populate_all_levels();

  printf("Lvl\tmsize\ttsize\trng\tbats\tants\tspdrs\tscrps\torcs\tpc.\tmap\tamlt\tring\tdlmn\n");

  for (player.depth = 1; player.depth <= N_LEVELS; player.depth++) {
    maze.width = MAX_MAZE_WIDTH - (player.depth - 1) * ((MAX_MAZE_WIDTH - MIN_MAZE_WIDTH) / (N_LEVELS - 1));
    maze.height = MAX_MAZE_HEIGHT - (player.depth - 1) * ((MAX_MAZE_HEIGHT - MIN_MAZE_HEIGHT) / (N_LEVELS - 1));

    maze.tile_width = maze.tile_height = (player.depth == N_LEVELS) ? 1 * WALK_SEQUENCE_WIDTH + 2 : (6 - 2 * ((player.depth - 1)/ 5)) * WALK_SEQUENCE_WIDTH;

    range =  (SAF_SCREEN_WIDTH / maze.tile_width) / 2 + 1;

    printf(
        "%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%s\t%s\t%s\t%s\t%s\n",
        player.depth,
        maze.width,
        maze.tile_width,
        range,
        remaining_foes[player.depth][FOE_BAT],
        remaining_foes[player.depth][FOE_GIANT_ANT],
        remaining_foes[player.depth][FOE_GIANT_SPIDER],
        remaining_foes[player.depth][FOE_GIANT_SCORPION],
        remaining_foes[player.depth][FOE_ORC],
        "Y",
        player.depth <= DEEPEST_LEVEL_WITH_MAP ? "Y" : "",
        player.depth == AMULET_LEVEL ? "Y" : "",
        player.depth == RING_LEVEL ? "Y" : "",
        player.depth != N_LEVELS ? "Y" : ""
        );
  }
  printf("Done\n");
  //done = 1;
  return 1;
}
#endif
