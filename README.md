# A-Mazed!

  There is little left of the bygone wisdom scraped in the forgotten parchments
of yesteryear:

  "... find pleasure in dismembering unlucky villagers for the pure fun of
it, then playing with the pieces about the tunnels..."

  "... the misfortuned victims' souls never reach peace as their remains are
left to the scavengers..."

  "... in their underground lairs; they also build magical structures said to
be able to contain and repeat Time Itself once and again, which they use to
factually relive their misdeeds..."

  The following piece of lore has been reconstructed from the remaining pieces
of the lost scroll of ancient times:

  "Legend says that a giant petagolon came to the village, offended by the
lack of respect shown by children and adults alike, and stole the sacred mosaic
of the oracle, the most cherished artifact of the Antiquity.

  The monster carried the relic to the depths of its lair, smashing it on the
walls on its way down, breaking it down into pieces and scattering them all
around down the hideous caves.  The run was strenuous and wore the behemoth out
to the point of exhaustion, making it fall to a long, repairing sleep, leaving
the care of the fifteen levels of filthy tunnels, traps and dead ends, as long
forgotten scripts used to talk about, to disgusting giant insect servants,
as well as perverted shadows of some who once were villagers, and other pests of
their breed.

  Since then, without the protection of the oracle devices, the village started
a period of calamities and famine, followed by massive diseases and disgrace,
leaving just a shadow of what life used to be."

  In the chilly nights of the village, it is whispered that the creature is
still sleeping its long sleep, laying down amongst the fragments of the stolen
holy item, and one day a wandering pilgrim will come from afar to hunt down the
petagolon and recover the pieces of the lost sacred mosaic of the oracle, thus
making its restoration a reality and bringing joy and prosperity back to the
village.

  Today, such a character has arrived in the village and is now heading towards
the cursed place, holding but a staff in hand and the determination to explore
the greater depths of the tunnels, kill the cause of so much misfortune, gather
the pieces of mosaic making it possible to restore it, and bring the village
back to its former welfare.

  The villagers are raising all their prayers to their deities, to beg their
helping the pilgrim hero attain success and write a new, colourful and cheerful
page in their history... and they won't consent to a failed returning
messenger.

---

## Features

### Gameplay

- Round trip to the depths of the earth and back
- 2 main game views: overview and close up
- Final puzzle
- Engaging, coherent story
- Increasing difficulty
- 5 types of enemies
- Different kinds of items to find, with different uses
- Final (or not?) BOSS
- Magical ancient structures
- A mission to accomplish
- Multitude of enemies to combat
- Save/load progress
- Teleportation
- Magical monster and object detection
- Magical mapping
- Intuitive controls: only 2 buttons needed

### Art

- 15 large procedurally generated levels
- Hand-made pixel art
  - Sprites
  - Cut scenes
  - Procedurally generated textures
  - 4 types of areas
  - Animated characters
  - Ornamental scenery elements
- Smooth animated transitions
- Flashy signalling of important events
- Colourful state indicators
- Final successful ending scene
- Numerous animated cut scenes

### Technology

- Multiple RNGs
- Everything that SAF gives you
  - Multiplatform
  - 256 colors RGB 332 colorspace
  - 64x64 screen
  - ...
- Offscreen drawing
- Simplistic collision detection
- Pure C99 code
- Range-based monster activation
- Tile based game land
- Sensible orientation of elements for the top down action
- Ordered melee of enemies (they get in the way of each other)
- Precompiled for Linux and for the web: play online!
- GPL3 license (source [here](https://gitlab.com/jsangradorp/amazed/))

 ... and lots more!

## Thanks

I'd like to thank [Drummyfish](http://www.tastyfish.cz/) for creating and
liberating [SAF](https://codeberg.org/drummyfish/SAF). It has brought the joy
of programming back to me. My friend
[Iván Velamazán](http://ivelamazan.info/) has provided beta-testing for the
game, and Román Rodríguez has contributed suggestions too. I would also like
to thank [CNLohr](http://youtube.com/cnlohr) for
[rawdrawandroid](https://github.com/cnlohr/rawdrawandroid) and allowing the
world use it, and in turn the team producing
[rawdraw](https://github.com/cntools/rawdraw) for the same reasons. This list
wouldn't be complete without mentioning
[Edward Rosten](http://www.edwardrosten.com/), for allowing the use of his
[Random Number Generator](https://github.com/edrosten/8bit_rng) code.

In general, to everyone who believes in sharing and making this a better world.
I know I am letting many out of the list and I plan to update it as I remember
you. If you think you have contributed to this project, you are probably right,
so let me know and I will add you.

## Bugs

Hundreds, probably. And besides bugs, there are copy/paste's, inefficient
algorithms, naive implementations and trial-and-error solutions all around, but
it's been thanks to those that I have finally got this to a presentable state.
Maybe you want to improve the code or the game, or fix some bug yourself? Be my
guest, that's why I am making it [Free Software](https://fsf.org).

---

You can play online [here](https://jsangradorp.gitlab.io/amazed/).

A-Mazed was programmed using the fantastic SAF library, which allows me to
have the same code compile and run in multiple environments.

Visit SAF [here](https://codeberg.org/drummyfish/SAF).
