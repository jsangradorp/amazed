#define SOUND_STEP (SAF_SOUND_BEEP)
#define SOUND_PETAGOLON_STOMP (SAF_SOUND_BUMP)
#define SOUND_PLAYER_WAS_HIT (SAF_SOUND_BOOM)
#define SOUND_FOE_WAS_HIT (SAF_SOUND_CLICK)
#define SOUND_BURIAL (SAF_SOUND_BUMP)

typedef enum { DOWN = 0, LEFT, UP, RIGHT } direction_t;

typedef struct {
  int8_t x;
  int8_t y;
  uint8_t current_image_index;
} closeup_t;

typedef struct {
  int8_t x;
  int8_t y;
  direction_t direction;
  closeup_t closeup;
  uint8_t was_hit;
  uint8_t hit_points_left;
} puppet_t;

typedef struct {
  uint8_t just_teleported;

  uint16_t score;
  uint8_t depth;
  uint8_t wants_level_map;
  uint8_t has_amulet;
  uint8_t has_ring;
  uint8_t has_torch;
  uint8_t is_slashing;
  uint8_t is_interrupted;
  uint16_t pieces_carried;
  uint16_t level_maps_carried;
  uint8_t corpses_buried;
  uint8_t frames_still;
  puppet_t puppet;
} player_t;

player_t player;
uint16_t highest_score;

struct petagolon {
  puppet_t puppet;
  uint8_t remaining_frames_on_ground;
  uint8_t destroyed_tiles;
  uint8_t foot_height;
  uint8_t is_dead;
};

struct petagolon petagolon;

typedef struct {
  int8_t dx;
  int8_t dy;
} move_t;

move_t moves_table[4] = {
  { .dx =  0, .dy =  1, },
  { .dx = -1, .dy =  0, },
  { .dx =  0, .dy = -1, },
  { .dx =  1, .dy =  0, },
};

typedef enum {
  ITEM_NOTHING = 0,
  ITEM_ENTRANCE,
  ITEM_EXIT,
  ITEM_LEVEL_MAP,
  ITEM_AMULET,
  ITEM_MOSAIC_PIECE,
  ITEM_RING,
  ITEM_DOLMEN,
  ITEM_CORPSE,
} item_t;

typedef enum {
  ORNAMENT_NOTHING,
  ORNAMENT_LIZARD,
  ORNAMENT_RATHOLE,
  ORNAMENT_DEBRIS,
  ORNAMENT_CRACKS,
  ORNAMENT_MUDPATCHES,
  ORNAMENT_BUMPS,
  ORNAMENT_PUDDLE,
  ORNAMENTS_END
} ornament_t;

uint8_t ornament_type_color[9] = {
  SAF_COLOR_BLACK,
  ORNAMENT_COLOR,
  ORNAMENT_COLOR,
  ORNAMENT_COLOR,
  ORNAMENT_COLOR,
  ORNAMENT_COLOR,
  ORNAMENT_COLOR,
  ORNAMENT_COLOR,
  SAF_COLOR_BLACK,
};

/* uint8_t ornament_type_color[9] = {
  SAF_COLOR_BLACK,
  SAF_COLOR_GRAY,
  FLOOR_HIGHLIGHT_COLOR,
  SAF_COLOR_GRAY_DARK,
  FLOOR_HIGHLIGHT_COLOR,
  SAF_COLOR_BROWN,
  FLOOR_HIGHLIGHT_COLOR,
  41,
  SAF_COLOR_BLACK,
}; */


typedef struct {
  uint8_t tried_directions;
  move_t backtrack_move;
  uint8_t area;
  item_t contained_item;
  ornament_t ornament;
} maze_tile_info_t;

typedef struct {
  int8_t xs;
  int8_t ys;
  int8_t xf;
  int8_t yf;
  maze_tile_info_t info[MAX_MAZE_WIDTH][MAX_MAZE_HEIGHT];
  uint8_t underlying_structure[2 + MAX_MAZE_WIDTH * MAX_MAZE_HEIGHT];
  uint8_t known_parts[2 + MAX_MAZE_WIDTH * MAX_MAZE_HEIGHT];
  uint16_t path_length;
  uint16_t path_max_length;
  uint8_t width;
  uint8_t height;
  uint8_t tile_width;
  uint8_t tile_height;
  uint32_t last_move_frame;
  uint8_t n_islands;
  direction_t stair_down_direction;
  direction_t stair_up_direction;
  direction_t dolmen_direction;
#ifdef DEBUG
  uint8_t actual_islands;
#endif
} maze_overview_t;

maze_overview_t maze;

uint8_t calculate_range(uint8_t tile_width);

uint8_t (approach_descend_loop)(void);
uint8_t (approach_mosaic_loop)(void);
uint8_t (ascend_loop)(void);
uint8_t (back_to_dolmen_loop)(void);
uint8_t (closeup_loop)(void);
uint8_t (death_loop)(void);
uint8_t (descend_loop)(void);
uint8_t (dolmen_crossed_loop)(void);
uint8_t (exit_allowed_loop)(void);
uint8_t (exit_disallowed_loop)(void);
uint8_t (fading_loop)(void);
uint8_t (falling_petagolon_loop)(void);
uint8_t (grave_loop)(void);
uint8_t (legs_loop)(void);
uint8_t (mosaic_loop)(void);
uint8_t (mosaic_solved_loop)(void);
uint8_t (object_found_loop)(void);
uint8_t (overview_loop)(void);
uint8_t (pilgrim_out_loop)(void);
uint8_t (success_loop)(void);
uint8_t (teleport_loop)(void);
uint8_t (title_loop)(void);
uint8_t (ghost_loop)(void);
uint8_t (burial_loop)(void);

#ifdef DEBUG
uint8_t (object_found_loop)(void);
#endif

uint8_t (*current_loop)(void);
uint8_t (*loop_after_transition)(void);
void fade_to_loop(uint8_t (*next_loop)(void));
void transition_to_loop(uint8_t (*next_loop)(void));

#ifdef DEBUG
uint8_t show_debug_overlay = 0;
#endif
