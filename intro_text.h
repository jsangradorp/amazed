_SAF_CONST char s1[]="";
_SAF_CONST char s2[]="";
_SAF_CONST char s3[]="";
_SAF_CONST char s4[]="";
_SAF_CONST char s5[]="";
_SAF_CONST char s6[]="";
_SAF_CONST char s7[]="";
_SAF_CONST char s8[]="";
_SAF_CONST char s9[]="";
_SAF_CONST char s10[]="";
_SAF_CONST char s11[]="";
_SAF_CONST char s12[]="";
_SAF_CONST char s13[]="";
_SAF_CONST char s14[]="";
_SAF_CONST char s15[]="There is";
_SAF_CONST char s16[]="little";
_SAF_CONST char s17[]="left of the";
_SAF_CONST char s18[]="bygone wisdom";
_SAF_CONST char s19[]="scraped in";
_SAF_CONST char s20[]="the forgotten";
_SAF_CONST char s21[]="parchments";
_SAF_CONST char s22[]="of yesteryear:";
_SAF_CONST char s23[]="";
_SAF_CONST char s24[]="\"... find";
_SAF_CONST char s25[]="pleasure in";
_SAF_CONST char s26[]="dismembering";
_SAF_CONST char s27[]="unlucky";
_SAF_CONST char s28[]="villagers for";
_SAF_CONST char s29[]="the pure fun";
_SAF_CONST char s30[]="of it, then";
_SAF_CONST char s31[]="playing with";
_SAF_CONST char s32[]="the pieces";
_SAF_CONST char s33[]="about the";
_SAF_CONST char s34[]="tunnels...\"";
_SAF_CONST char s35[]="";
_SAF_CONST char s36[]="\"... the";
_SAF_CONST char s37[]="misfortuned";
_SAF_CONST char s38[]="victims' souls";
_SAF_CONST char s39[]="never reach";
_SAF_CONST char s40[]="peace as their";
_SAF_CONST char s41[]="remains are";
_SAF_CONST char s42[]="left to the";
_SAF_CONST char s43[]="scavengers...\"";
_SAF_CONST char s44[]="";
_SAF_CONST char s45[]="\"... in their";
_SAF_CONST char s46[]="underground";
_SAF_CONST char s47[]="lairs; they";
_SAF_CONST char s48[]="also build";
_SAF_CONST char s49[]="magical";
_SAF_CONST char s50[]="structures";
_SAF_CONST char s51[]="said to";
_SAF_CONST char s52[]="be able to";
_SAF_CONST char s53[]="contain and";
_SAF_CONST char s54[]="repeat Time";
_SAF_CONST char s55[]="Itself once";
_SAF_CONST char s56[]="and again,";
_SAF_CONST char s57[]="which they use";
_SAF_CONST char s58[]="to factually";
_SAF_CONST char s59[]="relive their";
_SAF_CONST char s60[]="misdeeds...\"";
_SAF_CONST char s61[]="";
_SAF_CONST char s62[]="The following";
_SAF_CONST char s63[]="piece of";
_SAF_CONST char s64[]="lore has been";
_SAF_CONST char s65[]="reconstructed";
_SAF_CONST char s66[]="from the";
_SAF_CONST char s67[]="remaining";
_SAF_CONST char s68[]="pieces of the";
_SAF_CONST char s69[]="lost scroll of";
_SAF_CONST char s70[]="ancient times:";
_SAF_CONST char s71[]="";
_SAF_CONST char s72[]="\"Legend says";
_SAF_CONST char s73[]="that a giant";
_SAF_CONST char s74[]="petagolon";
_SAF_CONST char s75[]="came to the";
_SAF_CONST char s76[]="village,";
_SAF_CONST char s77[]="offended by";
_SAF_CONST char s78[]="the lack of";
_SAF_CONST char s79[]="respect shown";
_SAF_CONST char s80[]="by children";
_SAF_CONST char s81[]="and adults";
_SAF_CONST char s82[]="alike, and";
_SAF_CONST char s83[]="stole the";
_SAF_CONST char s84[]="sacred mosaic";
_SAF_CONST char s85[]="of the oracle,";
_SAF_CONST char s86[]="the most";
_SAF_CONST char s87[]="cherished";
_SAF_CONST char s88[]="artifact of";
_SAF_CONST char s89[]="the Antiquity.";
_SAF_CONST char s90[]="";
_SAF_CONST char s91[]="The monster";
_SAF_CONST char s92[]="carried the";
_SAF_CONST char s93[]="relic to";
_SAF_CONST char s94[]="the depths";
_SAF_CONST char s95[]="of its lair,";
_SAF_CONST char s96[]="smashing it on";
_SAF_CONST char s97[]="the walls on";
_SAF_CONST char s98[]="its way down,";
_SAF_CONST char s99[]="breaking it";
_SAF_CONST char s100[]="down into";
_SAF_CONST char s101[]="pieces and";
_SAF_CONST char s102[]="scattering";
_SAF_CONST char s103[]="them all";
_SAF_CONST char s104[]="around";
_SAF_CONST char s105[]="down the";
_SAF_CONST char s106[]="hideous caves.";
_SAF_CONST char s107[]="The run was";
_SAF_CONST char s108[]="strenuous";
_SAF_CONST char s109[]="and wore the";
_SAF_CONST char s110[]="behemoth out";
_SAF_CONST char s111[]="to the point";
_SAF_CONST char s112[]="of exhaustion,";
_SAF_CONST char s113[]="making it fall";
_SAF_CONST char s114[]="to a long,";
_SAF_CONST char s115[]="repairing";
_SAF_CONST char s116[]="sleep,";
_SAF_CONST char s117[]="leaving the";
_SAF_CONST char s118[]="care of the";
_SAF_CONST char s119[]="fifteen levels";
_SAF_CONST char s120[]="of filthy";
_SAF_CONST char s121[]="tunnels,";
_SAF_CONST char s122[]="traps and";
_SAF_CONST char s123[]="dead ends, as";
_SAF_CONST char s124[]="long forgotten";
_SAF_CONST char s125[]="scripts used";
_SAF_CONST char s126[]="to talk about,";
_SAF_CONST char s127[]="to disgusting";
_SAF_CONST char s128[]="giant insect";
_SAF_CONST char s129[]="servants,";
_SAF_CONST char s130[]="as well as";
_SAF_CONST char s131[]="perverted";
_SAF_CONST char s132[]="shadows of";
_SAF_CONST char s133[]="some who";
_SAF_CONST char s134[]="once were";
_SAF_CONST char s135[]="villagers, and";
_SAF_CONST char s136[]="other pests of";
_SAF_CONST char s137[]="their breed.";
_SAF_CONST char s138[]="";
_SAF_CONST char s139[]="Since then,";
_SAF_CONST char s140[]="without the";
_SAF_CONST char s141[]="protection of";
_SAF_CONST char s142[]="the oracle";
_SAF_CONST char s143[]="devices,";
_SAF_CONST char s144[]="the village";
_SAF_CONST char s145[]="started a";
_SAF_CONST char s146[]="period of";
_SAF_CONST char s147[]="calamities";
_SAF_CONST char s148[]="and famine,";
_SAF_CONST char s149[]="followed";
_SAF_CONST char s150[]="by massive";
_SAF_CONST char s151[]="diseases and";
_SAF_CONST char s152[]="disgrace,";
_SAF_CONST char s153[]="leaving just";
_SAF_CONST char s154[]="a shadow of";
_SAF_CONST char s155[]="what life used";
_SAF_CONST char s156[]="to be.\"";
_SAF_CONST char s157[]="";
_SAF_CONST char s158[]="In the chilly";
_SAF_CONST char s159[]="nights of the";
_SAF_CONST char s160[]="village, it is";
_SAF_CONST char s161[]="whispered that";
_SAF_CONST char s162[]="the creature";
_SAF_CONST char s163[]="is still";
_SAF_CONST char s164[]="sleeping its";
_SAF_CONST char s165[]="long sleep,";
_SAF_CONST char s166[]="laying down";
_SAF_CONST char s167[]="amongst the";
_SAF_CONST char s168[]="fragments of";
_SAF_CONST char s169[]="the stolen";
_SAF_CONST char s170[]="holy item,";
_SAF_CONST char s171[]="and one day";
_SAF_CONST char s172[]="a wandering";
_SAF_CONST char s173[]="pilgrim";
_SAF_CONST char s174[]="will come";
_SAF_CONST char s175[]="from afar to";
_SAF_CONST char s176[]="hunt down the";
_SAF_CONST char s177[]="petagolon and";
_SAF_CONST char s178[]="recover the";
_SAF_CONST char s179[]="pieces of the";
_SAF_CONST char s180[]="lost sacred";
_SAF_CONST char s181[]="mosaic of the";
_SAF_CONST char s182[]="oracle, thus";
_SAF_CONST char s183[]="making its";
_SAF_CONST char s184[]="restoration";
_SAF_CONST char s185[]="a reality and";
_SAF_CONST char s186[]="bringing joy";
_SAF_CONST char s187[]="and prosperity";
_SAF_CONST char s188[]="back to the";
_SAF_CONST char s189[]="village.";
_SAF_CONST char s190[]="";
_SAF_CONST char s191[]="Today, such a";
_SAF_CONST char s192[]="character has";
_SAF_CONST char s193[]="arrived in the";
_SAF_CONST char s194[]="village and is";
_SAF_CONST char s195[]="now heading";
_SAF_CONST char s196[]="towards the";
_SAF_CONST char s197[]="cursed place,";
_SAF_CONST char s198[]="holding but";
_SAF_CONST char s199[]="a staff in";
_SAF_CONST char s200[]="hand and the";
_SAF_CONST char s201[]="determination";
_SAF_CONST char s202[]="to explore";
_SAF_CONST char s203[]="the greater";
_SAF_CONST char s204[]="depths of";
_SAF_CONST char s205[]="the tunnels,";
_SAF_CONST char s206[]="kill the cause";
_SAF_CONST char s207[]="of so much";
_SAF_CONST char s208[]="misfortune,";
_SAF_CONST char s209[]="gather the";
_SAF_CONST char s210[]="pieces of";
_SAF_CONST char s211[]="mosaic making";
_SAF_CONST char s212[]="it possible";
_SAF_CONST char s213[]="to restore it,";
_SAF_CONST char s214[]="and bring the";
_SAF_CONST char s215[]="village back";
_SAF_CONST char s216[]="to its former";
_SAF_CONST char s217[]="welfare.";
_SAF_CONST char s218[]="";
_SAF_CONST char s219[]="The villagers";
_SAF_CONST char s220[]="are raising";
_SAF_CONST char s221[]="all their";
_SAF_CONST char s222[]="prayers to";
_SAF_CONST char s223[]="their deities,";
_SAF_CONST char s224[]="to beg their";
_SAF_CONST char s225[]="helping the";
_SAF_CONST char s226[]="pilgrim hero";
_SAF_CONST char s227[]="attain success";
_SAF_CONST char s228[]="and write a";
_SAF_CONST char s229[]="new, colourful";
_SAF_CONST char s230[]="and cheerful";
_SAF_CONST char s231[]="page in their";
_SAF_CONST char s232[]="history... and";
_SAF_CONST char s233[]="they won't";
_SAF_CONST char s234[]="consent to";
_SAF_CONST char s235[]="a failed";
_SAF_CONST char s236[]="returning";
_SAF_CONST char s237[]="messenger.";
_SAF_CONST char s238[]="";
_SAF_CONST char s239[]="";
_SAF_CONST char s240[]="";
_SAF_CONST char s241[]="";
_SAF_CONST char s242[]="";
_SAF_CONST char s243[]="";
_SAF_CONST char s244[]="";
_SAF_CONST char s245[]="";
_SAF_CONST char s246[]="";
_SAF_CONST char s247[]="";
_SAF_CONST char s248[]="";
_SAF_CONST char s249[]="";
_SAF_CONST char s250[]="";
_SAF_CONST char s251[]="";

#define N_INTRO_TEXT_LINES (251)

_SAF_CONST char *const intro_text[] = {
s1,
s2,
s3,
s4,
s5,
s6,
s7,
s8,
s9,
s10,
s11,
s12,
s13,
s14,
s15,
s16,
s17,
s18,
s19,
s20,
s21,
s22,
s23,
s24,
s25,
s26,
s27,
s28,
s29,
s30,
s31,
s32,
s33,
s34,
s35,
s36,
s37,
s38,
s39,
s40,
s41,
s42,
s43,
s44,
s45,
s46,
s47,
s48,
s49,
s50,
s51,
s52,
s53,
s54,
s55,
s56,
s57,
s58,
s59,
s60,
s61,
s62,
s63,
s64,
s65,
s66,
s67,
s68,
s69,
s70,
s71,
s72,
s73,
s74,
s75,
s76,
s77,
s78,
s79,
s80,
s81,
s82,
s83,
s84,
s85,
s86,
s87,
s88,
s89,
s90,
s91,
s92,
s93,
s94,
s95,
s96,
s97,
s98,
s99,
s100,
s101,
s102,
s103,
s104,
s105,
s106,
s107,
s108,
s109,
s110,
s111,
s112,
s113,
s114,
s115,
s116,
s117,
s118,
s119,
s120,
s121,
s122,
s123,
s124,
s125,
s126,
s127,
s128,
s129,
s130,
s131,
s132,
s133,
s134,
s135,
s136,
s137,
s138,
s139,
s140,
s141,
s142,
s143,
s144,
s145,
s146,
s147,
s148,
s149,
s150,
s151,
s152,
s153,
s154,
s155,
s156,
s157,
s158,
s159,
s160,
s161,
s162,
s163,
s164,
s165,
s166,
s167,
s168,
s169,
s170,
s171,
s172,
s173,
s174,
s175,
s176,
s177,
s178,
s179,
s180,
s181,
s182,
s183,
s184,
s185,
s186,
s187,
s188,
s189,
s190,
s191,
s192,
s193,
s194,
s195,
s196,
s197,
s198,
s199,
s200,
s201,
s202,
s203,
s204,
s205,
s206,
s207,
s208,
s209,
s210,
s211,
s212,
s213,
s214,
s215,
s216,
s217,
s218,
s219,
s220,
s221,
s222,
s223,
s224,
s225,
s226,
s227,
s228,
s229,
s230,
s231,
s232,
s233,
s234,
s235,
s236,
s237,
s238,
s239,
s240,
s241,
s242,
s243,
s244,
s245,
s246,
s247,
s248,
s249,
s250,
s251,
};
