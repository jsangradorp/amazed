/*
   A-Mazed

   Copyright (C) 2023  Julio Sangrador-Paton

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
   */

/* Maximum technically sensible size is 72x72
   Minimum technically sensible size is 12x12 */

#define MAX_MAZE_WIDTH (72)
#define MAX_MAZE_HEIGHT (72)
#define MIN_MAZE_WIDTH (16)
#define MIN_MAZE_HEIGHT (16)

#define N_LEVELS (15)
#define DEEPEST_LEVEL_WITH_MAP (12)
#define DEEPEST_LEVEL_WITH_TORCHES (10)
#define AMULET_LEVEL (5)
#define RING_LEVEL (10)
#define FIRST_LEVEL_WITH_CORPSES (2)
#define DEEPEST_LEVEL_WITH_CORPSES (13)
#define MAX_CORPSES (255)

#define CLOSEUP_PLAYER_SPEED (2)

#ifdef SAF_HEADER
#include SAF_HEADER
#else
#warning SAF_HEADER not set. Including "./lib/SAF/saf.h" as a default.
#warning If the SAF header is somewhere else, add -DSAF_HEADER='"./path/to/your/saf.h"' (quoting important) to the compilation.
#include "./lib/SAF/saf.h"
#endif

#ifndef _SAF_COPY_STRING
#define _SAF_COPY_STRING(buffer, addr) (strcpy((buffer), (addr)))
#endif

#include "./rng-4294967294.c"

#include "palette.h"

#include "./assets.h"

#include "./globals.h"
#include "./util.h"
#include "./textfncs.h"
#include "./player.h"

#include "./objects.h"
#include "./foes.h"

#include "./saveload.h"

#include "./maze.h"
#include "./closeup.h"
#include "./mosaic.h"
#include "./cutscenes.h"

#include "./debug.h"

void
SAF_init(void) {
  init_foe_types();
  init_foe_rng();
  load_highest_score();
  current_loop = intro_loop;
  // XXX current_loop = success_loop;
#ifdef DEBUG
  //current_loop = ghost_loop;
#ifdef DUMP_LEVELS_INFO
  current_loop = dump_level_info_loop;
#else
#ifdef DUMP_MAPS
  current_loop = dump_levels_images;
#endif
#endif
#ifdef TEST_SAVELOAD
  current_loop = test_save_load_loop;
#endif
#endif
}

uint8_t
SAF_loop(void) {
  if (transitioning_out) {
    update_transition_out();
    return 1;
  }
  uint8_t res = current_loop();
  if (transitioning_in) {
    update_transition_in();
  }
  if ((SAF_frame() % (10 * SAF_FPS)) == 0) {
    save_highest_score();
  }
#ifdef DEBUG
  if (SAF_buttonJustPressed(SAF_BUTTON_C)) {
    show_debug_overlay = !show_debug_overlay;
  }
#ifndef NO_OVERLAY
  if (show_debug_overlay) {
    debug_overlay();
  }
#endif
#endif
  return res;
}
