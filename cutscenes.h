/*
   A-Mazed

   Copyright (C) 2023  Julio Sangrador-Paton

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
   */

#include <string.h>

void
prepare_level(void) {
  prepare_maze();
  prepare_closeup();
}

uint8_t
fading_loop(void) {
  const uint8_t parts = 32;
  static uint8_t fade_count = SAF_FPS / 5;
  uint8_t x = SAF_random() % (SAF_SCREEN_WIDTH / parts);
  uint8_t y = SAF_random() % (SAF_SCREEN_HEIGHT / parts);
  for (uint8_t i = 0; i < parts; i++) {
    for (uint8_t j = 0; j < parts; j++) {
      SAF_drawPixel(
          x + i * (SAF_SCREEN_WIDTH / parts),
          y + j * (SAF_SCREEN_HEIGHT / parts),
          SAF_COLOR_BLACK);
    }
  }
  SAF_random();
  if (--fade_count == 0) {
    fade_count = SAF_FPS / 5;
    current_loop = loop_after_transition;
  }
  return 1;
}

uint8_t
transition_loop(void) {
  static uint8_t transition_count = 15;
  static int i = 0;
  SAF_drawRect(
      i,
      i,
      SAF_SCREEN_WIDTH - 2 * i,
      SAF_SCREEN_HEIGHT - 2 * i,
      SAF_COLOR_BLACK,
      0);
  SAF_drawRect(
      i + 1,
      i + 1,
      SAF_SCREEN_WIDTH - 2 * (i + 1),
      SAF_SCREEN_HEIGHT - 2 * (i + 1),
      SAF_COLOR_BLACK,
      0);
  i+=2;
  if (--transition_count == 0) {
    transition_count = SAF_FPS / 5;
    i = 0;
    current_loop = loop_after_transition;
  }
  return 1;
}

void
fade_to_loop(uint8_t (*next_loop)(void)) {
  loop_after_transition = next_loop;
  current_loop = fading_loop;
}

void
transition_to_loop(uint8_t (*next_loop)(void)) {
  loop_after_transition = next_loop;
  current_loop = transition_loop;
}

void
draw_floor(void) {
  SAF_clearScreen(FLOOR_COLOR);
  seed_rnd(default_rng, 1);
  for (uint8_t i = 0; i < 255; i++) {
    SAF_drawPixel(rnd(default_rng) % SAF_SCREEN_WIDTH, rnd(default_rng) % SAF_SCREEN_HEIGHT, FLOOR_HIGHLIGHT_COLOR);
    SAF_drawPixel(rnd(default_rng) % SAF_SCREEN_WIDTH, rnd(default_rng) % SAF_SCREEN_HEIGHT, FLOOR_HIGHLIGHT_COLOR);
    SAF_drawPixel(rnd(default_rng) % SAF_SCREEN_WIDTH, rnd(default_rng) % SAF_SCREEN_HEIGHT, FLOOR_HIGHLIGHT_COLOR);
    SAF_drawPixel(rnd(default_rng) % SAF_SCREEN_WIDTH, rnd(default_rng) % SAF_SCREEN_HEIGHT, FLOOR_HIGHLIGHT_COLOR);
  }
}

#define OX (SAF_SCREEN_WIDTH / 2)
#define OY (SAF_SCREEN_HEIGHT / 2)
#define HALF_W (WALK_SEQUENCE_WIDTH / 2)
#define HALF_H (WALK_SEQUENCE_HEIGHT / 2)

uint8_t
descend_loop(void) {
  static int pos = -OX;
  static uint8_t current_step = 3;

  int8_t size =  4 - (pos + OX) / 16; /* descending it should be 4 to 1 if pos is -32 to 31 */

  uint8_t size_transform[5] = { /* we skip 0 for size from 1 to 4 */
    SAF_TRANSFORM_NONE,
    SAF_TRANSFORM_NONE,
    SAF_TRANSFORM_SCALE_2,
    SAF_TRANSFORM_SCALE_3,
    SAF_TRANSFORM_SCALE_4,
  };

  draw_floor();

  /* Presuming the image is a square */
  direction_t d = maze.stair_down_direction;
  int8_t dx = moves_table[d].dx;
  int8_t dy = moves_table[d].dy;

  SAF_drawImage1Bit(
      stair_down_image,
      (SAF_SCREEN_WIDTH - 4 * stair_down_image[0]) / 2,
      (SAF_SCREEN_HEIGHT - 4 * stair_down_image[1]) / 2,
      0,
      FLOOR_COLOR,
      FLOOR_DARK_COLOR,
      SAF_TRANSFORM_SCALE_4 | transforms_table[d]);

  SAF_drawImage1Bit(
      walk_sequence[current_step + 1],
      OX - size * HALF_W + pos * dx,
      OY - size * HALF_H + pos * dy,
      walk_sequence_mask[current_step + 1],
      PLAYER_BODY_COLOR,
      PLAYER_EYES_COLOR,
      size_transform[size] | transforms_table[d]);

  if (player.has_torch && is_torch_lit()) {
    int8_t rx = -1 + SAF_random() % 3;
    int8_t ry = -1 + SAF_random() % 3;
    for (uint8_t yy = 0; yy < size; yy++) {
      for (uint8_t xx = 0; xx < size; xx++) {
        SAF_drawPixel(
            xx + OX
            + dy * size * (walk_torch_delta_x + (d == UP))
            + dx * (pos + size * (walk_torch_delta_y[current_step + 1] + (d == LEFT))),
            yy + OY
            + dy * (pos + size * (walk_torch_delta_y[current_step + 1] + (d == UP)))
            - dx * size * (walk_torch_delta_x + (d == RIGHT)),
            SAF_COLOR_YELLOW);
        SAF_drawPixel(
            xx + OX
            + dy * size * (walk_torch_delta_x + rx + (d == UP))
            + dx * (pos + size * (walk_torch_delta_y[current_step + 1] + rx + (d == LEFT))),
            yy + OY
            + dy * (pos + size * (walk_torch_delta_y[current_step + 1] + ry + (d == UP)))
            - dx * size * (walk_torch_delta_x + rx + (d == RIGHT)),
            SAF_COLOR_RED);
      }
    }
  }

  /* cover player */
  uint8_t x = (d != RIGHT) ?
        0 :
        SAF_SCREEN_WIDTH - (SAF_SCREEN_WIDTH - 4 * stair_down_image[0]) / 2;
  uint8_t y = (d != DOWN) ?
        0 :
        SAF_SCREEN_HEIGHT - (SAF_SCREEN_HEIGHT - 4 * stair_down_image[1]) / 2;
  uint8_t w = ((d == UP) || (d == DOWN)) ?
        SAF_SCREEN_WIDTH :
        (SAF_SCREEN_WIDTH - 4 * stair_down_image[0]) / 2;
  uint8_t h = ((d == LEFT) || (d == RIGHT)) ?
        SAF_SCREEN_HEIGHT :
        (SAF_SCREEN_HEIGHT - 4 * stair_down_image[1]) / 2;

  SAF_drawRect( x, y, w, h, FLOOR_COLOR, 1);
  seed_rnd(default_rng, 1);
  for (uint8_t i = 0; i < 64; i++) {
    SAF_drawPixel(x + rnd(default_rng) % w, y + rnd(default_rng) % h, FLOOR_HIGHLIGHT_COLOR);
    SAF_drawPixel(x + rnd(default_rng) % w, y + rnd(default_rng) % h, FLOOR_HIGHLIGHT_COLOR);
  }

  /* until here */

  // SAF_drawPixel(
  //     OX + pos * dx,
  //     OY + pos * dy,
  //     SAF_COLOR_WHITE);

  if ((SAF_frame() % 2) != 0) {
    current_step = (current_step + 1) % (N_WALK_STEPS - 2);
    if ((current_step == 0) || (current_step == 8)) {
      playSound(SOUND_STEP);
    }
  }

  pos = pos + 1;

  /* Check cutscene end, reset static vars and execute descend */
  if (pos >= OX) {
    current_step = 3;
    pos = -OX;

    player.depth++;
    prepare_level();
    player.puppet.direction = opposite_direction_to(maze.stair_up_direction);
    player.puppet.x = maze.xs + moves_table[player.puppet.direction].dx;
    player.puppet.y = maze.ys + moves_table[player.puppet.direction].dy;
    player.puppet.closeup.x = maze.tile_width  / 2 * (1 - moves_table[player.puppet.direction].dx);
    player.puppet.closeup.y = maze.tile_height / 2 * (1 - moves_table[player.puppet.direction].dy);
    player.puppet.closeup.current_image_index = 0;
    if (player.depth == N_LEVELS) {
      petagolon.puppet.x = player.puppet.x + 1;
      petagolon.puppet.y = player.puppet.y - 3;
    }
    fade_to(closeup_loop);
  }
  return 1;
}

#define LEGS_WAIT_FRAMES (3 * SAF_FPS)

uint8_t
legs_loop(void) {
  static uint16_t frames = LEGS_WAIT_FRAMES;

  SAF_clearScreen(GENERIC_FILL_COLOR);

  /* Floor */

  uint8_t j = 1;
  for (uint16_t i = 0; i < 32; i += j++) {
    SAF_drawLine(0, 32 + i, 63, 32 + i, GENERIC_BORDER_COLOR);
  }

  /* Background wall */

  SAF_drawRect(0, 0, SAF_SCREEN_WIDTH, 33, SAF_COLOR_GRAY_DARK, 1);

  /* Petagolon legs */

  SAF_drawImageCompressed(
      petagolon_legs_image,
      -1 + SAF_random() % 2,
      0,
      SAF_TRANSFORM_NONE,
      0xe0
      );

  if (!frames-- || SAF_buttonPressed(SAF_BUTTON_A)) {
    frames = LEGS_WAIT_FRAMES;
    /* Copied and adapted from descend loop */
    player.depth++;
    /* prepare_level(); */
    player.puppet.direction = opposite_direction_to(maze.stair_up_direction);
    player.puppet.x = maze.xs  - 1;
    player.puppet.y = maze.ys;
    player.puppet.closeup.x = maze.tile_width  / 2 * 2;
    player.puppet.closeup.y = maze.tile_height / 2;
    fade_to_loop(closeup_loop);
  }
  return 1;
}

#define DISALLOWED_WAIT_FRAMES (3 * SAF_FPS)

uint8_t
exit_disallowed_loop(void) {
  static uint16_t frames = DISALLOWED_WAIT_FRAMES;

  SAF_clearScreen(GENERIC_FILL_COLOR);

  /* Floor */

  uint8_t j = 1;
  for (uint16_t i = 0; i < 32; i += j++) {
    SAF_drawLine(0, 32 + i, 63, 32 + i, GENERIC_BORDER_COLOR);
  }

  /* Starry night */

  SAF_drawRect(0, 0, SAF_SCREEN_WIDTH, 33, SAF_COLOR_BLUE_DARK, 1);

  seed_rnd(default_rng, 1);
  for (uint8_t i = 0; i < 30; i++) {
    uint8_t c = rnd(default_rng);
    SAF_drawPixel(rnd(default_rng) % SAF_SCREEN_WIDTH, rnd(default_rng) % 33, c < 80 ? SAF_COLOR_WHITE : c < 160 ? SAF_COLOR_GRAY : SAF_COLOR_GRAY_DARK);
  }

  /* Characters */

  SAF_drawImageCompressed(
      pilgrim_image02,
      25,
      15,
      SAF_TRANSFORM_SCALE_2 | ((frames > 2 * SAF_FPS) * SAF_TRANSFORM_FLIP),
      0xe0
      );

  SAF_drawImageCompressed(
      pilgrim_image02,
      -20,
      13,
      SAF_TRANSFORM_SCALE_2 | ((frames < 2 * SAF_FPS) * SAF_TRANSFORM_FLIP),
      0xe0
      );

  if (!frames-- || SAF_buttonPressed(SAF_BUTTON_A)) {
    frames = DISALLOWED_WAIT_FRAMES;
    fade_to_loop(descend_loop);
  }
  return 1;
}

#define ALLOWED_WAIT_FRAMES (3 * SAF_FPS / 2)

uint8_t
exit_allowed_loop(void) {
  static uint16_t frames = ALLOWED_WAIT_FRAMES;

  SAF_clearScreen(GENERIC_FILL_COLOR);

  /* Floor */

  uint8_t j = 1;
  for (uint16_t i = 0; i < 32; i += j++) {
    SAF_drawLine(0, 32 + i, 63, 32 + i, GENERIC_BORDER_COLOR);
  }

  /* Starry night */

  SAF_drawRect(0, 0, SAF_SCREEN_WIDTH, 33, SAF_COLOR_BLUE_DARK, 1);

  seed_rnd(default_rng, 1);
  for (uint8_t i = 0; i < 30; i++) {
    uint8_t c = rnd(default_rng);
    SAF_drawPixel(rnd(default_rng) % SAF_SCREEN_WIDTH, rnd(default_rng) % 33, c < 80 ? SAF_COLOR_WHITE : c < 160 ? SAF_COLOR_GRAY : SAF_COLOR_GRAY_DARK);
  }

  /* Characters */

  SAF_drawImageCompressed(
      pilgrim_image02,
      25 + ALLOWED_WAIT_FRAMES - frames,
      15,
      SAF_TRANSFORM_SCALE_2 | SAF_TRANSFORM_FLIP,
      0xe0
      );

  SAF_drawImageCompressed(
      pilgrim_image02,
      -20 - ALLOWED_WAIT_FRAMES + frames,
      13,
      SAF_TRANSFORM_SCALE_2,
      0xe0
      );

  if (!frames-- || SAF_buttonPressed(SAF_BUTTON_A)) {
    frames = ALLOWED_WAIT_FRAMES;
    current_loop = approach_mosaic_loop;
  }
  return 1;
}

uint8_t
ascend_loop(void) {
  static int pos = -OX;
  static uint8_t current_step = 3;

  int8_t size =  1 + (pos + OX) / 16; /* ascending it should be 1 to 4 if pos is -32 to 31 */

  uint8_t size_transform[5] = { /* we skip 0 for size from 1 to 4 */
    SAF_TRANSFORM_NONE,
    SAF_TRANSFORM_NONE,
    SAF_TRANSFORM_SCALE_2,
    SAF_TRANSFORM_SCALE_3,
    SAF_TRANSFORM_SCALE_4,
  };

  draw_floor();

  /* Presuming the image is a square */
  direction_t d = maze.stair_up_direction;
  int8_t dx = moves_table[d].dx;
  int8_t dy = moves_table[d].dy;

  SAF_drawImage1Bit(
      stair_up_image,
      (SAF_SCREEN_WIDTH - 4 * stair_up_image[0]) / 2,
      (SAF_SCREEN_HEIGHT - 4 * stair_up_image[1]) / 2,
      0,
      FLOOR_COLOR,
      FLOOR_DARK_COLOR,
      SAF_TRANSFORM_SCALE_4 | transforms_table[d]);

  SAF_drawImage1Bit(
      walk_sequence[current_step + 1],
      OX - size * HALF_W + pos * dx,
      OY - size * HALF_H + pos * dy,
      walk_sequence_mask[current_step + 1],
      PLAYER_BODY_COLOR,
      PLAYER_EYES_COLOR,
      size_transform[size] | transforms_table[d]);

  if (player.has_torch && is_torch_lit()) {
    int8_t rx = -1 + SAF_random() % 3;
    int8_t ry = -1 + SAF_random() % 3;
    for (uint8_t yy = 0; yy < size; yy++) {
      for (uint8_t xx = 0; xx < size; xx++) {
        SAF_drawPixel(
            xx + OX
            + dy * size * (walk_torch_delta_x + (d == UP))
            + dx * (pos + size * (walk_torch_delta_y[current_step + 1] + (d == LEFT))),
            yy + OY
            + dy * (pos + size * (walk_torch_delta_y[current_step + 1] + (d == UP)))
            - dx * size * (walk_torch_delta_x + (d == RIGHT)),
            SAF_COLOR_YELLOW);
        SAF_drawPixel(
            xx + OX
            + dy * size * (walk_torch_delta_x + rx + (d == UP))
            + dx * (pos + size * (walk_torch_delta_y[current_step + 1] + rx + (d == LEFT))),
            yy + OY
            + dy * (pos + size * (walk_torch_delta_y[current_step + 1] + ry + (d == UP)))
            - dx * size * (walk_torch_delta_x + rx + (d == RIGHT)),
            SAF_COLOR_RED);
      }
    }
  }

  if ((SAF_frame() % 2) != 0) {
    current_step = (current_step + 1) % (N_WALK_STEPS - 2);
    if ((current_step == 0) || (current_step == 8)) {
      playSound(SOUND_STEP);
    }
  }

  pos = pos + 1;

  /* Check cutscene end, reset static vars and execute descend */
  if (pos >= OX) {
    current_step = 3;
    pos = -OX;

    player.depth--;
    /* Check for success or abandon */
    if (player.depth == 0) {
      if (count_pieces_carried() == N_LEVELS) {
        fade_to_loop(exit_allowed_loop);
      } else {
        fade_to_loop(exit_disallowed_loop);
      }
    /* And cater for the other levels */
    } else if (!petagolon.is_dead && player.depth == (N_LEVELS - 1)) {
        fade_to_loop(legs_loop);
    } else {
      prepare_level();
      player.puppet.direction = opposite_direction_to(maze.stair_down_direction);
      player.puppet.x = maze.xf + moves_table[player.puppet.direction].dx;
      player.puppet.y = maze.yf + moves_table[player.puppet.direction].dy;
      player.puppet.closeup.x = maze.tile_width  / 2 * (1 - moves_table[player.puppet.direction].dx);
      player.puppet.closeup.y = maze.tile_height / 2 * (1 - moves_table[player.puppet.direction].dy);
      if (!player.has_torch && (player.depth == DEEPEST_LEVEL_WITH_TORCHES)) {
        player.has_torch = 1;
        go_to_object_found_loop(torch_image, torch_mask, SAF_COLOR_RED, TORCH_BORDER_COLOR, " a torch ");
      } else {
        fade_to_loop(closeup_loop);
      }
    }
  }
  return 1;
}

typedef struct {
  uint8_t x;
  uint8_t y;
  uint8_t r;
  uint8_t min;
  uint8_t max;
  uint8_t color;
} firework_t;

#define N_FIREWORKS (3)

firework_t fireworks[N_FIREWORKS] = {
  {
    .x = 12,
    .y = 10,
    .r = 5,
    .min = 5,
    .max = 9,
    .color = SAF_COLOR_YELLOW},
  {
    .x = 42,
    .y = 13,
    .r = 9,
    .min = 5,
    .max = 12,
    .color = SAF_COLOR_GREEN},
  {
    .x = 35,
    .y = 8,
    .r = 13,
    .min = 5,
    .max = 17,
    .color = SAF_COLOR_GREEN},
};

#define SUCCESS_WAIT_FRAMES (SAF_FPS * 5)
uint8_t
success_loop(void) {
  static uint8_t y = 0;
  static uint8_t saved_highest_score = 0;

  if (!saved_highest_score) {
    save_highest_score();
    saved_highest_score = 1;
  }

  SAF_clearScreen(SAF_COLOR_BLACK);
  SAF_drawImage(
      title_image,
      0,
      y,
      SAF_TRANSFORM_NONE,
      SAF_COLOR_BLACK);

  SAF_randomSeed(1);
  for(uint8_t i = 0; i < 20; i++) {
    SAF_drawPixel(SAF_random() % 64, y - SAF_random() % 42, SAF_random() % 2 ? SAF_COLOR_GRAY : SAF_COLOR_GRAY_DARK);
  }

  if (y < 42) {
    if (SAF_frame() % 2) {
      y++;
    }
  } else { /* Score and fireworks */

    if (((SAF_frame() / (2 * SAF_FPS)) % 2) == 0) {
      char c[2] = "0";
      uint32_t divisor = 100000;
      if (shown_score < player.score) {
        shown_score += (player.score - shown_score) / 8 + 1;
      }
      for (uint8_t i = 0; i < 5; i++) {
        c[0] = '0' + shown_score % divisor / (divisor / 10);
        divisor /= 10;
        SAF_drawText(c,
            8 + 10 * i,
            43 + 6 * SAF_sin((3 * SAF_frame() + 10 * i) & 0xff) / 127,
            (SAF_frame() + 9 * i) & 0xff,
            2);
      }
    } else {
      SAF_drawImage(
          skull_image,
            13,
            43 + 6 * SAF_sin((3 * SAF_frame()) & 0xff) / 127,
          SAF_TRANSFORM_NONE,
          SAF_COLOR_RED);
      char c[2] = "0";
      uint32_t divisor = 1000;
      for (uint8_t i = 0; i < 3; i++) {
        c[0] = '0' + player.corpses_buried % divisor / (divisor / 10);
        divisor /= 10;
        SAF_drawText(
            c,
            13 + 10 * (i + 1),
            43 + 6 * SAF_sin((3 * SAF_frame() + 10 * i) & 0xff) / 127,
            (SAF_frame() + 9 * i) & 0xff,
            2);
      }
    }

    for (uint8_t i = 0; i < N_FIREWORKS; i++) {
      firework_t fw = fireworks[i];
      for (uint16_t z = 0; z < 255; z += 16) {
        if (fw.r >= fw.min) {
          if (fw.r == fw.min) {
            SAF_clearScreen(fw.color);
            break;
          }
          SAF_drawPixel(fw.x + (fw.r - fw.min) * SAF_cos(z) / 128, fw.y + (fw.r - fw.min) * SAF_sin(z) / 128, fw.color);
        }
      }
      if (!(SAF_frame() % 3)) {
        fireworks[i].r++;
        fireworks[i].y++;
        if (fireworks[i].r > fireworks[i].max) {
          fireworks[i].r = 0;
          fireworks[i].min = rnd(default_rng) % 20;
          fireworks[i].max = fireworks[i].min + 10 + rnd(default_rng) % 15;
          fireworks[i].y = rnd(default_rng) % 10;
          fireworks[i].x = 5 + rnd(default_rng) % 54;
          fireworks[i].color = ((6 + rnd(default_rng) % 2) << 5) | ((6 + rnd(default_rng) % 2) << 2) | (2 + rnd(default_rng) % 2);
        }
      }
    }
  }

  if (SAF_buttonPressed(SAF_BUTTON_A)) {
    y = 0;
    saved_highest_score = 0;
    curtain_to(title_loop);
  }
  return 1;
}

uint8_t
grave_loop(void) {
  SAF_clearScreen(SAF_COLOR_BLACK);

  SAF_drawImageCompressed(
      pilgrim_image03,
      32 - pilgrim_image03[0] - 3,
      32 - pilgrim_image03[1] + 1,
      SAF_TRANSFORM_SCALE_2,
      0xe0);

  SAF_drawRect(23, 9, 18, 9, 0x24, 1);

  if (((SAF_frame() / (2 * SAF_FPS)) % 2) == 0) {
    char c[2] = "0";
    uint32_t divisor = 100000;
    for (uint8_t i = 0; i < 5; i++) {
      c[0] = '0' + player.score % divisor / (divisor / 10);
      divisor /= 10;
      SAF_drawText(c, 55 + 4 * SAF_sin((3 * SAF_frame() + 10 * i) % 0xff) / 127, 8 + 10 * i, 0x24, 2);
    }
  } else {
    SAF_drawImage(
        skull_image_dark,
        53 + 4 * SAF_sin((3 * SAF_frame()) % 0xff) / 127,
        10,
        SAF_TRANSFORM_NONE,
        SAF_COLOR_RED);
    char c[2] = "0";
    uint32_t divisor = 1000;
    for (uint8_t i = 0; i < 3; i++) {
      c[0] = '0' + player.corpses_buried % divisor / (divisor / 10);
      divisor /= 10;
      SAF_drawText(c, 55 + 4 * SAF_sin((3 * SAF_frame() + 10 * (i + 1)) % 0xff) / 127, 21 + 10 * i, 0x24, 2);
    }
  }

  for (uint8_t i = 0; i < 255; i++) {
    SAF_drawPixel(rnd(default_rng) % 64, rnd(default_rng) % 64, SAF_COLOR_BLACK);
  }

  if (SAF_buttonJustPressed(SAF_BUTTON_A)) {
    fade_to_loop(title_loop);
  }
  return 1;
}

#define DEATH_WAIT_FRAMES (SAF_FPS * 5)
#define MAX_DARKNESS (1000)
uint8_t
death_loop(void) {
  static uint16_t frames = DEATH_WAIT_FRAMES;
  static uint16_t darkness = 0;
  static uint8_t saved_highest_score = 0;

  if (!saved_highest_score) {
    save_highest_score();
    saved_highest_score = 1;
  }

  SAF_clearScreen(SAF_COLOR_RED_DARK);

  uint8_t rotations[4] = {
    SAF_TRANSFORM_NONE,
    SAF_TRANSFORM_ROTATE_90,
    SAF_TRANSFORM_ROTATE_180,
    SAF_TRANSFORM_ROTATE_270,
  };

  SAF_drawImageCompressed(
      pilgrim_image02,
      32 - pilgrim_image02[0] / 2,
      32 - pilgrim_image02[1] / 2,
      rotations[(SAF_frame() / 4) % 4],
      0xe0);

  seed_rnd(default_rng, 1);
  for (uint16_t i = 0; i < darkness; i++) {
    SAF_drawPixel(rnd(default_rng) % 64, rnd(default_rng) % 64, SAF_COLOR_BLACK);
  }
  darkness+=100;

  if (!frames--) {
    darkness = 0;
    frames = DEATH_WAIT_FRAMES;
    saved_highest_score = 0;
    fade_to_loop(grave_loop);
  }
  return 1;
}

void
create_floor_texture(uint8_t *image) {
  uint8_t w = image[0];
  uint8_t h = image[1];
  uint8_t color_1 = 0x64;
  uint8_t color_2 = FLOOR_COLOR;

  double red_step = ((color_2 >> 5) - (color_1 >> 5)) / (double)h;
  double green_step = (((color_2 >> 2) & 7) - ((color_1 >> 2) & 7)) / (double)h;
  double blue_step = ((color_2 & 3) - (color_1 & 3)) / (double)h;

  double red = color_1 >> 5;
  double green = (color_1 >> 2) & 7;
  double blue = color_1 & 3;

  for (uint8_t y = 0; y < h; y++) {
    uint8_t color = (((uint8_t)red) << 5) | (((uint8_t)green) << 2) | ((uint8_t)blue);
    for (uint8_t x = 0; x < w; x++) {
      image[2 + w * y + x] = color;
    }
    red += red_step;
    green += green_step;
    blue += blue_step;
  }

  for (uint16_t i = 0; i < 10000; i++) {
    uint8_t x;
    uint8_t y;
    const uint8_t limit = 32;

    do {
      x = rnd(default_rng) % SAF_SCREEN_WIDTH;
      y = rnd(default_rng) % h;
    } while ((y <= limit) || (y >= (h - limit)));

    int8_t p = -limit + rnd(default_rng) % (2 * limit + 1);
    uint8_t aux = image[2 + w * y + x];
    image[2 + w * y + x] = image[2 + w * (y + p) + x];
    image[2 + w * (y + p) + x] = aux;
  }
}

#define INITIAL_Y (64)

uint8_t
approach_descend_loop(void) {
  static uint8_t floor_texture[2 + SAF_SCREEN_WIDTH * 2 * SAF_SCREEN_HEIGHT];
  static uint8_t is_texture_created = 0;
  static int8_t y = INITIAL_Y;
  static int8_t step = 0;

  if(!is_texture_created) {
    floor_texture[0] = SAF_SCREEN_WIDTH;
    floor_texture[1] = 2 * SAF_SCREEN_HEIGHT;
    create_floor_texture(floor_texture);
    is_texture_created = 1;
  }

  SAF_clearScreen(SAF_COLOR_BLACK);
  SAF_drawImage(
      floor_texture,
      0,
      y,
      SAF_TRANSFORM_NONE,
      SAF_COLOR_BLACK
      );

  SAF_drawImage1Bit(
      stair_down_image,
      32 - stair_down_image[0] / 2,
      y + 2 * SAF_SCREEN_HEIGHT - 32 - stair_down_image[1] / 2,
      0,
      FLOOR_COLOR,
      0x64 /* better than FLOOR_DARK_COLOR */,
      SAF_TRANSFORM_NONE);

  SAF_drawImage1Bit(
      walk_sequence[step],
      32 - WALK_SEQUENCE_WIDTH / 2,
      32 - WALK_SEQUENCE_HEIGHT / 2,
      walk_sequence_mask[step],
      PLAYER_BODY_COLOR,
      PLAYER_EYES_COLOR,
      SAF_TRANSFORM_NONE);

  step = (step + 1) % N_WALK_STEPS;

  y -= y > -SAF_SCREEN_HEIGHT + stair_down_image[1] / 2 + 1;

  if (y == -SAF_SCREEN_HEIGHT + stair_down_image[1] / 2 + 1) {
    y = INITIAL_Y;
    step = 0;
    fade_to_loop(descend_loop);
  } else if (SAF_buttonJustPressed(SAF_BUTTON_A)) {
    current_loop = descend_loop; /* Abrupt */
  }
  return 1;
}

#define PILGRIM_MOVE_SKIP_FRAMES (5)

uint8_t
pilgrim_out_loop(void) {
  static uint8_t y = 31;
  static int8_t x = 1;
  static uint8_t step = 0;

  SAF_clearScreen(SAF_COLOR_BLACK);
  SAF_drawImage(
      title_image,
      0,
      0,
      SAF_TRANSFORM_NONE,
      SAF_COLOR_BLACK
      );
  SAF_drawImage(
      amazed_image,
      x,
      9,
      SAF_TRANSFORM_NONE,
      0xe0
      );
  SAF_drawImageCompressed(
      pilgrim_image[step],
      33,
      y,
      SAF_TRANSFORM_NONE,
      0xe0
      );
  x -= 10 * (x > -110);
  if ((SAF_frame() % PILGRIM_MOVE_SKIP_FRAMES) == 0) {
    step = (step + 1) % PILGRIM_N_STEPS;
    y += y < 64;
  }

  if (SAF_buttonJustPressed(SAF_BUTTON_A)) {
    current_loop = descend_loop;
  } else if (y == 64) {
    y = 31;
    x = 1;
    step = 0;
    transition_to_loop(approach_descend_loop);
  }
  return 1;
}

void
draw_curly_text(char *text, int8_t x, int8_t y) {
  char *current = text;
  char t[2] = " ";
  while (*current && ((x + (current - text) * 5) < (127 - 5))) {
    *t = *current;
    uint8_t xx = x + (current - text) * 5;
    uint8_t yy = y + 5 * SAF_sin(10 * (x + (current - text))) / 127;
    SAF_drawText(t, xx, yy, SAF_COLOR_WHITE, 1);
    current++;
  }
}

void
draw_title_text(char *text, int8_t x, int8_t y, uint8_t color) {
  char *current = text;
  char t[2] = " ";
  while (*current && ((x + (current - text) * 5) < (127 - 5))) {
    *t = *current;
    uint8_t xx = x + (current - text) * 5;
    SAF_drawText(t, xx, y, color, 1);
    current++;
  }
}

extern uint8_t curCharIndex;
uint8_t curLineIndex;

uint8_t
fader_colors(unsigned char c, int8_t x, int8_t y, uint8_t color, uint8_t x_offset, uint8_t y_offset) {
  _SAF_UNUSED(c);
  _SAF_UNUSED(x);
  _SAF_UNUSED(x_offset);
  switch(y + y_offset) {
    case 0:
    case 1:
    case 62:
    case 63:
      return SAF_COLOR_BLACK;
    case 2:
    case 3:
    case 60:
    case 61:
      return SAF_COLOR_RGB(64, 64, 64);
    case 4:
    case 5:
    case 58:
    case 59:
      return SAF_COLOR_RGB(128, 128, 128);
    case 6:
    case 7:
    case 56:
    case 57:
      return SAF_COLOR_RGB(192, 192, 192);
    default:
      return (curLineIndex + curCharIndex) % 2 ? color : SAF_COLOR_RGB(223, 223, 223);
  }
}

#include "./intro_text.h"

#define TEXTLINE_HEIGHT (5)
#define N_LINES_VISIBLE ((SAF_SCREEN_HEIGHT / TEXTLINE_HEIGHT) + 2)
#define TOP_Y (-5)

uint8_t
startup_loop(void) {
  
  static uint16_t curScanLine = 0;

  SAF_clearScreen(SAF_COLOR_BLACK);
  seed_rnd(default_rng, 1);
  for(uint8_t i = 0; i < 100; i++) {
    SAF_drawPixel(rnd(default_rng) % 64, rnd(default_rng) % 64, SAF_COLOR_RGB(32, 32, 32));
    SAF_drawPixel(rnd(default_rng) % 64, rnd(default_rng) % 64, SAF_COLOR_RGB(64, 64, 64));
  }
  uint8_t curTopTextLine = curScanLine / TEXTLINE_HEIGHT;
  uint8_t offset = curScanLine % TEXTLINE_HEIGHT;
  for (uint8_t line = 0; (line < N_LINES_VISIBLE) && (line < (N_INTRO_TEXT_LINES - curTopTextLine)); line++) {
    curLineIndex = curTopTextLine + line;
    char buffer[17]; /* Longest string including final 0 */
    _SAF_COPY_STRING(buffer, intro_text[curTopTextLine + line]);
    drawCenteredTextTighter(buffer, 32, TOP_Y + line * TEXTLINE_HEIGHT + TEXTLINE_HEIGHT - offset, SAF_COLOR_WHITE, fader_colors);
  }
  curScanLine += ((SAF_frame() % 4) == 0);
  if ((curScanLine > (N_INTRO_TEXT_LINES - N_LINES_VISIBLE) * TEXTLINE_HEIGHT) ||
      SAF_buttonJustPressed(SAF_BUTTON_A)) {
    curScanLine = 0;
    fade_to(title_loop);
  }
  return 1;
}

void
drawArc(int8_t x, int8_t y, uint8_t r, int8_t z0, int8_t zf, uint8_t color) {
  r++;
  int8_t x0 = x + r * SAF_cos(z0) / 128;
  int8_t y0 = y - r * SAF_sin(z0) / 128;

  // uint8_t step = (zf - z0) / 32;
  uint8_t step = 1;

  for (int8_t z = z0 + 1; z < zf; z += step) {
    SAF_drawLine(x0, y0, x + r * SAF_cos(z) / 128, y - r * SAF_sin(z) / 128, color);
    x0 = x + r * SAF_cos(z) / 128;
    y0 = y - r * SAF_sin(z) / 128;
  }

}

void
drawJSP(int8_t cx, int8_t cy, int8_t r, uint8_t color) {
  for (int8_t x = -1; x < 2; x++) {
    for (int8_t y = -1; y < 2; y++) {
      drawArc(cx + r + x, cy + r + y, r, -128,  64, color);
      drawArc(cx - r + x, cy + r + y, r, -128,   0, color);

      drawArc(cx + r + x, cy - r + y, r,    0, 127, color);
      drawArc(cx + r + x, cy - r + y, r, -128, -64, color);

      drawArc(cx + r + x, cy + y, r, -128, 127, color);
      SAF_drawLine(cx + x, cy - 2 * r + y, cx + x, cy + 2 * r + y, color);
    }
  }
}

uint8_t
intro_loop(void) {
  uint8_t background = SAF_COLOR_BLACK;
  uint8_t color = SAF_COLOR_WHITE;
  static int16_t frame = 0;

  static uint8_t intro_step = 0;

  static uint8_t r = 15;
  static uint8_t cx = 31;
  uint8_t cy = 31;

  SAF_clearScreen(background);

  if (intro_step == 0) {
    static uint8_t dot_size = 1;
    cx = 29 - 36 * SAF_cos(2 * frame) / 128;
    SAF_drawRect(cx, 29, dot_size, dot_size, color, 1);
    if (frame == 128) {
      dot_size = 3;
    }
    if (frame == 192) {
      intro_step = 1;
    }
  } else if (intro_step == 1) {
    r = 1 + 14 * (128 + SAF_cos(2 * frame)) / 256;
    cx = 29 - 36 * SAF_cos(1 * frame + 192) / 128;
    drawJSP(cx, cy, r, color);
    if (r >= 14) {
      intro_step = 2;
      frame = 0;
    }
  } else if (intro_step == 2) {
    cx = 31 - 2 + rnd(default_rng) % 5;
    cy = 31 - 2 + rnd(default_rng) % 5;
    drawJSP(cx, cy, r, color);
    if (frame == 10) {
      intro_step = 3;
      frame = 0;
    }
  } else if (intro_step == 3) {
    cx = 31 - 1 + rnd(default_rng) % 3;
    cy = 31 - 1 + rnd(default_rng) % 3;
    drawJSP(cx, cy, r, color);
    if (frame == 10) {
      intro_step = 4;
      frame = 0;
    }
  } else if (intro_step == 4) {
    cx = 31;
    cy = 31;
    drawJSP(cx, cy, r, color);
    if (frame == 10) {
      intro_step = 5;
      frame = 0;
    }
  } else if (intro_step == 5) {
    r -= (r > 0);
    drawJSP(cx, cy, r, color);
    if (r == 0) {
      intro_step = 6;
      frame = 0;
    }
  } else if (intro_step == 6) {
    if (frame == 20) {
      current_loop = startup_loop;
    }
  }

  if (SAF_buttonJustPressed(SAF_BUTTON_A)) {
    current_loop = title_loop;
  }
  ++frame;
  return 1;
}

uint8_t (help_loop_2)(void);
 
uint8_t
help_loop_1(void) {
  const int8_t cx = SAF_SCREEN_WIDTH / 2;
  const int8_t q = SAF_SCREEN_WIDTH / 3;
  static uint8_t frames = 0;

  seed_rnd(default_rng, 1);
  
  for (uint8_t x = 0; x < SAF_SCREEN_WIDTH; x++) {
    for (uint8_t y = 0; y < SAF_SCREEN_HEIGHT; y++) {
      if (((x > q) && (x < (SAF_SCREEN_WIDTH - q))) || ((y > q) && (y < (SAF_SCREEN_HEIGHT - q)))) {
        SAF_drawPixel(x, y, rnd(default_rng) < (248 - 2 * 16) ? FLOOR_COLOR : FLOOR_HIGHLIGHT_COLOR);
      } else {
        SAF_drawPixel(x, y, rnd(default_rng) < 192 ? SAF_COLOR_BLACK : SAF_COLOR_GRAY_DARK);
      }
    }
  }

  drawOutlinedCenteredTextTighter("CONTROLS", cx, 10, SAF_COLOR_BROWN, SAF_COLOR_YELLOW);
  drawOutlinedCenteredTextTighter("Arrows: move", cx, 29, SAF_COLOR_GRAY_DARK, SAF_COLOR_GRAY);
  drawOutlinedCenteredTextTighter("A: Overview", cx, 37, SAF_COLOR_GREEN_DARK, SAF_COLOR_GREEN);
  drawOutlinedCenteredTextTighter("B: Slash", cx, 45, SAF_COLOR_BROWN, SAF_COLOR_ORANGE);
  drawOutlinedCenteredTextTighter("C: Magic?", cx, 53, SAF_COLOR_RGB(127, 31, 63), SAF_COLOR_RED);

  if (++frames >= (5 * SAF_FPS)) {
    frames = 0;
    curtain_to(help_loop_2);
    return 1;
  }

  if (SAF_buttonJustPressed(SAF_BUTTON_C)) {
    frames = 0;
    fade_to(title_loop);
  }
  return 1;
}

uint8_t
help_loop_2(void) {
  const int8_t cx = SAF_SCREEN_WIDTH / 2;
  static uint8_t frames = 0;

  seed_rnd(default_rng, 1);
  
  draw_land();

  drawOutlinedCenteredTextTighter("CONTROLS", cx, 10, SAF_COLOR_GRAY, SAF_COLOR_WHITE);
  drawOutlinedCenteredTextTighter("Arrows: move", cx, 29, SAF_COLOR_GREEN_DARK, SAF_COLOR_GREEN);
  drawOutlinedCenteredTextTighter("A: Closeup", cx, 37, SAF_COLOR_ORANGE, SAF_COLOR_RGB(255, 192, 127));
  drawOutlinedCenteredTextTighter("B: Magic?", cx, 45, SAF_COLOR_RED, SAF_COLOR_RGB(127, 31, 63));
  drawOutlinedCenteredTextTighter("C: Magic?", cx, 53, SAF_COLOR_RED, SAF_COLOR_RGB(127, 31, 63));

  if (++frames >= (5 * SAF_FPS)) {
    frames = 0;
    curtain_to(help_loop_1);
    return 1;
  }

  if (SAF_buttonJustPressed(SAF_BUTTON_C)) {
    frames = 0;
    fade_to(title_loop);
  }
  return 1;
}

#define SECONDS_SEQUENCE_1 (4)
#define SECONDS_SEQUENCE_2 (7)
#define SECONDS_SEQUENCE_3 (8)
uint8_t
title_loop(void) {
  static uint16_t frames_elapsed = 0;
  static uint8_t transform = 0;
  static int8_t cx = 0;
  static int8_t cy = 0;
  static uint16_t dots = 1500;
  static uint8_t step = 0;

  SAF_clearScreen(SAF_COLOR_BLACK);
  SAF_drawImage(
      title_image,
      cx + 0,
      cy + 0,
      SAF_TRANSFORM_NONE,
      SAF_COLOR_BLACK
      );
  SAF_drawImageCompressed(
      pilgrim_image[step],
      cx + 33,
      cy + 31,
      SAF_TRANSFORM_NONE,
      0xe0
      );
  if ((SAF_frame() % PILGRIM_MOVE_SKIP_FRAMES) == 0) {
    step = (step + 1) % PILGRIM_N_STEPS;
  }

  /* Fade in */

  seed_rnd(default_rng, 1);
  for (uint16_t i = 0; i < dots; i++) {
    SAF_drawPixel(cx + rnd(default_rng) % 64, cy + rnd(default_rng) % 64, SAF_COLOR_BLACK);
    SAF_drawPixel(cx + rnd(default_rng) % 64, cy + rnd(default_rng) % 64, SAF_COLOR_BLACK);
    SAF_drawPixel(cx + rnd(default_rng) % 64, cy + rnd(default_rng) % 64, SAF_COLOR_BLACK);
    SAF_drawPixel(cx + rnd(default_rng) % 64, cy + rnd(default_rng) % 64, SAF_COLOR_BLACK);
  }
  dots -= 100 * (dots >= 100);

  /* You've been... */

  if (frames_elapsed > (SAF_FPS * SECONDS_SEQUENCE_1)) {
    char message[15] = "You've been...";
    static int8_t x = 64;
    draw_curly_text(message, x, 25);
    x -= 2 * (x > -5 * 14);
    if (x == -5 * 14) {
      uint8_t scales[3] = {
        SAF_TRANSFORM_SCALE_3,
        SAF_TRANSFORM_SCALE_2,
        SAF_TRANSFORM_NONE,
      };

      /* A-mazed! */

      uint8_t factors[3] = { 3, 2, 1 };

      SAF_drawImage(
          amazed_image,
          cx + 1 + amazed_image[0] / 2 - factors[transform] * amazed_image[0] / 2,
          cy + 9 + amazed_image[1] / 2 - factors[transform] * amazed_image[1] / 2,
          scales[transform],
          0xe0
          );
      transform += transform < 2;

      /* Some earthquake */

      if ((transform == 2) && frames_elapsed < (SAF_FPS * SECONDS_SEQUENCE_3)) {
        cx = -2 + SAF_random() % 5;
        cy = -2 + SAF_random() % 5;
      } else {
        cx = cy = 0;
      };

      draw_title_text("A: start", (SAF_SCREEN_WIDTH - 11 * 5) / 2, 0, SAF_frame() & 0xff);
      if (there_is_saved_game()) {
        draw_title_text("B: load", (SAF_SCREEN_WIDTH - 11 * 5) / 2, 5, SAF_frame() & 0xff);
      }

      if ((SAF_frame() / 75) % 2) {
        draw_title_text("C: help", 1, 59, SAF_COLOR_GRAY);
      } else if (highest_score > 0) {
        char t[6] = "00000";
        uint32_t divisor = 100000;
        for(uint8_t i = 0; i < 5; i++) {
          t[i] = '0' + highest_score % divisor / (divisor / 10);
          divisor /= 10;
        }
        draw_title_text("HI SCORE", 1, 53, 0x80 + (SAF_frame() & 0x7f));
        draw_title_text(t, 7, 59, 0x80 + (SAF_frame() & 0x7f));
      }
    }
  }

  frames_elapsed++;

  if (SAF_buttonJustPressed(SAF_BUTTON_B) && there_is_saved_game()) {
    clear_shown_score();
    reset_player();
    populate_all_levels();
    load_game();
    prepare_level();
    move_player_to_dolmen();
    transition_to_loop(back_to_dolmen_loop);
  } else if (SAF_buttonJustPressed(SAF_BUTTON_A)) {
    clear_shown_score();
    reset_player();
    populate_all_levels();
    if (frames_elapsed > SAF_FPS * SECONDS_SEQUENCE_2) {
      current_loop = pilgrim_out_loop;
    } else {
      curtain_to(descend_loop);
    }
    frames_elapsed = 0;
    step = 0;
    dots = 1500;
  } else if (SAF_buttonJustPressed(SAF_BUTTON_C)) {
    fade_to(help_loop_1);
  }
  return 1;
}

#define STARTING_POINT (-80)

uint8_t
approach_mosaic_loop(void) {
  static int8_t y = STARTING_POINT;
  static uint8_t current_piece = 0;
  static uint8_t current_step = 0;

  if (y < 0) {
    SAF_clearScreen(SAF_COLOR_BLACK);
    SAF_drawRect(0, y, 64, 80, FLOOR_COLOR, 1);
    seed_rnd(default_rng, 1);
    for (uint16_t i = 0; i < 500; i++) {
      SAF_drawPixel(rnd(default_rng) % 64, y + rnd(default_rng) % 80, FLOOR_HIGHLIGHT_COLOR);
    }
    SAF_drawImageCompressed(
        sacred_frame,
        32 - sacred_frame[0] / 2,
        y + 12,
        SAF_TRANSFORM_NONE,
        0x13
        );
    SAF_drawImage1Bit(
        walk_sequence[current_step],
        32 - WALK_SEQUENCE_WIDTH / 2,
        -y + 32,
        walk_sequence_mask[current_step],
        PLAYER_BODY_COLOR,
        PLAYER_EYES_COLOR,
        SAF_TRANSFORM_ROTATE_180);
    SAF_drawImage1Bit(
        walk_sequence[4],
        24 - WALK_SEQUENCE_WIDTH / 2,
        y + 56,
        walk_sequence_mask[4],
        PLAYER_BODY_COLOR,
        PLAYER_EYES_COLOR,
        SAF_TRANSFORM_ROTATE_270);
    SAF_drawImage1Bit(
        walk_sequence[4],
        40 - WALK_SEQUENCE_WIDTH / 2,
        y + 53,
        walk_sequence_mask[4],
        PLAYER_BODY_COLOR,
        PLAYER_EYES_COLOR,
        SAF_TRANSFORM_ROTATE_90);
    current_step = (y == -2) ? 4 : ((current_step + 1) % N_WALK_STEPS);
    y++;
    return 1;
  }

  if ((SAF_frame() % 5) == 0) {
    SAF_drawImage1Bit(
        pieces[current_piece],
        32 - pieces[current_piece][0] / 2,
        23 - pieces[current_piece][1] / 2,
        0,
        PIECE_BORDER_COLOR,
        PIECE_FILL_COLOR,
        SAF_TRANSFORM_NONE
        );
    current_piece++;
  }
  if (current_piece == 16) {
    y = STARTING_POINT;
    current_step = 0;
    current_piece = 0;
    transition_to_loop(mosaic_loop);
  }
#ifdef DEBUG
  if (SAF_buttonJustPressed(SAF_BUTTON_A)) {
    y = STARTING_POINT;
    current_step = 0;
    current_piece = 0;
  }
#endif
  return 1;
}

uint8_t
burial_loop(void) {
  static uint16_t n_sand_grains = 0;
  static uint16_t frames = 0;

  draw_floor();
  SAF_drawImage1Bit(
      puddle_image,
      2, 2,
      puddle_image,
      ORNAMENT_COLOR,
      ORNAMENT_COLOR,
      SAF_TRANSFORM_SCALE_3 | SAF_TRANSFORM_ROTATE_270);
  SAF_drawImage(
      skull_image,
      19, 20,
      SAF_TRANSFORM_NONE,
      SAF_COLOR_RED);
  SAF_drawImage(
      ribs_image,
      17, 20 + skull_image[1] + 2,
      SAF_TRANSFORM_NONE,
      SAF_COLOR_RED);
  rng_state_t r;
  seed_rnd(&r, 1);
  for (uint16_t i = 0; i < n_sand_grains; i++) {
    SAF_drawPixel( 13 + rnd(&r) % 22, 5 + rnd(&r) % 56, rnd(&r) % 2 ? FLOOR_COLOR : FLOOR_HIGHLIGHT_COLOR);
  }
  frames++;
  if(!((frames / (SAF_FPS / 2)) % 2)) {
    SAF_drawRect(
        25, 25, 20, 3, PLAYER_EYES_COLOR, 2);
  }
  if((frames % SAF_FPS) == 0) {
    SAF_playSound(SOUND_BURIAL);
    n_sand_grains+=500;
    if (n_sand_grains > 2500) {
      n_sand_grains = 0;
      frames = 0;
      curtain_to(ghost_loop);
    }
  }
  SAF_drawImage1Bit(
      orc04,
      32, 20,
      orc04_mask,
      PLAYER_EYES_COLOR,
      PLAYER_BODY_COLOR,
      SAF_TRANSFORM_SCALE_3 | (!((frames / (SAF_FPS / 2)) % 2) ? SAF_TRANSFORM_ROTATE_90 : SAF_TRANSFORM_ROTATE_180));
  return 1;
}

#define Y0_INIT (62)
uint8_t
ghost_loop(void) {
  static int8_t y0 = Y0_INIT;
  uint8_t thanks_colors[12] = {
    SAF_COLOR_BLUE,
    SAF_COLOR_BLUE,
    SAF_COLOR_BLUE,
    SAF_COLOR_BLUE,
    SAF_COLOR_BLUE,
    SAF_COLOR_BLUE,
    SAF_COLOR_BLUE,
    SAF_COLOR_BLUE_DARK,
    SAF_COLOR_BLACK,
    SAF_COLOR_BLACK,
    SAF_COLOR_BLACK,
    SAF_COLOR_BLUE_DARK,
  };
  static uint8_t thanks_color = 0;

  /* uint8_t wl[10] = { 5, 4, 3, 2, 1, 0, 1, 2, 3, 4}; */
  SAF_clearScreen(SAF_COLOR_BLACK);
  SAF_drawPixel(rnd(default_rng) % 64, rnd(default_rng) % 64, 5);
  SAF_drawPixel(rnd(default_rng) % 64, rnd(default_rng) % 64, 5);
  for (uint8_t y = 0; y < 64; y++) {
    int8_t x0 = /* wl[(SAF_frame() / 50) % 10]*/5 * SAF_sin((2 * SAF_frame() + 4 * y) % 0xff) / 128;
    for (uint8_t x = 0; x < 64; x++) {
      uint8_t c = ghost_image[2 + 64 * y + x];
      if (c) {
        SAF_drawPixel(x0 + x, y0 + y, c);
      }
    }
  }

  if ((SAF_frame() % 1) == 0) {
    char *t[6] = {"t", "h", "a", "n", "k", "s"};
    for (uint8_t i = 0; i < 6; i++) {
      SAF_drawText(t[i], (5 * SAF_sin((2 * SAF_frame()) % 0xff) / 128) + 9 + 9 * i, y0 + 52, thanks_colors[thanks_color] , 2);
    }
  }
  if ((SAF_frame() % 3) == 0) {
    thanks_color = (thanks_color + 1) % 12;
  }
  if ((SAF_frame() % 3) == 0) {
    --y0; if ((y0 < -64) || SAF_buttonJustPressed(SAF_BUTTON_A)) {
      y0 = Y0_INIT;
      thanks_color = 0;
      bury_corpse(player.puppet.x, player.puppet.y);
      fade_to(closeup_loop);
    }
  }
  return 1;
}
