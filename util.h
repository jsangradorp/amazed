/*
   A-Mazed

   Copyright (C) 2023  Julio Sangrador-Paton

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
   */

uint8_t transforms_table[4] = {
  SAF_TRANSFORM_NONE, /* DOWN */
  SAF_TRANSFORM_ROTATE_90, /* RIGHT */
  SAF_TRANSFORM_ROTATE_180, /* UP */
  SAF_TRANSFORM_ROTATE_270, /* LEFT */
};

void
set_pixel(uint8_t image[], int8_t x, int8_t y, uint8_t color) {
  if ((x < 0) || (x >= image[0]) || (y < 0) || (y >= image[1]))
    return;
  image[2 + image[0] * y + x] = color;
}

uint8_t
get_pixel(uint8_t image[], uint8_t x, uint8_t y) {
  return image[2 + image[0] * y + x];
}

/* Next function copied almost verbatim from SAF */
void
draw_circle(
    uint8_t image[],
    int8_t x,
    int8_t y,
    uint8_t radius,
    uint8_t color,
    uint8_t filled) {
  int8_t drawX = 0;
  int8_t drawY = radius;

  int16_t d = 3 - 2 * radius;

  if (!filled) {
    set_pixel(image, x, y + radius, color);
    set_pixel(image, x, y - radius, color);
    set_pixel(image, x + radius, y, color);
    set_pixel(image, x - radius, y, color);
  } else {
    for (int8_t i = x - radius; i <= x + radius; ++i) {
      set_pixel(image, i, y, color);
    }
  }

  while (drawY >= drawX) {
    if (d < 0) {
      d += 4 * drawX + 6;
    } else {
      d += 4 * (drawX - drawY) + 10;
      drawY--;
    }

    drawX++;

    int8_t xPlus  = x + drawX;
    int8_t xMinus = x - drawX;
    int8_t yPlus  = y + drawY;
    int8_t yMinus = y - drawY;
    int8_t x2Plus  = x + drawY;
    int8_t x2Minus = x - drawY;
    int8_t y2Plus  = y + drawX;
    int8_t y2Minus = y - drawX;

    if (!filled) {
      set_pixel(image, xPlus, yPlus, color);
      set_pixel(image, xPlus, yMinus, color);
      set_pixel(image, xMinus, yPlus, color);
      set_pixel(image, xMinus, yMinus, color);
      set_pixel(image, x2Plus, y2Plus, color);
      set_pixel(image, x2Plus, y2Minus, color);
      set_pixel(image, x2Minus, y2Plus, color);
      set_pixel(image, x2Minus, y2Minus, color);
    } else {
      for (int8_t i = xMinus; i <= xPlus; ++i) {
        set_pixel(image, i, yPlus, color);
        set_pixel(image, i, yMinus, color);
      }
      for (int8_t i = x2Minus; i <= x2Plus; ++i) {
        set_pixel(image, i, y2Plus, color);
        set_pixel(image, i, y2Minus, color);
      }
    }
  }
}

uint8_t
intersects(int16_t x1, int16_t y1, int16_t w1, int16_t h1, int16_t x2, int16_t y2, int16_t w2, int16_t h2) {
  return  !(
             (x1 > (x2 + w2))
          || ((x1 + w1) < x2)
          || (y1 > (y2 + h2))
          || ((y1 + h1) < y2)
          );
}

uint8_t
collides(uint8_t *image1, int16_t x1, int16_t y1, uint8_t *image2, int16_t x2, int16_t y2) {
  return intersects(x1 - image1[0] / 2, y1 - image1[1] / 2, image1[0], image1[1], x2 - image2[0] / 2, y2 - image2[1] / 2, image2[0], image2[1]);
}

direction_t
opposite_direction_to(direction_t d) {
  return (d + 2) % 4;
}

uint8_t
is_wall(uint8_t *image, int8_t x, int8_t y) {
  uint8_t c = get_pixel(image, x, y);
  return (c == T_WALL) || (c == T_SHOW_BACKGROUND);
}

uint8_t
is_void(uint8_t *image, int8_t x, int8_t y) {
  uint8_t c = get_pixel(image, x, y);
  return (c == T_VOID);
}

uint8_t
is_tile_empty_floor(uint8_t x, uint8_t y) {
  uint8_t tile_type = get_pixel(maze.underlying_structure, x, y);
  return
    (tile_type != T_WALL)
    && (tile_type != T_VOID)
    && (tile_type != T_SHOW_BACKGROUND)
    && (maze.info[x][y].contained_item == ITEM_NOTHING);
}

#define TRANSITION_OUT_FRAMES (5)
#define TRANSITION_IN_FRAMES (5)

uint8_t(*next_loop)(void);
uint32_t frames_transitioning = 0;
uint8_t transitioning_out = 0;
uint8_t transitioning_in = 0;
void(*transition_out_filter)(uint32_t);
void(*transition_in_filter)(uint32_t);

void
transition_to(uint8_t(*receiving_loop)(void), void(*out)(uint32_t), void(*in)(uint32_t)) {
  frames_transitioning = 0;
  next_loop = receiving_loop;
  transition_out_filter = out;
  transition_in_filter = in;
  transitioning_out = 1;
}

void
curtain_out(uint32_t transition_frame) {
  uint8_t step = (SAF_SCREEN_WIDTH / 2 / TRANSITION_OUT_FRAMES);
  for (uint8_t i = 0; i < (transition_frame * step); i++) {
    SAF_drawRect(i, i, SAF_SCREEN_WIDTH - 2 * i, SAF_SCREEN_WIDTH - 2 * i, SAF_COLOR_BLACK, 0);
  }
}

void
curtain_in(uint32_t transition_frame) {
  uint8_t step = (SAF_SCREEN_WIDTH / 2 / TRANSITION_IN_FRAMES);
  for (int8_t i = (TRANSITION_OUT_FRAMES + TRANSITION_IN_FRAMES - transition_frame) * step; i >= 0; i--) {
    SAF_drawRect(i, i, SAF_SCREEN_WIDTH - 2 * i, SAF_SCREEN_WIDTH - 2 * i, SAF_COLOR_BLACK, 0);
  }
}

void
curtain_to(uint8_t(*next_loop)(void)) {
  transition_to(next_loop, curtain_out, curtain_in);
}

#define BULK_PIXELS (600)
rng_state_t fade_rng_s;
rng_state_t *fade_rng = &fade_rng_s;

void
fade_out(uint32_t transition_frame) {
  uint8_t step = (SAF_SCREEN_WIDTH / 2 / TRANSITION_OUT_FRAMES);
  seed_rnd(fade_rng, 1);
  for (uint16_t i = 0; i < (transition_frame * step) * BULK_PIXELS; i++) {
    SAF_drawPixel(rnd(fade_rng) % 64, rnd(fade_rng) % 64, SAF_COLOR_BLACK);
  }
}

void
fade_in(uint32_t transition_frame) {
  uint8_t step = (SAF_SCREEN_WIDTH / 2 / TRANSITION_IN_FRAMES);
  seed_rnd(fade_rng, 1);
  for (int16_t i = (TRANSITION_OUT_FRAMES + TRANSITION_IN_FRAMES - transition_frame) * step * BULK_PIXELS; i >= 0; i--) {
    SAF_drawPixel(rnd(fade_rng) % 64, rnd(fade_rng) % 64, SAF_COLOR_BLACK);
  }
}

void
fade_to(uint8_t(*next_loop)(void)) {
  transition_to(next_loop, fade_out, fade_in);
}

void
reset_transitioning(void) {
    current_loop = next_loop;
    transitioning_out = 0;
    transitioning_in = 0;
    frames_transitioning = 0;
}

void
update_transition_out(void) {
  transition_out_filter(frames_transitioning);
  frames_transitioning += (frames_transitioning < TRANSITION_OUT_FRAMES);
  if ((frames_transitioning > 1) && SAF_buttonJustPressed(SAF_BUTTON_A)) {
    reset_transitioning();
  } else if (frames_transitioning >= TRANSITION_OUT_FRAMES) {
    transitioning_out = 0;
    transitioning_in = 1;
    current_loop = next_loop;
  }
}

void
update_transition_in(void) {
  transition_in_filter(frames_transitioning);
  frames_transitioning += frames_transitioning < (TRANSITION_OUT_FRAMES + TRANSITION_IN_FRAMES);
  if (SAF_buttonJustPressed(SAF_BUTTON_A)) {
    reset_transitioning();
  } else if (frames_transitioning >= (TRANSITION_OUT_FRAMES + TRANSITION_IN_FRAMES)) {
    transitioning_in = 0;
    frames_transitioning = 0;
  }
}

void
playSound(uint8_t sound) {
  SAF_playSound(sound);
}

uint8_t
anyButtonPressed(void) {
  for(uint8_t b = SAF_BUTTON_UP; b < SAF_BUTTONS; b++) {
    if(SAF_buttonPressed(b)) {
      return 1;
    }
  }
  return 0;
}
