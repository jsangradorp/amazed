/*
 * Copyright Edward Rosten 2008--2013. 
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND OTHER CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR OTHER CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * */

/*
 * Code copied by me, Julio Sangrador, with very small modifications
 * from https://github.com/edrosten/8bit_rng/blob/master/rng-4294967294.cc
 * */

typedef struct {
  uint8_t rng8_x;
  uint8_t rng8_y;
  uint8_t rng8_z;
  uint8_t rng8_a;
} rng_state_t;

uint8_t
rnd(rng_state_t *s) {
  uint8_t t = s->rng8_x ^ (s->rng8_x << 4);
  s->rng8_x = s->rng8_y;
  s->rng8_y = s->rng8_z;
  s->rng8_z = s->rng8_a;
  s->rng8_a = s->rng8_z ^ t ^ ( s->rng8_z >> 1) ^ (t << 1);

  return s->rng8_a;
}

void
seed_rnd(rng_state_t *s, uint32_t seed) {
  s->rng8_x = (seed >> 24) & 0xff;
  s->rng8_y = (seed >> 16) & 0xff;
  s->rng8_z = (seed >>  8) & 0xff;
  s->rng8_a = (seed >>  0) & 0xff;
}

rng_state_t default_rng_struct = {
  .rng8_x = 0,
  .rng8_y = 0,
  .rng8_z = 0,
  .rng8_a = 1,
};

rng_state_t *default_rng = &default_rng_struct;
