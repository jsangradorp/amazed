#define ORC_SEQUENCE_WIDTH (8)
#define ORC_SEQUENCE_HEIGHT (8)

uint8_t orc01[] = {
  ORC_SEQUENCE_WIDTH, ORC_SEQUENCE_HEIGHT,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
  /* 00100010 */ 34,
  /* 00111110 */ 62,
  /* 01110010 */ 114,
  /* 01111100 */ 124,
  /* 01001100 */ 76,
};

uint8_t orc01_mask[] = {
  ORC_SEQUENCE_WIDTH, ORC_SEQUENCE_HEIGHT,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
  /* 00100010 */ 34,
  /* 00111110 */ 62,
  /* 01111110 */ 126,
  /* 01111100 */ 124,
  /* 01001100 */ 76,
};

uint8_t orc02[] = {
  ORC_SEQUENCE_WIDTH, ORC_SEQUENCE_HEIGHT,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
  /* 00000010 */ 2,
  /* 00111110 */ 62,
  /* 01110010 */ 114,
  /* 01111100 */ 124,
  /* 01000100 */ 68,
  /* 00000000 */ 0,
};

uint8_t orc02_mask[] = {
  ORC_SEQUENCE_WIDTH, ORC_SEQUENCE_HEIGHT,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
  /* 00000010 */ 2,
  /* 00111110 */ 62,
  /* 01111110 */ 126,
  /* 01111100 */ 124,
  /* 01000100 */ 68,
  /* 00000000 */ 0,
};

uint8_t orc03[] = {
  ORC_SEQUENCE_WIDTH, ORC_SEQUENCE_HEIGHT,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
  /* 00111110 */ 62,
  /* 01110010 */ 114,
  /* 01111100 */ 124,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
};

uint8_t orc03_mask[] = {
  ORC_SEQUENCE_WIDTH, ORC_SEQUENCE_HEIGHT,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
  /* 00111110 */ 62,
  /* 01111110 */ 126,
  /* 01111100 */ 124,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
};

uint8_t orc04[] = {
  ORC_SEQUENCE_WIDTH, ORC_SEQUENCE_HEIGHT,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
  /* 01111110 */ 126,
  /* 01100110 */ 102,
  /* 00111100 */ 60,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
};

uint8_t orc04_mask[] = {
  ORC_SEQUENCE_WIDTH, ORC_SEQUENCE_HEIGHT,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
  /* 01111110 */ 126,
  /* 01111110 */ 126,
  /* 00111100 */ 60,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
};

uint8_t orc05[] = {
  ORC_SEQUENCE_WIDTH, ORC_SEQUENCE_HEIGHT,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
  /* 01111100 */ 124,
  /* 01001110 */ 78,
  /* 00111110 */ 62,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
};

uint8_t orc05_mask[] = {
  ORC_SEQUENCE_WIDTH, ORC_SEQUENCE_HEIGHT,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
  /* 01111100 */ 124,
  /* 01111110 */ 126,
  /* 00111110 */ 62,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
};

uint8_t orc06[] = {
  ORC_SEQUENCE_WIDTH, ORC_SEQUENCE_HEIGHT,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
  /* 01000000 */ 64,
  /* 01111100 */ 124,
  /* 01001110 */ 78,
  /* 00111110 */ 62,
  /* 00100010 */ 34,
  /* 00000000 */ 0,
};

uint8_t orc06_mask[] = {
  ORC_SEQUENCE_WIDTH, ORC_SEQUENCE_HEIGHT,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
  /* 01000000 */ 64,
  /* 01111100 */ 124,
  /* 01111110 */ 126,
  /* 00111110 */ 62,
  /* 00100010 */ 34,
  /* 00000000 */ 0,
};

uint8_t orc07[] = {
  ORC_SEQUENCE_WIDTH, ORC_SEQUENCE_HEIGHT,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
  /* 01000100 */ 68,
  /* 01111100 */ 124,
  /* 01001110 */ 78,
  /* 00111110 */ 62,
  /* 00110010 */ 50,
};

uint8_t orc07_mask[] = {
  ORC_SEQUENCE_WIDTH, ORC_SEQUENCE_HEIGHT,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
  /* 00000000 */ 0,
  /* 01000100 */ 68,
  /* 01111100 */ 124,
  /* 01111110 */ 126,
  /* 00111110 */ 62,
  /* 00100110 */ 50,
};

#define N_ORC_STEPS 18

uint8_t *orc_sequence[N_ORC_STEPS] = {
  orc01,
  orc01,
  orc02,
  orc03,
  orc04,
  orc05,
  orc06,
  orc07,
  orc07,
  orc07,
  orc07,
  orc06,
  orc05,
  orc04,
  orc03,
  orc02,
  orc01,
  orc01,
};

uint8_t *orc_sequence_mask[N_ORC_STEPS] = {
  orc01_mask,
  orc01_mask,
  orc02_mask,
  orc03_mask,
  orc04_mask,
  orc05_mask,
  orc06_mask,
  orc07_mask,
  orc07_mask,
  orc07_mask,
  orc07_mask,
  orc06_mask,
  orc05_mask,
  orc04_mask,
  orc03_mask,
  orc02_mask,
  orc01_mask,
  orc01_mask,
};

