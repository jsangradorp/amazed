#define WALK_SEQUENCE_WIDTH (8)
#define WALK_SEQUENCE_HEIGHT (8)

uint8_t walk_image_01[10] = {
0x08,0x08,0x00,0x00,0x00,0x00,0x00,0x0c,0x00,0x00};
uint8_t walk_mask_01[10] = {
0x08,0x08,0x00,0x00,0x00,0x62,0x7e,0x7e,0x7c,0x4c};
uint8_t walk_image_02[10] = {
0x08,0x08,0x00,0x00,0x00,0x00,0x0c,0x00,0x00,0x00};
uint8_t walk_mask_02[10] = {
0x08,0x08,0x00,0x00,0x02,0x7e,0x7e,0x7c,0x44,0x40};
uint8_t walk_image_03[10] = {
0x08,0x08,0x00,0x00,0x00,0x00,0x0c,0x00,0x00,0x00};
uint8_t walk_mask_03[10] = {
0x08,0x08,0x00,0x00,0x00,0x7e,0x7e,0x7c,0x40,0x00};
uint8_t walk_image_04[10] = {
0x08,0x08,0x00,0x00,0x00,0x18,0x00,0x00,0x00,0x00};
uint8_t walk_mask_04[10] = {
0x08,0x08,0x00,0x00,0x7e,0x7e,0x7c,0x40,0x00,0x00};
uint8_t walk_image_05[10] = {
0x08,0x08,0x00,0x00,0x00,0x00,0x30,0x00,0x00,0x00};
uint8_t walk_mask_05[10] = {
0x08,0x08,0x00,0x00,0x40,0x7c,0x7e,0x3e,0x00,0x00};
uint8_t walk_image_06[10] = {
0x08,0x08,0x00,0x00,0x00,0x00,0x30,0x00,0x00,0x00};
uint8_t walk_mask_06[10] = {
0x08,0x08,0x00,0x40,0x40,0x7c,0x7e,0x3e,0x22,0x00};
uint8_t walk_image_07[10] = {
0x08,0x08,0x00,0x00,0x00,0x00,0x00,0x30,0x00,0x00};
uint8_t walk_mask_07[10] = {
0x08,0x08,0x00,0x40,0x40,0x44,0x7c,0x7e,0x3e,0x32};

#define N_WALK_STEPS 18

uint8_t *walk_sequence[N_WALK_STEPS] = {
  walk_image_01,
  walk_image_01,
  walk_image_02,
  walk_image_03,
  walk_image_04,
  walk_image_05,
  walk_image_06,
  walk_image_07,
  walk_image_07,
  walk_image_07,
  walk_image_07,
  walk_image_06,
  walk_image_05,
  walk_image_04,
  walk_image_03,
  walk_image_02,
  walk_image_01,
  walk_image_01,
};

uint8_t walk_torch_delta_x = 2;

int8_t walk_torch_delta_y[N_WALK_STEPS] = {
1,
1,
0,
0,
-1,
1,
2,
3,
3,
3,
3,
2,
1,
-1,
0,
0,
1,
1,
};

uint8_t *walk_sequence_mask[N_WALK_STEPS] = {
  walk_mask_01,
  walk_mask_01,
  walk_mask_02,
  walk_mask_03,
  walk_mask_04,
  walk_mask_05,
  walk_mask_06,
  walk_mask_07,
  walk_mask_07,
  walk_mask_07,
  walk_mask_07,
  walk_mask_06,
  walk_mask_05,
  walk_mask_04,
  walk_mask_03,
  walk_mask_02,
  walk_mask_01,
  walk_mask_01,
};
