uint8_t piece00[15] = {
0x0a,0x0a,0xff,0xe0,0x39,0x26,0xdb,0x8b,0xe4,0xfb,0x1e,0x03,0xc0,0x7f,0xf0};
uint8_t piece01[15] = {
0x0a,0x0a,0xff,0xe1,0xf8,0x3e,0x0f,0x83,0xf0,0xfc,0x1f,0x87,0xf1,0xff,0xf0};
uint8_t piece02[15] = {
0x0a,0x0a,0xff,0xe0,0xd8,0x7e,0x1f,0x8f,0x63,0x98,0xe6,0x31,0x9c,0x7f,0xf0};
uint8_t piece03[15] = {
0x0a,0x0a,0xff,0xf0,0x19,0x26,0x6d,0x84,0x60,0x98,0xf6,0x71,0xbe,0xff,0xf0};
uint8_t piece04[15] = {
0x0a,0x0a,0xff,0xec,0x1f,0x87,0xf9,0xbf,0xe3,0xf8,0x0e,0x01,0x80,0x7f,0xf0};
uint8_t piece05[15] = {
0x0a,0x0a,0xff,0xec,0x39,0x8e,0x33,0x84,0x70,0x9f,0x0e,0x33,0x83,0xff,0xf0};
uint8_t piece06[15] = {
0x0a,0x0a,0xff,0xe6,0x19,0x0e,0x47,0xa3,0x69,0x9c,0x87,0x01,0xf3,0xff,0xf0};
uint8_t piece07[15] = {
0x0a,0x0a,0xff,0xfe,0x1e,0x07,0x01,0x80,0x60,0x38,0xff,0xff,0xff,0xff,0xf0};
uint8_t piece08[15] = {
0x0a,0x0a,0xff,0xff,0xff,0xff,0xf1,0xc0,0x60,0x18,0x0e,0x07,0x87,0xff,0xf0};
uint8_t piece09[15] = {
0x0a,0x0a,0xff,0xfc,0xf8,0x0e,0x13,0x99,0x6c,0x5e,0x27,0x09,0x86,0x7f,0xf0};
uint8_t piece10[15] = {
0x0a,0x0a,0xff,0xfc,0x1c,0xc7,0x0f,0x90,0xe2,0x1c,0xc7,0x19,0xc3,0x7f,0xf0};
uint8_t piece11[15] = {
0x0a,0x0a,0xff,0xe0,0x18,0x07,0x01,0xfc,0x7f,0xd9,0xfe,0x1f,0x83,0x7f,0xf0};
uint8_t piece12[15] = {
0x0a,0x0a,0xff,0xf7,0xd8,0xe6,0xf1,0x90,0x62,0x1b,0x66,0x49,0x80,0xff,0xf0};
uint8_t piece13[15] = {
0x0a,0x0a,0xff,0xe3,0x98,0xc6,0x71,0x9c,0x6f,0x1f,0x87,0xe1,0xb0,0x7f,0xf0};
uint8_t piece14[15] = {
0x0a,0x0a,0xff,0xf8,0xfe,0x1f,0x83,0xf0,0xfc,0x1f,0x07,0xc1,0xf8,0x7f,0xf0};
uint8_t piece15[15] = {
0x0a,0x0a,0xff,0xe0,0x3c,0x07,0x8d,0xf2,0x7d,0x1d,0xb6,0x49,0xc0,0x7f,0xf0};

uint8_t *pieces[16] = {
  piece00,
  piece01,
  piece02,
  piece03,
  piece04,
  piece05,
  piece06,
  piece07,
  piece08,
  piece09,
  piece10,
  piece11,
  piece12,
  piece13,
  piece14,
  piece15,
};
