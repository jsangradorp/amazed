
# Linux

In order to compile the SDL2 version on Linux, you need the SDL2 libraries.
For example, on Debian systems, `sudo apt-get install libsdl2-dev`.

Once you clone the repo you need to either 1) compile against your own
version of `saf.h`; or 2) clone the submodules tracked in the code.

To 1) compile against your own version of `saf.h` (pay attention to the
quoting, it is important):

```
git clone https://gitlab.com/jsangradorp/amazed.git amazed
cd amazed
cc \
  -DSAF_HEADER='"path/tp/your/saf.h"' \
  -DSAF_PROGRAM_NAME='"A-Mazed"' \
  -DSAF_PLATFORM_SDL2 \
  -o a-mazed a-mazed.c -lSDL2 \
  && ./a-mazed -h
```

On the other hand, to 2) clone the submodules tracked in the repository and
compile with the saf.h version originally used for development:

```
git clone https://gitlab.com/jsangradorp/amazed.git amazed
cd amazed
git submodule update --init --recursive
make run
```

The `SAF_init()` and `SAF_loop()` functions are towards the end of
`a-mazed.c`. From there on you just need to follow the track ;-)

# Android

## Preparations

### Install needed Android development libraries and tools

For Debian 12 Bullseye (my current development environment) :

```
sudo apt install default-jdk-headless adb unzip zip
mkdir ~/android-sdk
export ANDROID_HOME=~/android-sdk
printf "\nexport ANDROID_HOME=~/android-sdk\n" >> ~/.bashrc
cd ~/android-sdk/
wget https://dl.google.com/android/repository/commandlinetools-linux-10406996_latest.zip
unzip commandlinetools-linux-10406996_latest.zip
yes | $ANDROID_HOME/cmdline-tools/bin/sdkmanager --sdk_root=${ANDROID_HOME} --licenses
$ANDROID_HOME/cmdline-tools/bin/sdkmanager --sdk_root=${ANDROID_HOME} "build-tools;30.0.2" "cmake;3.10.2.4988404" "ndk;21.3.6528147" "patcher;v4" "platform-tools" "platforms;android-30" "tools" "system-images;android-30;default;x86_64"
```

#### Run the Android emulator

```
${ANDROID_HOME}/cmdline-tools/bin/avdmanager create avd -n android30 -k "system-images;android-30;default;x86_64"
sed -i -e 's/^hw.keyboard=no$/hw.keyboard=yes/' -e 's#android-sdk/##' ~/.android/avd/android30.avd/config.ini
${ANDROID_HOME}/emulator/emulator @android30
```

#### Alternatively, prepare your physical device

TODO

## Install the code, compile and run it

```
git clone https://gitlab.com/jsangradorp/amazed.git amazed
cd amazed
git submodule update --init --recursive
make FRONTEND="SRA" run
```
