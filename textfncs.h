uint8_t curCharIndex;

typedef uint8_t (*color_func_ptr)(unsigned char c, int8_t x, int8_t y, uint8_t color, uint8_t x_offset, uint8_t y_offset);

/* uint8_t
get_actual_color(unsigned char c, int8_t x, int8_t y, uint8_t color, uint8_t x_offset, uint8_t y_offset) {
  _SAF_UNUSED(c);
  _SAF_UNUSED(x);
  _SAF_UNUSED(y);
  _SAF_UNUSED(x_offset);
  _SAF_UNUSED(y_offset);

  return color;
} */

void
drawSingleCharacter(unsigned char c, int8_t x, int8_t y, uint8_t color, color_func_ptr color_func) {
  uint8_t chardata[2];
  SAF_getFontCharacter(c, chardata);
  for (uint8_t nibble = 0; nibble < 4; nibble++) {
    for (uint8_t bit = 0; bit < 4; bit++) {
      uint8_t pixel_on = chardata[nibble > 1] & (1 << (4 * (nibble % 2) + bit));
      if (pixel_on) {
        SAF_drawPixel(x + bit, y + nibble, color_func == 0 ? color : color_func(c, x, y, color, bit, nibble));
      }
    }
  }
}

void
drawTextTighter(const char *text, int8_t x, int8_t y, uint8_t color, color_func_ptr color_func) {
  curCharIndex = 0;
  for (; *text; x += 4, text++, curCharIndex++) {
    drawSingleCharacter(*text, x, y, color, color_func);
  }
}

void
drawCenteredTextTighter(const char *text, int8_t cx, int8_t y, uint8_t color, color_func_ptr color_func) {
  uint8_t l = 0;
  for (; *(text + l++););
  drawTextTighter(text, cx - 2 * --l, y, color, color_func);
}

void
drawOutLinedTextTighter(const char *text, int8_t x, int8_t y, uint8_t color, uint8_t outline_color) {
  for (int8_t xx = -1; xx < 2; xx++) {
    for (int8_t yy = -1; yy < 2; yy++) {
      if (xx || yy) {
        drawTextTighter(text, x + xx, y + yy, outline_color, 0);
      }
    }
  }
  drawTextTighter(text, x, y, color, 0);
}

void
drawOutlinedCenteredTextTighter(const char *text, int8_t cx, int8_t y, uint8_t color, uint8_t outline_color) {
  for (int8_t xx = -1; xx < 2; xx++) {
    for (int8_t yy = -1; yy < 2; yy++) {
      if (xx || yy) {
        drawCenteredTextTighter(text, cx + xx, y + yy, outline_color, 0);
      }
    }
  }
  drawCenteredTextTighter(text, cx, y, color, 0);
}

void
drawMultiLineText(const char *text, int8_t x, int8_t y, uint8_t color) {
  uint8_t max_per_line = (64 - x) / 4;
  const char *start = text;

  uint8_t cursor = 0;
  uint8_t space = 0;
  unsigned char c;

  do {
    while((c = _SAF_READ_CONST(start + cursor)) && (cursor < max_per_line)) {
      if (c == ' ') {
        space = cursor;
      } else if (c == '\n') {
        space = cursor;
        break;
      }
      cursor++;
    }
    if ((c == ' ') || (c == '\n') || (c == 0) || (space == 0)) {
      space = cursor;
    }
    for (cursor = 0; cursor < space; cursor++) {
      drawSingleCharacter(_SAF_READ_CONST(start + cursor), x + 4 * cursor + 1, y,                              SAF_COLOR_RGB(192, 192, 192), 0);
      drawSingleCharacter(_SAF_READ_CONST(start + cursor), x + 4 * cursor    , y, ((y + cursor) % 2) ? color : SAF_COLOR_RGB( 32,  0,  0), 0);
    }
    y += 5;
    start = start + space + ((c == ' ') || (c == '\n') || (space < max_per_line));
    cursor = 0;
    space = 0;
  } while (c);
}


