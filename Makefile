
APPTITLE = A-Mazed!
APPNAME = a-mazed
APPVERSION = v1.0beta
ORGNAME = org.jsangradorp

FRONTEND ?= SDL2
SRCS_EXTRA = assets.h \
						 assets/Amazed_image.h \
						 assets/amulet.h \
						 assets/amulet_mask.h \
						 assets/corpse.h \
						 assets/darkness.h \
						 assets/dolmen.h \
						 assets/foes_images.h \
						 assets/ghost.h \
						 assets/heart.h \
						 assets/heart_mask.h \
						 assets/illumination.h \
						 assets/level_map.h \
						 assets/level_map_mask.h \
						 assets/mosaic_indicator_image.h \
						 assets/orc.h \
						 assets/ornament_images.h \
						 assets/petagolon_image.h \
						 assets/petagolon_legs.h \
						 assets/pieces.h \
						 assets/pilgrim_image.h \
						 assets/ring.h \
						 assets/ring_mask.h \
						 assets/sacred_frame.h \
						 assets/sacred_mosaic.h \
						 assets/save_icon.h \
						 assets/scale_up.h \
						 assets/scale_up_mask.h \
						 assets/slash.h \
						 assets/stair_down.h \
						 assets/stair_up.h \
						 assets/stair_up_mask.h \
						 assets/title_image.h \
						 assets/torch.h \
						 assets/walk.h \
						 closeup.h \
						 cutscenes.h \
						 debug.h \
						 foes.h \
						 globals.h \
						 maze.h \
						 mosaic.h \
						 objects.h \
						 palette.h \
						 player.h \
						 rng-4294967294.c \
						 saveload.h \
						 util.h
include lib/Makefile
