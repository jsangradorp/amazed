#define BITS_PER_BYTE (8)
#define WORD_SIZE (12)

typedef struct {
  uint8_t *data;
} bitmap_t;

void
clear_bit(bitmap_t *bitmap, uint8_t bit) {
  bitmap->data[bit / BITS_PER_BYTE] &= ~(1 << bit % BITS_PER_BYTE);
}

void
set_bit(bitmap_t *bitmap, uint8_t bit) {
  bitmap->data[bit / BITS_PER_BYTE] |= 1 << bit % BITS_PER_BYTE;
}

void
assign_bit(bitmap_t *bitmap, uint8_t bit, uint8_t value) {
  if (value) {
    set_bit(bitmap, bit);
  } else {
    clear_bit(bitmap, bit);
  }
}

uint8_t
get_bit(bitmap_t *bitmap, uint8_t bit) {
  return !!(bitmap->data[bit / BITS_PER_BYTE] & 1 << bit % BITS_PER_BYTE);
}

void
set_word_at(bitmap_t *bitmap, uint8_t word_index, uint16_t value) {
  uint8_t first_bit = word_index * WORD_SIZE;
  for (uint8_t bit = 0; bit < WORD_SIZE; bit++) {
    assign_bit(bitmap, first_bit + bit, !!(value & 1 << bit));
  }
}

uint16_t
get_word_at(bitmap_t *bitmap, uint8_t word_index) {
  uint16_t value = 0;
  uint8_t first_bit = word_index * WORD_SIZE;
  for (uint8_t bit = 0; bit < WORD_SIZE; bit++) {
    value |= get_bit(bitmap, first_bit + bit) << bit;
  }
  return value;
}

  /*
   * byte 0:
   *  - dolmen level (4 bits)
   *  - player.has_amulet (1 bit)
   *  - player.has_ring (1 bit)
   *  - petagolon.is_dead (1 bit)
   *  - to signal there is something saved (1 bit)
   * byte 1:
   *  - player.puppet.hit_points_left (8 bits)
   * bytes 2-3:
   *  - player.pieces_carried (15 bits LSB-MSB)
   *  - player.has_torch (bit 15)
   * bytes 4-5:
   *  - player.level_maps_carried (14 bits)
   *  - FREE 2 bits
   * bytes 6-26: (168 bits)
   *  - remaining_foes 14 * (4 bits ants + 3 bits spiders + 3 bits scorpions + 2 bits orcs)
   * bytes 27-28: (16 bits)
   *  - score
   * bytes 29-30: (16 bits)
   *  - HIGHEST score
   * byte 31: (8 bits)
   *  - corpses_buried (8 bits)
   **/

#ifdef DEBUG
uint16_t total_level[N_LEVELS];
#endif

uint8_t
there_is_saved_game() {
  return !!(SAF_load(0) & (1 << 7));
}

void
save_game() {
  uint8_t bytes[32];
  bytes[0] =
    (player.depth & 0x0f)
    | (player.has_amulet << 4)
    | (player.has_ring << 5)
    | (petagolon.is_dead << 6)
    | (1 << 7) /* to signal there is something saved */;
  bytes[1] = player.puppet.hit_points_left;
  bytes[2] = player.pieces_carried & 0xff;
  bytes[3] = (player.pieces_carried >> 8) | (player.has_torch << 7);
  bytes[4] = player.level_maps_carried & 0xff;
  bytes[5] = player.level_maps_carried >> 8; /* Note I am not giving the 2 bits back */
  bitmap_t bitmap;
  bitmap.data = bytes + 6;
  for (uint8_t level = 1; level < 15; level++) {
    uint16_t level_foes =
        remaining_foes[level][FOE_GIANT_SCORPION] << 9
      | remaining_foes[level][FOE_GIANT_ANT] << 5
      | remaining_foes[level][FOE_GIANT_SPIDER] << 2
      | remaining_foes[level][FOE_ORC] << 0;
#ifdef DEBUG
    total_level[level] = level_foes;
#endif
    set_word_at(&bitmap, level - 1, level_foes); /* The WORD_SIZE (12) least significant bits of level_foes */
  }
  bytes[27] = player.score & 0xff;
  bytes[28] = player.score >> 8;
  bytes[29] = highest_score & 0xff;
  bytes[30] = highest_score >> 8;
  bytes[31] = player.corpses_buried;

  for (uint8_t i = 0; i < 32; i++) {
    SAF_save(i, bytes[i]);
  }
}

void
load_highest_score(void) {
  highest_score = SAF_load(29) | SAF_load(30) << 8;
}

void
save_highest_score(void) {
  SAF_save(29, highest_score & 0xff);
  SAF_save(30, highest_score >> 8);
}

void
load_game() {
  uint8_t bytes[32];

  for (uint8_t i = 0; i < 32; i++) {
    bytes[i] = SAF_load(i);
  }

  player.depth = bytes[0] & 0x0f;
  player.has_amulet = !!(bytes[0] & 1 << 4);
  player.has_ring = !!(bytes[0] & 1 << 5);
  petagolon.is_dead = !!(bytes[0] & 1 << 6);
  player.puppet.hit_points_left = bytes[1];
  player.has_torch = !!(bytes[3] & 1 << 7);
  player.pieces_carried = bytes[2] | (bytes[3] & 0x7f) << 8;
  player.level_maps_carried = bytes[4] | bytes[5] << 8;
  bitmap_t bitmap;
  bitmap.data = bytes + 6;
  for (uint8_t level = 1; level < 15; level++) {
    uint16_t level_foes = get_word_at(&bitmap, level - 1);
#ifdef DEBUG
    total_level[level] = level_foes;
#endif
    remaining_foes[level][FOE_GIANT_SCORPION] = level_foes >> 9;
    remaining_foes[level][FOE_GIANT_ANT] = level_foes >> 5 & 0x0f;
    remaining_foes[level][FOE_GIANT_SPIDER] = level_foes >> 2 & 0x07;
    remaining_foes[level][FOE_ORC] = level_foes >> 0 & 0x03;
  }
  player.score = bytes[27] | bytes[28] << 8;
  player.corpses_buried = bytes[31];
}

void
draw_dolmen_landscape(uint8_t flash) {
  SAF_clearScreen(GENERIC_FILL_COLOR);

  /* Floor */

  uint8_t j = 1;
  for (uint16_t i = 0; i < 32; i += j++) {
    SAF_drawLine(0, 32 + i, 63, 32 + i, GENERIC_BORDER_COLOR);
  }

  /* Background wall */

  SAF_drawRect(0, 0, SAF_SCREEN_WIDTH, 33, flash ? SAF_COLOR_GRAY : SAF_COLOR_GRAY_DARK, 1);

  /* Pilgrim */

  SAF_drawImageCompressed(
      pilgrim_image02,
      -12,
      10,
      SAF_TRANSFORM_NONE,
      0xe0
      );
  if(flash) {
    SAF_drawRect(1, 14, 9, 4, 0x24, 1);
  }

  /* Dolmen */

  SAF_drawImage1Bit(
      dolmen_image,
      17, 8,
      dolmen_mask,
      flash ? SAF_COLOR_WHITE : DOLMEN_FILL_COLOR,
      DOLMEN_BORDER_COLOR,
      SAF_TRANSFORM_SCALE_4
      );
}

void
move_player_to_dolmen(void) {
  uint8_t y;
  uint8_t x;

  for (y = 0; y < maze.height; y++) {
    for (x = 0; x < maze.width; x++) {
      if (maze.info[x][y].contained_item == ITEM_DOLMEN) {
        break;
      }
    }
    if (maze.info[x][y].contained_item == ITEM_DOLMEN) {
      break;
    }
  }

#ifdef DEBUG
  printf("Dolmen at: %u, %u\n\n", x, y);
  fflush(NULL);
#endif

  for (direction_t d = DOWN; d <= RIGHT; d++) {
    if(!is_wall(maze.underlying_structure, x + moves_table[d].dx, y + moves_table[d].dy)) {
      player.puppet.x = x + moves_table[d].dx;
      player.puppet.y = y + moves_table[d].dy;
      player.puppet.closeup.x = maze.tile_width / 2;
      player.puppet.closeup.y = maze.tile_height / 2;
      player.puppet.direction = d;
      player.puppet.closeup.current_image_index = 8;
      break;
    }
  }
#ifdef DEBUG
  printf("x: %d\ny: %d\ncloseup.x: %d\ncloseup.y: %d\ndirection: %d\ncurrent_image_index: %d\n",
      player.puppet.x,
      player.puppet.y,
      player.puppet.closeup.x,
      player.puppet.closeup.y,
      player.puppet.direction,
      player.puppet.closeup.current_image_index);
  fflush(NULL);
#endif
}

uint8_t
back_to_dolmen_loop(void) {
  static uint16_t frames = 0;

  draw_dolmen_landscape(0);

  SAF_drawRect(0, 0, 64, 64, SAF_COLOR_WHITE, 0);
  SAF_drawRect(1, 1, 62, 62, SAF_COLOR_WHITE, 0);
  SAF_drawRect(2, 2, 60, 60, SAF_COLOR_WHITE, 0);
  SAF_drawRect(3, 3, 58, 58, SAF_COLOR_WHITE, 0);
  SAF_drawLine(56, 5, 58, 5, SAF_COLOR_WHITE);
  SAF_drawLine(58, 5, 58, 7, SAF_COLOR_WHITE);
  SAF_drawLine(5, 56, 5, 58, SAF_COLOR_WHITE);
  SAF_drawLine(5, 58, 7, 58, SAF_COLOR_WHITE);

  frames++;

  if (frames <= 63) {
    SAF_drawRect(frames, 0, 64 - frames, 64, SAF_COLOR_BLACK, 1);
    SAF_drawLine(frames, 0, frames, 63, (frames % 2) ? SAF_COLOR_RED : SAF_COLOR_RED_DARK);
  }

  if (SAF_buttonJustPressed(SAF_BUTTON_A) || (frames >= 4 * SAF_FPS)) {
    frames = 0;
    current_loop = closeup_loop;
  }
  return 1;
}

#define DOLMEN_WAIT_FRAMES (9 * SAF_FPS)
#define FLASH_LIMIT (3 * SAF_FPS)
#define PHOTO_LIMIT (6 * SAF_FPS)
#define FULL_LIMIT (6 * SAF_FPS + 64)

#include "./assets/save_icon.h"

uint8_t
dolmen_crossed_loop(void) {
  static uint16_t frames = 0;

#define N_SAVE_ICON_COLORS (6)
  uint8_t color[N_SAVE_ICON_COLORS] = {
    SAF_COLOR_BLACK,
    SAF_COLOR_GRAY_DARK,
    SAF_COLOR_GRAY,
    SAF_COLOR_WHITE,
    SAF_COLOR_GRAY,
    SAF_COLOR_GRAY_DARK,
  };
  static uint8_t current_color = 0;

  uint8_t flash = (rnd(default_rng) < 32) && (frames < FLASH_LIMIT);

  draw_dolmen_landscape(flash);

  if (frames > FLASH_LIMIT) {
    SAF_drawRect(0, 0, 64, 64, SAF_COLOR_WHITE, 0);
    SAF_drawRect(1, 1, 62, 62, SAF_COLOR_WHITE, 0);
    SAF_drawRect(2, 2, 60, 60, SAF_COLOR_WHITE, 0);
    SAF_drawRect(3, 3, 58, 58, SAF_COLOR_WHITE, 0);
    SAF_drawLine(56, 5, 58, 5, SAF_COLOR_WHITE);
    SAF_drawLine(58, 5, 58, 7, SAF_COLOR_WHITE);
    SAF_drawLine(5, 56, 5, 58, SAF_COLOR_WHITE);
    SAF_drawLine(5, 58, 7, 58, SAF_COLOR_WHITE);
  }

  if ((frames > PHOTO_LIMIT) && (frames < FULL_LIMIT)) {
    SAF_drawLine(64 - (frames - PHOTO_LIMIT), 0, 64 - (frames - PHOTO_LIMIT), 63, (frames % 2) ? SAF_COLOR_GREEN : SAF_COLOR_GREEN_DARK);
  }

  if ((++frames > DOLMEN_WAIT_FRAMES) || SAF_buttonPressed(SAF_BUTTON_A)) {
    frames = 0;
    save_game();
    move_player_to_dolmen();
    fade_to_loop(closeup_loop);
  }
  SAF_drawImage1Bit(
      saveicon_image,
      48,
      1,
      saveicon_image,
      color[current_color],
      color[current_color],
      SAF_TRANSFORM_NONE);
  current_color = (current_color + (SAF_frame() % 3 == 0)) % N_SAVE_ICON_COLORS;
  return 1;
}

#ifdef DEBUG

void
fill_with_random_data(void) {
  player.depth = rnd(default_rng) % 0x0f;
  player.has_amulet = rnd(default_rng) % 2;
  player.has_ring = rnd(default_rng) % 2;
  petagolon.is_dead = rnd(default_rng) % 2;
  player.puppet.hit_points_left = rnd(default_rng);
  player.pieces_carried = rnd(default_rng) | rnd(default_rng) << 7;
  player.level_maps_carried = rnd(default_rng) | rnd(default_rng) << 8;
  player.score = rnd(default_rng) | rnd(default_rng) << 8;
  player.corpses_buried = rnd(default_rng);
  for (uint8_t level = 1; level < 15; level++) {
    remaining_foes[level][FOE_GIANT_SCORPION] = rnd(default_rng) % 8;
    remaining_foes[level][FOE_GIANT_ANT] = rnd(default_rng) % 16;
    remaining_foes[level][FOE_GIANT_SPIDER] = rnd(default_rng) % 8;
    remaining_foes[level][FOE_ORC] = rnd(default_rng) % 4;
    uint16_t level_foes =
        remaining_foes[level][FOE_GIANT_SCORPION] << 9
      | remaining_foes[level][FOE_GIANT_ANT] << 5
      | remaining_foes[level][FOE_GIANT_SPIDER] << 2
      | remaining_foes[level][FOE_ORC] << 0;
    total_level[level] = level_foes;
  }
}

void
dump_data(void) {
  printf("player.depth = %d\n", player.depth);
  printf("player.has_amulet = %d\n", player.has_amulet);
  printf("player.has_ring = %d\n", player.has_ring);
  printf("player.has_torch = %d\n", player.has_torch);
  printf("petagolon.is_dead = %d\n", petagolon.is_dead);
  printf("player.puppet.hit_points_left = %d\n", player.puppet.hit_points_left);
  printf("player.pieces_carried = %d\n", player.pieces_carried);
  printf("player.level_maps_carried = %d\n", player.level_maps_carried);
  printf("player.score = %d\n", player.score);
  printf("player.corpses_buried = %d\n", player.corpses_buried);
  printf("Level\tTotal\tScrps\tAnts\tSpdrs\tOrcs\n");
  for (uint8_t level = 1; level < 15; level++) {
    printf("%u\t%u\t%u\t%u\t%u\t%u\n",
        level,
        total_level[level],
        remaining_foes[level][FOE_GIANT_SCORPION],
        remaining_foes[level][FOE_GIANT_ANT],
        remaining_foes[level][FOE_GIANT_SPIDER],
        remaining_foes[level][FOE_ORC]);
  }
  printf("\n");
  fflush(NULL);
}

uint8_t
test_save_load_loop(void) {
  static uint8_t save_done = 0;
  static uint8_t load_done = 0;

  if (!save_done) {
    fill_with_random_data();
    printf("Random assignment done for save.\n\n");
    fflush(NULL);
    dump_data();
    save_game();
    printf("Save done.\n\n");
    fflush(NULL);
    save_done = 1;
  } else if (!load_done) {
    fill_with_random_data();
    printf("Random assignment done for load.\n\n");
    fflush(NULL);
    dump_data();
    load_game();
    printf("Load done.\n\n");
    fflush(NULL);
    dump_data();
    load_done = 1;
  }
  return 1;
}

#endif
