/*
   A-Mazed

   Copyright (C) 2023  Julio Sangrador-Paton

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
   */

#define N_AREAS (4)

void
draw_land(void) {
  SAF_clearScreen(SAND_BASE_COLOR);
  rng_state_t r;
  seed_rnd(&r, 1);
  for (uint8_t i = 0; i < 85; i++) {
    SAF_drawPixel(
        rnd(&r) % SAF_SCREEN_WIDTH,
        rnd(&r) % SAF_SCREEN_HEIGHT,
        SAND_HIGHLIGHT_COLOR);
  }
}

void
draw_life_indicator(void) {
  uint8_t colors[5] = {
    SAF_COLOR_GREEN,
    SAF_COLOR_YELLOW,
    SAF_COLOR_ORANGE,
    SAF_COLOR_RED,
    SAF_COLOR_RED_DARK,
  };

  uint8_t y0 = 12;

  int8_t health_level = 9 -
    9 * (player.puppet.hit_points_left - 1) / PLAYER_HIT_POINTS;

  if (health_level < 7) {
  SAF_drawRect(
      SAF_SCREEN_WIDTH - 10,
      y0 - 11 + health_level,
      8,
      7 - health_level,
      colors[health_level / 2],
      1);
  }
  if (health_level < 10) {
  SAF_drawRect(
      SAF_SCREEN_WIDTH - 8,
      y0 - 4 + (health_level < 7 ? 0 : (health_level - 7)),
      4,
      health_level < 7 ? 2 : 2 - (health_level - 7),
      colors[health_level / 2],
      1);
  }
  SAF_drawImage1Bit(
      heart_image,
      53,
      1,
      heart_mask,
      HEART_FILL_COLOR,
      HEART_BORDER_COLOR,
      SAF_TRANSFORM_NONE);
}

void
draw_level_indicator(void) {
  SAF_drawLine(3, 58, 3, 60, SAF_COLOR_WHITE);
  SAF_drawLine(1, 60, 3, 62, SAF_COLOR_WHITE);
  SAF_drawLine(5, 60, 3, 62, SAF_COLOR_WHITE);

  char level_text[12];
  drawTextTighter(SAF_intToStr(player.depth, level_text), 7, 59, SAF_COLOR_WHITE, 0);
}

void
draw_pieces_indicator(void) {
  uint8_t y0 = 59;

  for (uint8_t y = 0; y < 4; y++) {
    for (uint8_t x = 0; x < 4; x++) {
      SAF_drawPixel(59 + x, y0 + y, (player.pieces_carried & (1 << (4 * y + x))) ? mosaic_indicator_image[2 + 4 * y + x] : 16 * x + 16 * y);
    }
  }
  uint8_t pieces = count_pieces_carried();
  char c[12];
  drawTextTighter(SAF_intToStr(pieces, c), 55 - 4 * (pieces > 9), y0, SAF_COLOR_WHITE, 0);
}

void
draw_map_indicator(void) {
  if (player.level_maps_carried & (1 << player.depth)) {
    SAF_drawImage1Bit(
      level_map_image,
      1,
      1,
      level_map_mask,
      player.wants_level_map ? LEVEL_MAP_FILL_COLOR_ACTIVE : LEVEL_MAP_FILL_COLOR,
      player.wants_level_map ? LEVEL_MAP_BORDER_COLOR_ACTIVE : LEVEL_MAP_BORDER_COLOR,
      SAF_TRANSFORM_NONE);
  }
}

void
draw_amulet_indicator(void) {
  if (player.has_amulet) {
    SAF_drawImage1Bit(
      amulet_image,
      1,
      47,
      amulet_mask,
      player.wants_level_map ? LEVEL_MAP_FILL_COLOR_ACTIVE : AMULET_FILL_COLOR,
      player.wants_level_map ? LEVEL_MAP_BORDER_COLOR_ACTIVE : AMULET_BORDER_COLOR,
      /* AMULET_FILL_COLOR, */
      /* AMULET_BORDER_COLOR, */
      SAF_TRANSFORM_NONE);
  }
}

void
draw_ring_indicator(void) {
  if (player.has_ring) {
    SAF_drawImage1Bit(
      ring_image,
      53,
      48,
      ring_mask,
      RING_FILL_COLOR,
      RING_BORDER_COLOR,
      SAF_TRANSFORM_NONE);
    SAF_drawPixel(60,50, SAF_COLOR_WHITE);
    SAF_drawPixel(55,54, SAF_COLOR_WHITE);
  }
}

void
draw_torch_indicator(void) {
  uint8_t lit_torch = is_torch_lit();
  if (player.has_torch) {
    SAF_drawImage1Bit(
      torch_image,
      12,
      1,
      torch_mask,
      lit_torch ? SAF_COLOR_RED : SAF_COLOR_GRAY_DARK,
      TORCH_BORDER_COLOR,
      SAF_TRANSFORM_NONE);
    SAF_drawPixel(12 + 6, 1 + 3, lit_torch ? TORCH_FILL_COLOR : SAF_COLOR_BLACK);
    SAF_drawPixel(12 + 7, 1 + 2, lit_torch ? TORCH_FILL_COLOR : SAF_COLOR_BLACK);
  }
}

uint16_t shown_score;
uint8_t score_color;

void
clear_shown_score(void) {
  shown_score = 0;
  score_color = 0;
}

#define N_SCORE_COLORS (6)
uint8_t score_colors[N_SCORE_COLORS] = {
  SAF_COLOR_WHITE,
  SAF_COLOR_GRAY,
  SAF_COLOR_GRAY_DARK,
  SAF_COLOR_BLACK,
  SAF_COLOR_GRAY_DARK,
  SAF_COLOR_GRAY,
};

void
draw_score(void) {
  if (shown_score < player.score) {
    shown_score += (player.score - shown_score) / 8 + 1;
    score_color = (score_color + 1 * ((SAF_frame() % 3) == 0)) % N_SCORE_COLORS;
  } else {
    score_color = (score_color + (score_color != 0) * ((SAF_frame() % 3) == 0)) % N_SCORE_COLORS;
  }

  char t[6] = "00000";
  uint32_t divisor = 100000;
  for (uint8_t i = 0; i < 5; i++) {
    t[i] = '0' + shown_score % divisor / (divisor / 10);
    divisor /= 10;
  }
  drawOutlinedCenteredTextTighter(t, 32, 58, score_colors[score_color], SAF_COLOR_BLACK);
}

void
draw_burials_indicator(void) {
  if (player.corpses_buried == 0) {
    return;
  }
  SAF_drawImage(
      skull_image,
      23,
      2,
      SAF_TRANSFORM_NONE,
      SAF_COLOR_RED);
  char t[12];
  drawTextTighter(
      SAF_intToStr(player.corpses_buried, t),
      32,
      4,
      SAF_COLOR_WHITE,
      0);
}

void
draw_maze(void) {
  int8_t ox = (SAF_SCREEN_WIDTH - maze.width) / 2;
  int8_t oy = (SAF_SCREEN_HEIGHT - maze.height) / 2;

  draw_land();

  if (
      ((player.level_maps_carried & (1 << player.depth)) && player.wants_level_map)
#ifdef DEBUG
      || (show_debug_overlay)
#endif
     ) {
    SAF_drawImage(
        maze.underlying_structure,
        (SAF_SCREEN_WIDTH - maze.width) / 2,
        (SAF_SCREEN_HEIGHT - maze.height) / 2,
        SAF_TRANSFORM_NONE,
        T_SHOW_BACKGROUND);
  } else {
    SAF_drawImage(
        maze.known_parts,
        (SAF_SCREEN_WIDTH - maze.width) / 2,
        (SAF_SCREEN_HEIGHT - maze.height) / 2,
        SAF_TRANSFORM_NONE,
        T_SHOW_BACKGROUND);
  }
  /* Show known items */
  for (uint8_t yy = 0; yy < maze.height; yy++) {
    for (uint8_t xx = 0; xx < maze.width; xx++) {
      if (
          (maze.info[xx][yy].contained_item == ITEM_LEVEL_MAP)
          && (
            (get_pixel(maze.known_parts, xx, yy) != T_SHOW_BACKGROUND)
            || (player.has_amulet && player.wants_level_map)
#ifdef DEBUG
            || (show_debug_overlay)
#endif
            )
          ) {
        SAF_drawPixel(ox + xx, oy + yy, MAZE_COLOR_MAP); /* Show known items */
      }
      if (
          (maze.info[xx][yy].contained_item == ITEM_AMULET)
          && (
            (get_pixel(maze.known_parts, xx, yy) != T_SHOW_BACKGROUND)
#ifdef DEBUG
            || (show_debug_overlay)
#endif
            )
          ) {
        SAF_drawPixel(ox + xx, oy + yy, MAZE_COLOR_AMULET); /* Show known items */
      }
      if (
          (maze.info[xx][yy].contained_item == ITEM_RING)
          && (
            (get_pixel(maze.known_parts, xx, yy) != T_SHOW_BACKGROUND)
            || (player.has_amulet && player.wants_level_map)
#ifdef DEBUG
            || (show_debug_overlay)
#endif
            )
          ) {
        SAF_drawPixel(ox + xx, oy + yy, MAZE_COLOR_RING); /* Show known items */
      }
      if (
          (maze.info[xx][yy].contained_item == ITEM_MOSAIC_PIECE)
          && (
            (get_pixel(maze.known_parts, xx, yy) != T_SHOW_BACKGROUND)
            || (player.has_amulet && player.wants_level_map)
#ifdef DEBUG
            || (show_debug_overlay)
#endif
            )
          ) {
        SAF_drawPixel(ox + xx, oy + yy, MAZE_COLOR_PIECE); /* Show known items */
      }
      if (
          (maze.info[xx][yy].contained_item == ITEM_DOLMEN)
          && (
            (get_pixel(maze.known_parts, xx, yy) != T_SHOW_BACKGROUND)
            || ((player.has_amulet || (player.level_maps_carried & (1 << player.depth))) && player.wants_level_map)
#ifdef DEBUG
            || (show_debug_overlay)
#endif
            )
          ) {
        SAF_drawPixel(ox + xx, oy + yy, MAZE_COLOR_DOLMEN); /* Show dolmens */
      }
#ifdef DEBUG
      if (maze.info[xx][yy].contained_item == ITEM_CORPSE) {
        SAF_drawPixel(ox + xx, oy + yy, SAF_COLOR_YELLOW); /* Show corpses locations, only in DEBUG mode */
      }
#endif
    }
  }

  /* Show foes (including worms) */
  for (uint8_t i = 0; i < N_FOES; i++) {
    if (
        foes[i].puppet.hit_points_left
        && (
          /* (get_pixel(maze.known_parts, foes[i].puppet.x, foes[i].puppet.y) != T_SHOW_BACKGROUND)
          || */ (player.has_amulet && player.wants_level_map)
#ifdef DEBUG
          || (show_debug_overlay)
#endif
          )
       ) {
      SAF_drawPixel(ox + foes[i].puppet.x, oy + foes[i].puppet.y, MAZE_COLOR_FOE);
    }
  }

  /* Show entrance and exit */

  if (((SAF_frame() / 10) % 2) == 0) {
    if (
    (get_pixel(maze.known_parts, maze.xf, maze.yf) != T_SHOW_BACKGROUND)
    || ((player.level_maps_carried & (1 << player.depth)) && player.wants_level_map)
#ifdef DEBUG
    || show_debug_overlay
#endif
    ) {
      if (player.depth != N_LEVELS) {
        SAF_drawPixel(ox + maze.xf, oy + maze.yf, T_EXIT); /* Blink exit */
      }
    }
    if (
    (get_pixel(maze.known_parts, maze.xs, maze.ys) != T_SHOW_BACKGROUND)
    || ((player.level_maps_carried & (1 << player.depth)) && player.wants_level_map)
#ifdef DEBUG
    || show_debug_overlay
#endif
    ) {
      SAF_drawPixel(ox + maze.xs, oy + maze.ys, T_ENTRANCE); /* Blink entrance */
    }
  }

  /* Show blinking player */

  if (player.just_teleported) {
    SAF_drawPixel(ox + player.puppet.x - 1, oy + player.puppet.y, SAF_COLOR_WHITE);
    SAF_drawPixel(ox + player.puppet.x, oy + player.puppet.y - 1, SAF_COLOR_WHITE);
    SAF_drawPixel(ox + player.puppet.x + 1, oy + player.puppet.y, SAF_COLOR_WHITE);
    SAF_drawPixel(ox + player.puppet.x, oy + player.puppet.y + 1, SAF_COLOR_WHITE);
    //SAF_drawPixel(ox + player.puppet.x, oy + player.puppet.y, SAF_COLOR_WHITE);
    --player.just_teleported;
  } else if (((2 * (SAF_frame() - maze.last_move_frame) / SAF_FPS) % 2) == 0) {
    SAF_drawPixel(ox + player.puppet.x, oy + player.puppet.y, T_PLAYER);
  }
}

void
draw_other_indicators(void) {
    draw_level_indicator();
    draw_pieces_indicator();
    draw_map_indicator();
    draw_amulet_indicator();
    draw_ring_indicator();
    draw_torch_indicator();
    draw_burials_indicator();
}

void
init_maze(void) {
  uint8_t ox;
  uint8_t oy;
  uint8_t r;

  maze.path_length = 0;
  maze.path_max_length = 0;
  maze.last_move_frame = 0;
  maze.width = MAX_MAZE_WIDTH - (player.depth - 1) * ((MAX_MAZE_WIDTH - MIN_MAZE_WIDTH) / (N_LEVELS - 1));
  maze.height = MAX_MAZE_HEIGHT - (player.depth - 1) * ((MAX_MAZE_HEIGHT - MIN_MAZE_HEIGHT) / (N_LEVELS - 1));
  ox = (maze.width / 2 - 1);
  oy = (maze.height / 2 - 1);
  r = (maze.width / 2 - 3);
  maze.tile_width = maze.tile_height = (player.depth == N_LEVELS) ? 1 * WALK_SEQUENCE_WIDTH + 2 : (6 - 2 * ((player.depth - 1)/ 5)) * WALK_SEQUENCE_WIDTH;
  maze.underlying_structure[0] = maze.known_parts[0] = maze.width;
  maze.underlying_structure[1] = maze.known_parts[1] = maze.height;

  /* Fill all maze with transparent color and no item */
  for (uint8_t x = 0; x < maze.width; x++) {
    for (uint8_t y = 0; y < maze.height; y++) {
      set_pixel(maze.underlying_structure, x, y, T_SHOW_BACKGROUND);
      set_pixel(maze.known_parts, x, y, T_SHOW_BACKGROUND);
      maze.info[x][y].contained_item = ITEM_NOTHING;
      maze.info[x][y].ornament = ORNAMENT_NOTHING;
      maze.info[x][y].tried_directions = 0;
    }
  }

  /* Make game board black (all walls) */
  draw_circle(maze.underlying_structure, ox, oy, r, T_WALL, 1);
  draw_circle(maze.underlying_structure, ox - 1, oy, r, T_WALL, 1);
  draw_circle(maze.underlying_structure, ox + 1, oy, r, T_WALL, 1);
  draw_circle(maze.underlying_structure, ox, oy - 1, r, T_WALL, 1);
  draw_circle(maze.underlying_structure, ox, oy + 1, r, T_WALL, 1);

  /* Fill the inside of the level with corridor */
  draw_circle(maze.underlying_structure, ox, oy, r + 1, player.depth == N_LEVELS ? T_CORRIDOR : T_WALL, 1);
  if (player.depth == N_LEVELS) {
    /* Make deepest level fully visible */
    draw_circle(maze.known_parts, ox, oy, r + 1, T_CORRIDOR, 1);
  }

  /* Create an impenetrable circular wall */
  draw_circle(maze.underlying_structure, ox, oy, r, T_CORRIDOR, 0);
  draw_circle(maze.underlying_structure, ox - 1, oy, r, T_CORRIDOR, 0);
  draw_circle(maze.underlying_structure, ox + 1, oy, r, T_CORRIDOR, 0);
  draw_circle(maze.underlying_structure, ox, oy - 1, r, T_CORRIDOR, 0);
  draw_circle(maze.underlying_structure, ox, oy + 1, r, T_CORRIDOR, 0);

  draw_circle(maze.underlying_structure, ox, oy, r + 1, T_WALL, 0);

  for (uint8_t x = 0; x < maze.width; x++) {
    for (uint8_t y = 0; y < maze.height; y++) {
      if (get_pixel(maze.underlying_structure, x, y) == T_CORRIDOR) {
        maze.info[x][y].tried_directions = 15; /* All directions blocked */
      }
    }
  }

  /* Initialize important variables */

  /* Ensure starting point inside the level */
  do {
    player.puppet.x = maze.xs = maze.xf =
      2 + 2 * (SAF_random() % ((maze.width - 2) / 2));
    player.puppet.y = maze.ys = maze.yf =
      2 + 2 * (SAF_random() % ((maze.height - 2) / 2));
  } while (
      SAF_sqrt(
        (player.puppet.x - ox) * (player.puppet.x - ox) + (player.puppet.y - oy) * (player.puppet.y - oy))
      >= (r - 2));
  maze.path_max_length = maze.path_length = 0;
  maze.info[maze.xs][maze.ys].backtrack_move = (move_t) { .dx = 0, .dy = 0}; /* Probably unneeded */
}

void
setup_deepest_level_area(void) {
  for (uint8_t y = 0; y < maze.height; y++) {
    for (uint8_t x = 0; x < maze.width; x++) {
      if (get_pixel(maze.underlying_structure, x, y) == T_CORRIDOR) {
        maze.info[x][y].area = 3;
      }
    }
  }
}

void
calculate_areas(void) {

  if (player.depth == N_LEVELS) {
    setup_deepest_level_area();
    return;
  }

  move_t move;
  uint8_t must_backtrack;
  direction_t d;
  uint8_t x, y;

  x = maze.xs;
  y = maze.ys;
  maze.info[x][y].backtrack_move = (move_t) { .dx = 0, .dy = 0}; /* Probably unneeded */
  maze.path_length = 0;

  for (uint8_t yy = 0; yy < maze.height; yy++) {
    for (uint8_t xx = 0; xx < maze.width; xx++) {
      maze.info[xx][yy].tried_directions = 0;
    }
  }

  do {
    d = RIGHT;
    must_backtrack = 0;
    do {
      if (maze.info[x][y].tried_directions == 15) {
        /* All directions tried and failed */
        must_backtrack = 1;
      } else {
        /* Choose a new direction to try (there must be at least one, as per the if)... */
        do {
          d = (d + 1) % 4;
        } while ((maze.info[x][y].tried_directions & (1 << d)) != 0);
        move.dx = moves_table[d].dx;
        move.dy = moves_table[d].dy;
        /* ... and note it down */
        maze.info[x][y].tried_directions |= (1 << d);
      }
    } while (
        !must_backtrack
        && (
          is_wall(maze.underlying_structure, x + move.dx, y + move.dy)
          || (
            (move.dx == maze.info[x][y].backtrack_move.dx)
            && (move.dy == maze.info[x][y].backtrack_move.dy)
            )
          )
        );

    if (must_backtrack) {
      move.dx = maze.info[x][y].backtrack_move.dx;
      move.dy = maze.info[x][y].backtrack_move.dy;
      maze.path_length--;
    } else {
      maze.info[x + move.dx][y + move.dy].backtrack_move.dx = -move.dx;
      maze.info[x + move.dx][y + move.dy].backtrack_move.dy = -move.dy;
      maze.path_length++;
    }

    x += move.dx;
    y += move.dy;

    uint8_t color = N_AREAS * (maze.path_length - 1) / maze.path_max_length;
    maze.info[x][y].area = color;
    set_pixel(maze.underlying_structure, x, y, area_colors[color]);
  } while(!((x == maze.xs) && (y == maze.ys)));
}

uint8_t
is_valid_island_position(uint8_t x, uint8_t y) {
  uint8_t ox = (maze.width / 2 - 1);
  uint8_t oy = (maze.height / 2 - 1);
  uint8_t r = (maze.width / 2 - 3);

  return is_wall(maze.underlying_structure, x, y)
    && (SAF_sqrt((x - ox)*(x - ox) + (y - oy)*(y - oy)) <= r)
    && ((!is_wall(maze.underlying_structure, x - 1, y) && !is_wall(maze.underlying_structure, x + 1, y))
        || (!is_wall(maze.underlying_structure, x, y - 1) && !is_wall(maze.underlying_structure, x, y + 1)));
}

#define MAX_ISLANDS (50)
#define MAX_TRIES (64)

void
add_islands(void) {
  uint8_t i;
  uint8_t x, y;

  maze.n_islands = player.depth > (N_LEVELS - 3) ? 0 : (MAX_ISLANDS - 3 * player.depth);
#ifdef DEBUG
  maze.actual_islands = 0;
#endif
  for (i = 0; i < maze.n_islands; i++) {
    uint8_t n_tries = 0;
    do {
      x = 2 + SAF_random() % (maze.width - 5);
      y = 2 + SAF_random() % (maze.height - 5);
    } while (!is_valid_island_position(x, y) && (++n_tries < MAX_TRIES));
    if (is_valid_island_position(x, y)) {
#ifdef DEBUG
      maze.actual_islands++;
#endif
      uint8_t area = 0;
      for (direction_t d = DOWN; d <= RIGHT; d++) {
        if (maze.info[x + moves_table[d].dx][y + moves_table[d].dy].area > area) {
          area = maze.info[x + moves_table[d].dx][y + moves_table[d].dy].area;
        }
      }
      set_pixel(maze.underlying_structure, x, y, area_colors[area]);
      maze.info[x][y].area = area;
    }
  }
}

#undef MAX_TRIES

void
init_petagolon(void) {
  petagolon.remaining_frames_on_ground = 0;
  petagolon.destroyed_tiles = 0;
  petagolon.foot_height = 12;
  petagolon.is_dead = 0;
}

void
finish_maze(void) {
  uint8_t ox = (maze.width / 2 - 1);
  uint8_t oy = (maze.height / 2 - 1);
  uint8_t r = (maze.width / 2 - 3);

  /* Fill scaffolding corridors with walls */
  draw_circle(maze.underlying_structure, ox, oy, r, T_WALL, 0);
  draw_circle(maze.underlying_structure, ox - 1, oy, r, T_WALL, 0);
  draw_circle(maze.underlying_structure, ox + 1, oy, r, T_WALL, 0);
  draw_circle(maze.underlying_structure, ox, oy - 1, r, T_WALL, 0);
  draw_circle(maze.underlying_structure, ox, oy + 1, r, T_WALL, 0);

  /* Set up and re-orientate stairs if needed */
  maze.info[maze.xs][maze.ys].contained_item = ITEM_ENTRANCE;
  maze.info[maze.xf][maze.yf].contained_item = ITEM_EXIT;

  maze.stair_down_direction = DOWN;
  maze.stair_up_direction = UP;
  for (direction_t d = DOWN; d <= RIGHT; d++) {
    if (!is_wall(maze.underlying_structure, maze.xf + moves_table[d].dx, maze.yf + moves_table[d].dy)) {
      maze.stair_down_direction = opposite_direction_to(d);
    }
    if (!is_wall(maze.underlying_structure, maze.xs + moves_table[d].dx, maze.ys + moves_table[d].dy)) {
      maze.stair_up_direction = opposite_direction_to(d);
    }
  }
  calculate_areas(); /* Must do this before adding islands, because then I'll be
                        unable to walk all the maze */
  if (player.depth == N_LEVELS) { /* Setup petagolon level */
    /* Remove stair down */
    set_pixel(maze.underlying_structure, maze.xf, maze.yf, T_CORRIDOR);
    maze.info[maze.xf][maze.yf].contained_item = ITEM_NOTHING;
    maze.info[maze.xf][maze.yf].area = 3;
    maze.xf = maze.yf = -1;

    /* Level specific stair up for deepest level */
    maze.xs = maze.width / 2;
    maze.ys = maze.height - 6;
    maze.info[maze.xs][maze.ys].contained_item = ITEM_ENTRANCE;
    maze.info[maze.xs][maze.ys].area = 3;
    maze.stair_up_direction = RIGHT;
    set_pixel(maze.underlying_structure, maze.xs, maze.ys, T_ENTRANCE);
    if (!petagolon.is_dead) {
      init_petagolon();
    }
  }
  add_islands();
  add_objects();
  add_foes();
}

void
build_maze(void) {

  if (player.depth == N_LEVELS) {
    return;
  }

  move_t move;
  uint8_t must_backtrack;
  direction_t d;
  uint8_t x, y;

  x = maze.xs;
  y = maze.ys;

  for (uint8_t yy = 0; yy < maze.height; yy++) {
    for (uint8_t xx = 0; xx < maze.width; xx++) {
      maze.info[xx][yy].tried_directions = 0;
    }
  }

  do {
    must_backtrack = 0;
    do {
      if (maze.info[x][y].tried_directions == 15) {
        /* All directions tried and failed */
        must_backtrack = 1;
      } else {
        do {
          d = rnd(default_rng) % 4;
        } while ((maze.info[x][y].tried_directions & (1 << d)) != 0);
        /* ... and note it down */
        move.dx = moves_table[d].dx;
        move.dy = moves_table[d].dy;
        maze.info[x][y].tried_directions |= (1 << d);
      }
    } while (
        !must_backtrack
        && (
          !is_wall(maze.underlying_structure, x + 2 * move.dx, y + 2 * move.dy)
          || (
            (move.dx == maze.info[x][y].backtrack_move.dx)
            && (move.dy == maze.info[x][y].backtrack_move.dy)
            )
          )
        );

    if (must_backtrack) {
      move.dx = maze.info[x][y].backtrack_move.dx;
      move.dy = maze.info[x][y].backtrack_move.dy;
      maze.path_length-=2;
    } else {
      maze.info[x + 2 * move.dx][y + 2 * move.dy].backtrack_move.dx = -move.dx;
      maze.info[x + 2 * move.dx][y + 2 * move.dy].backtrack_move.dy = -move.dy;
      maze.path_length+=2;
    }

    set_pixel(maze.underlying_structure, x, y, T_CORRIDOR);
    x += move.dx;
    y += move.dy;
    set_pixel(maze.underlying_structure, x, y, T_CORRIDOR);
    x += move.dx;
    y += move.dy;
    set_pixel(maze.underlying_structure, x, y, T_CORRIDOR);

    if ( maze.path_length > maze.path_max_length ) {
      maze.path_max_length = maze.path_length;
      maze.xf = x;
      maze.yf = y;
    }
  } while (!((x == maze.xs) && (y == maze.ys)));
}

void
prepare_maze(void) {
  SAF_randomSeed(player.depth);
  seed_rnd(default_rng, player.depth);
  init_maze();
  build_maze();
  finish_maze();
}

uint8_t
has_stair_down(uint8_t x, uint8_t y) {
  return ((player.depth != 15) && (x == maze.xf) && (y == maze.yf));
}

uint8_t
has_stair_up(uint8_t x, uint8_t y) {
  return ((x == maze.xs) && (y == maze.ys));
}

uint8_t
calculate_range(uint8_t tile_width) {
  return (SAF_SCREEN_WIDTH / tile_width) / 2 + 1;
}

void
uncover_discovered_places(void) {

  uint8_t range = calculate_range(maze.tile_width);

  for (int8_t y = -1; y <= 1; y++) {
    for (int8_t x = -1; x <= 1; x++) {
      set_pixel(
          maze.known_parts,
          player.puppet.x + x,
          player.puppet.y + y,
          get_pixel(
            maze.underlying_structure,
            player.puppet.x + x,
            player.puppet.y + y));
    }
  }

  /* Further distance, see only straight, not diagonal */
  for (uint8_t y = 2; y <= range; y++) {
    if (!is_wall(maze.underlying_structure, player.puppet.x, player.puppet.y + (y - 1))) {
      set_pixel(
          maze.known_parts,
          player.puppet.x,
          player.puppet.y + y,
          get_pixel(
            maze.underlying_structure,
            player.puppet.x,
            player.puppet.y + y));
    }
    else {
      break;
    }
  }
  for (uint8_t y = 2; y <= range; y++) {
    if (!is_wall(maze.underlying_structure, player.puppet.x, player.puppet.y - (y - 1))) {
      set_pixel(
          maze.known_parts,
          player.puppet.x,
          player.puppet.y - y,
          get_pixel(
            maze.underlying_structure,
            player.puppet.x,
            player.puppet.y - y));
    }
    else {
      break;
    }
  }
  for (uint8_t x = 2; x <= range; x++) {
    if (!is_wall(maze.underlying_structure, player.puppet.x + (x - 1), player.puppet.y)) {
      set_pixel(
          maze.known_parts,
          player.puppet.x + x,
          player.puppet.y,
          get_pixel(
            maze.underlying_structure,
            player.puppet.x + x,
            player.puppet.y));
    }
    else {
      break;
    }
  }
  for (uint8_t x = 2; x <= range; x++) {
    if (!is_wall(maze.underlying_structure, player.puppet.x - (x - 1), player.puppet.y)) {
      set_pixel(
          maze.known_parts,
          player.puppet.x - x,
          player.puppet.y,
          get_pixel(
            maze.underlying_structure,
            player.puppet.x - x,
            player.puppet.y));
    }
    else {
      break;
    }
  }
}

uint8_t
foe_in_sight(uint8_t i) {

  uint8_t range = calculate_range(maze.tile_width);

  /* Immediate vicinity */
  for (int8_t y = -1; y <= 1; y++) {
    for (int8_t x = -1; x <= 1; x++) {
      if ((foes[i].puppet.x == player.puppet.x + x) && (foes[i].puppet.y == player.puppet.y + y)) {
        return 1;
      }
    }
  }

  /* Further distance, see only straight, not diagonal,
   * and not across walls
   * */
  for (uint8_t y = 2; y <= range; y++) {
    if (!is_wall(maze.underlying_structure, player.puppet.x, player.puppet.y + (y - 1))) {
      if ((foes[i].puppet.x == player.puppet.x + 0) && (foes[i].puppet.y == player.puppet.y + y)) {
        return 1;
      }
    }
    else {
      break;
    }
  }
  for (uint8_t y = 2; y <= range; y++) {
    if (!is_wall(maze.underlying_structure, player.puppet.x, player.puppet.y - (y - 1))) {
      if ((foes[i].puppet.x == player.puppet.x + 0) && (foes[i].puppet.y == player.puppet.y - y)) {
        return 1;
      }
    }
    else {
      break;
    }
  }
  for (uint8_t x = 2; x <= range; x++) {
    if (!is_wall(maze.underlying_structure, player.puppet.x + (x - 1), player.puppet.y)) {
      if ((foes[i].puppet.x == player.puppet.x + x) && (foes[i].puppet.y == player.puppet.y + 0)) {
        return 1;
      }
    }
    else {
      break;
    }
  }
  for (uint8_t x = 2; x <= range; x++) {
    if (!is_wall(maze.underlying_structure, player.puppet.x - (x - 1), player.puppet.y)) {
      if ((foes[i].puppet.x == player.puppet.x - x) && (foes[i].puppet.y == player.puppet.y + 0)) {
        return 1;
      }
    }
    else {
      break;
    }
  }
  return 0;
}

void
teleport_player(void) {
  do {
    player.puppet.x = 2 + rnd(default_rng) % (maze.width - 4);
    player.puppet.y = 2 + rnd(default_rng) % (maze.height - 4);
  } while (!is_tile_empty_floor(player.puppet.x, player.puppet.y));
  player.puppet.closeup.x = rnd(default_rng) % maze.tile_width;
  player.puppet.closeup.y = rnd(default_rng) % maze.tile_height;
}

void
draw_overview_zzZ(int8_t x, int8_t y) {
  drawOutlinedCenteredTextTighter(
      "z",
      x + 6 * SAF_cos(SAF_frame() * 4 % 256) / 128,
      y - 2 - 6 * SAF_sin(SAF_frame() * 4 % 256) / 128,
      SAF_COLOR_GRAY,
      SAF_COLOR_GRAY_DARK);
  drawOutlinedCenteredTextTighter(
      "z",
      x + 6 * SAF_cos((43 + SAF_frame()) * 4 % 256) / 128,
      y - 2 - 6 * SAF_sin((43 + SAF_frame()) * 4 % 256) / 128,
      SAF_COLOR_GRAY,
      SAF_COLOR_GRAY_DARK);
  drawOutlinedCenteredTextTighter(
      "z",
      x + 6 * SAF_cos((86 + SAF_frame()) * 4 % 256) / 128,
      y - 2 - 6 * SAF_sin((86 + SAF_frame()) * 4 % 256) / 128,
      SAF_COLOR_GRAY,
      SAF_COLOR_GRAY_DARK);
}

#define MAX_FOE_MOVE_TRIES (32)

void
move_distant_foe(uint8_t i) {
  uint8_t n_tries = 0;
  direction_t d = 0;
  do {
    d = rnd(default_rng) % 4;
  } while (
      (++n_tries < MAX_FOE_MOVE_TRIES)
      && is_wall(maze.underlying_structure, foes[i].puppet.x + moves_table[d].dx, foes[i].puppet.y + moves_table[d].dy));
  if (n_tries < MAX_FOE_MOVE_TRIES) {
    foes[i].puppet.x = foes[i].puppet.x + moves_table[d].dx;
    foes[i].puppet.y = foes[i].puppet.y + moves_table[d].dy;
    foes[i].puppet.closeup.x = 32 - moves_table[d].dx * maze.tile_width  / 2;
    foes[i].puppet.closeup.y = 32 - moves_table[d].dy * maze.tile_height / 2;
  }
}

#define FOE_STILLNESS (250)
void
move_distant_foes(void) {
  for (uint8_t i = N_WORMS; i < N_FOES; i++) {
    if (
        foes[i].puppet.hit_points_left
        && !foe_in_sight(i) &&
        (rnd(default_rng) > FOE_STILLNESS)
       ) {
      move_distant_foe(i);
    }
  }
}
#define MAX_IDLE_FRAMES_OVERVIEW (5 * SAF_FPS)
uint8_t
overview_loop(void) {
  static uint16_t idle_frames = 0;

  if ((SAF_buttonPressed(SAF_BUTTON_LEFT) % 2) == 1) {
    maze.last_move_frame = SAF_frame();
    player.puppet.direction = LEFT;
    player.puppet.closeup.y = maze.tile_height / 2;
    if (!is_wall(maze.underlying_structure, player.puppet.x - 1, player.puppet.y)) {
      player.puppet.x -= 1;
      player.puppet.closeup.x = maze.tile_width - CLOSEUP_PLAYER_SPEED;
    } else {
      player.puppet.closeup.x = 0;
    }
  }
  if ((SAF_buttonPressed(SAF_BUTTON_RIGHT) % 2) == 1) {
    maze.last_move_frame = SAF_frame();
    player.puppet.direction = RIGHT;
    player.puppet.closeup.y = maze.tile_height / 2;
    if (!is_wall(maze.underlying_structure, player.puppet.x + 1, player.puppet.y)) {
      player.puppet.x += 1;
      player.puppet.closeup.x = 0;
    } else {
      player.puppet.closeup.x = maze.tile_width - CLOSEUP_PLAYER_SPEED;
    }
  }
  if ((SAF_buttonPressed(SAF_BUTTON_UP) % 2) == 1) {
    maze.last_move_frame = SAF_frame();
    player.puppet.direction = UP;
    player.puppet.closeup.x = maze.tile_width / 2;
    if (!is_wall(maze.underlying_structure, player.puppet.x, player.puppet.y - 1)) {
      player.puppet.y -= 1;
      player.puppet.closeup.y = maze.tile_height - CLOSEUP_PLAYER_SPEED;
    } else {
      player.puppet.closeup.y = 0;
    }
  }
  if ((SAF_buttonPressed(SAF_BUTTON_DOWN) % 2) == 1) {
    maze.last_move_frame = SAF_frame();
    player.puppet.direction = DOWN;
    player.puppet.closeup.x = maze.tile_width / 2;
    if (!is_wall(maze.underlying_structure, player.puppet.x, player.puppet.y + 1)) {
      player.puppet.y += 1;
      player.puppet.closeup.y = 0;
    } else {
      player.puppet.closeup.y =  maze.tile_height - CLOSEUP_PLAYER_SPEED;
    }
  }

  if (SAF_buttonJustPressed(SAF_BUTTON_C)) {
    if (player.has_ring) {
      teleport_player();
      player.just_teleported = 5;
      return 1;
    }
  } else {
    if (
        SAF_buttonJustPressed(SAF_BUTTON_B)
        && ((player.level_maps_carried & (1 << player.depth)) || player.has_amulet)) {
      player.wants_level_map = !player.wants_level_map;
    }
    if (SAF_buttonJustPressed(SAF_BUTTON_A)) {
      fade_to(closeup_loop);
    }
  }

  uncover_discovered_places();
  move_distant_foes();
  draw_maze();
  draw_life_indicator();
  if (idle_frames < MAX_IDLE_FRAMES_OVERVIEW) {
    draw_other_indicators();
  }
  // player.puppet.hit_points_left += (player.puppet.hit_points_left < PLAYER_HIT_POINTS);

  if (maze.info[player.puppet.x][player.puppet.y].contained_item != ITEM_NOTHING) {
    player.is_interrupted = 1;
    fade_to(closeup_loop);
  }
  else {
    for (uint8_t i = 0; i < N_FOES; i++) {
      if (
             foes[i].puppet.hit_points_left
             && foe_in_sight(i)
          ) {
        idle_frames = 0;
        player.is_interrupted = 1;
        fade_to(closeup_loop);
      }
    }
  }

  if (!anyButtonPressed()) {
    idle_frames += (idle_frames < MAX_IDLE_FRAMES_OVERVIEW);
  } else {
    idle_frames = 0;
  }

  if (idle_frames >= MAX_IDLE_FRAMES_OVERVIEW) {
    player.puppet.hit_points_left += (player.puppet.hit_points_left < PLAYER_HIT_POINTS);
    draw_overview_zzZ((SAF_SCREEN_WIDTH - maze.width) / 2 + player.puppet.x, (SAF_SCREEN_HEIGHT - maze.height) / 2 + player.puppet.y);
  }


  return 1;
}

#define BURIAL_SCORE (100)
void
bury_corpse(uint8_t tx, uint8_t ty) {
  if (maze.info[tx][ty].contained_item == ITEM_CORPSE) {
    maze.info[tx][ty].contained_item = ITEM_NOTHING;
    player.corpses_buried += (player.corpses_buried < 255);
    increase_score_by(BURIAL_SCORE);
  }
}
