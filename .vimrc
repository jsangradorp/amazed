" Recommended $HOME/.vimrc settings:
"
" source $VIMRUNTIME/defaults.vim
" :set autoindent expandtab tabstop=2 shiftwidth=2
" :set exrc
" :nmap <C-n> :bnext<C-m>
" :nmap <C-p> :bprevious<C-m>
" :set hidden

:autocmd BufWritePost * silent !ctags --exclude=public --exclude=build -R .
