/*
   A-Mazed

   Copyright (C) 2023  Julio Sangrador-Paton

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
   */

uint8_t
is_valid_tile_for_object(uint8_t x, uint8_t y) {
  return
    is_tile_empty_floor(x, y)
    && (maze.info[x][y].area > 0);
}

#define OBJECT_ANIM_FRAMES (16)
#define EXTRA_OBJECT_ANIM_FRAMES (1 * SAF_FPS)
uint8_t *object_image;
uint8_t *object_mask;
uint8_t object_border_color;
uint8_t object_fill_color;
char *object_text;

uint8_t
object_found_loop(void) {
  static uint8_t frames_showing_object_animation = OBJECT_ANIM_FRAMES + EXTRA_OBJECT_ANIM_FRAMES;

  uint8_t scaling_table[4] = {
    SAF_TRANSFORM_SCALE_4,
    SAF_TRANSFORM_SCALE_3,
    SAF_TRANSFORM_SCALE_2,
    SAF_TRANSFORM_NONE,
  };

  /* Show object found */
  frames_showing_object_animation--;
  int8_t frames = frames_showing_object_animation - EXTRA_OBJECT_ANIM_FRAMES;
  if (frames < 0) {
    frames = 0;
  }
  SAF_clearScreen(SAF_frame() % 2 ? FLOOR_COLOR : FLOOR_HIGHLIGHT_COLOR);
  SAF_drawText((object_image == torch_image) ? "you take" : "you found", 6, 2, SAF_COLOR_GRAY, 1);
  SAF_drawText(object_text, 4, 57, SAF_COLOR_GRAY, 1);
  SAF_drawImage1Bit(
      object_image,
      (SAF_SCREEN_WIDTH - (4 - frames / (OBJECT_ANIM_FRAMES / 4)) * object_image[0]) / 2,
      (SAF_SCREEN_HEIGHT - (4 - frames / (OBJECT_ANIM_FRAMES / 4)) * object_image[1]) / 2,
      object_mask,
      object_border_color,
      object_fill_color,
      scaling_table[frames / (OBJECT_ANIM_FRAMES / 4)]
      );
  if (frames_showing_object_animation == 0) {
    maze.info[player.puppet.x][player.puppet.y].contained_item = ITEM_NOTHING;
    player.puppet.closeup.x = (maze.tile_width - WALK_SEQUENCE_WIDTH) / 2;
    player.puppet.closeup.y = (maze.tile_height - WALK_SEQUENCE_HEIGHT) / 2;
    fade_to_loop(closeup_loop);

    frames_showing_object_animation = OBJECT_ANIM_FRAMES + EXTRA_OBJECT_ANIM_FRAMES;
  }
  return 1;
}

void
go_to_object_found_loop(uint8_t *image, uint8_t *mask, uint8_t border_color, uint8_t fill_color, char *text) {

  object_image = image;
  object_mask = mask;
  object_border_color = border_color;
  object_fill_color = fill_color;
  object_text = text;

  current_loop = object_found_loop;
}

uint8_t
is_valid_tile_for_dolmen(uint8_t x, uint8_t y) {
  return
    is_valid_tile_for_object(x, y)
    && (
        (is_wall(maze.underlying_structure, x - 1, y) +
         is_wall(maze.underlying_structure, x + 1, y) +
         is_wall(maze.underlying_structure, x, y - 1) +
         is_wall(maze.underlying_structure, x, y + 1)
         ) == 3);
}

uint8_t
is_valid_tile_for_piece(uint8_t x, uint8_t y) {
  return
    is_valid_tile_for_object(x, y)
    && ((player.depth != N_LEVELS)
        || (is_wall(maze.underlying_structure, x - 1, y) +
         is_wall(maze.underlying_structure, x + 1, y) +
         is_wall(maze.underlying_structure, x, y - 1) +
         is_wall(maze.underlying_structure, x, y + 1)
          > 0));
}

uint8_t (*tile_condition[9])(uint8_t, uint8_t) = {
  0,
  0,
  0,
  is_valid_tile_for_object,
  is_valid_tile_for_object,
  is_valid_tile_for_piece,
  is_valid_tile_for_object,
  is_valid_tile_for_dolmen,
  is_valid_tile_for_object,
};

void
add_object(uint8_t object_type, uint8_t *x, uint8_t *y) {
  do {
    *x = rnd(default_rng) % maze.width;
    *y = rnd(default_rng) % maze.height;
  } while (!tile_condition[object_type](*x, *y));
  maze.info[*x][*y].contained_item = object_type;
}

void
add_objects(void) {
  uint8_t x;
  uint8_t y;

  if (player.depth != N_LEVELS) {
    add_object(ITEM_DOLMEN, &x, &y);
    for (direction_t d = DOWN; d <= RIGHT; d++) {
      if (!is_wall(maze.underlying_structure, x + moves_table[d].dx, y + moves_table[d].dy)) {
        maze.dolmen_direction = d;
      }
    }
  }

  if ((player.depth <= DEEPEST_LEVEL_WITH_MAP) && !(player.level_maps_carried & (1 << player.depth))) {
    add_object(ITEM_LEVEL_MAP, &x, &y);
  }

  if ((player.depth == AMULET_LEVEL) && !(player.has_amulet)) {
    add_object(ITEM_AMULET, &x, &y);
  }

  if ((player.depth == RING_LEVEL) && !(player.has_ring)) {
    add_object(ITEM_RING, &x, &y);
  }

  if (((player.depth != N_LEVELS) || petagolon.is_dead) && !(player.pieces_carried & (1 << player.depth))) {
    add_object(ITEM_MOSAIC_PIECE, &x, &y);
  }

  if (
      (player.depth >= FIRST_LEVEL_WITH_CORPSES)
      && (player.depth <= DEEPEST_LEVEL_WITH_CORPSES)
      && (player.corpses_buried < MAX_CORPSES)
      ) {
    for (uint8_t i = 0; i < (N_LEVELS - player.depth) / 2; i++) {
      if ((player.corpses_buried + i) <= MAX_CORPSES) {
        add_object(ITEM_CORPSE, &x, &y);
      } else {
        break;
      }
    }
  }

  /* Ornaments */
  if (player.depth != N_LEVELS) {
    uint8_t x;
    uint8_t y;

    rng_state_t st;
    seed_rnd(&st, 20);

    uint16_t max_ornaments_in_level = maze.width * maze.height / 8;
    for (uint16_t i = 0; i < max_ornaments_in_level; i++) {
      x = rnd(&st) % maze.width;
      y = rnd(&st) % maze.height;
      if (is_tile_empty_floor(x, y)) {
        maze.info[x][y].ornament = 1 + SAF_random() % (ORNAMENTS_END - ORNAMENT_NOTHING - 1);
      }
    }
  }
}

