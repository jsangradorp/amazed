
#define N_WORMS (4)
#define N_FOES (N_WORMS + 63)
              /* ^ 31 bats + 15 ants + 7 spiders + 7 scorpios + 3 orcs per level + 4 worms*/

#ifdef NO_FOES
#undef N_FOES
#define N_FOES (1)
//#define N_FOES (N_WORMS + 1)
#endif

rng_state_t foe_generation_rng;

typedef enum {
  FOE_BAT,
  FOE_GIANT_ANT,
  FOE_GIANT_SPIDER,
  FOE_GIANT_SCORPION,
  FOE_ORC,
  FOE_WORM,
} foe_type_id_t;

typedef struct {
  foe_type_id_t type;
  uint8_t max_in_any_level;
  uint8_t startup_images_count;
  uint8_t images_count;
  uint8_t **images;
  uint8_t **masks;
  uint8_t hit_points;
  uint8_t damage;
  uint8_t scoring;
  uint8_t frames_resting;
  uint8_t min_area;
  uint8_t slow_down_factor;
  uint8_t speed;
  uint8_t border_color;
  uint8_t fill_color;
} foe_type_t;

foe_type_t foe_types[FOE_WORM - FOE_BAT + 1];

uint8_t remaining_foes[N_LEVELS + 1][FOE_ORC - FOE_BAT + 1];

void
populate_all_levels(void) {
  for (uint8_t i = 0; i <= N_LEVELS; i++) {
    for (foe_type_id_t type = FOE_BAT; type < FOE_ORC; type++) {
      remaining_foes[i][type] = (i == N_LEVELS) ? 0 : foe_types[type].max_in_any_level - i / (type + 1);
    }
    remaining_foes[i][FOE_ORC] = (i == N_LEVELS) ? 0 : i * foe_types[FOE_ORC].max_in_any_level / (N_LEVELS - 1);
  }
}

void
init_foe_rng(void) {
  seed_rnd(&foe_generation_rng, 2);
}

void
init_foe_types(void) {
  foe_types[FOE_BAT] = (foe_type_t){
    .type = FOE_BAT,
    .startup_images_count = 0,
    .images_count = BAT_IMAGES,
    .images = bat_images,
    .masks = bat_images_masks,
    .max_in_any_level = 31, /* NO SAVE */
    .hit_points = 5,
    .damage = 3,
    .scoring = 2,
    .frames_resting = 3,
    .min_area = 0,
    .slow_down_factor = 1,
    .speed = 1,
    .border_color = SAF_COLOR_BLACK,
    .fill_color = SAF_COLOR_WHITE,
  };
  foe_types[FOE_GIANT_ANT] = (foe_type_t){
    .type = FOE_GIANT_ANT,
    .startup_images_count = 0,
    .images_count = ANT_IMAGES,
    .images = ant_images,
    .masks = ant_images_masks,
    .max_in_any_level = 15, /* 4 bits max for save */
    .hit_points = 20,
    .damage = 4,
    .scoring = 5,
    .frames_resting = 6,
    .min_area = 1,
    .slow_down_factor = 1,
    .speed = 1,
    .border_color = SAF_COLOR_RED_DARK,
    .fill_color = SAF_COLOR_BLACK,
  };
  foe_types[FOE_GIANT_SPIDER] = (foe_type_t){
    .type = FOE_GIANT_SPIDER,
    .startup_images_count = 0,
    .images_count = SPIDER_IMAGES,
    .images = spider_images,
    .masks = spider_images_masks,
    .max_in_any_level = 7, /* 3 bits max for save */
    .hit_points = 50,
    .damage = 6,
    .scoring = 10,
    .frames_resting = 10,
    .min_area = 2,
    .slow_down_factor = 1,
    .speed = 1,
    .border_color = SAF_COLOR_GREEN_DARK,
    .fill_color = SAF_COLOR_WHITE,
  };
  foe_types[FOE_GIANT_SCORPION] = (foe_type_t){
    .type = FOE_GIANT_SCORPION,
    .startup_images_count = 0,
    .images_count = SCORPIO_IMAGES,
    .images = scorpio_images,
    .masks = scorpio_masks,
    .max_in_any_level = 7, /* 3 bits max for save */
    .hit_points = 40,
    .damage = 8,
    .scoring = 15,
    .frames_resting = SAF_FPS / 2,
    .min_area = 2,
    .slow_down_factor = 1,
    .speed = 1,
    .border_color = 254,
    .fill_color = SAF_COLOR_RED_DARK,
  };
  foe_types[FOE_ORC] = (foe_type_t){
    .type = FOE_ORC,
    .startup_images_count = 0,
    .images_count = N_ORC_STEPS,
    .images = orc_sequence,
    .masks = orc_sequence_mask,
    .max_in_any_level = 3, /* 2 bits max for save */
    .hit_points = 100,
    .damage = 10,
    .scoring = 50,
    .frames_resting = 15,
    .min_area = 3,
    .slow_down_factor = 1,
    .speed = 1,
    .border_color = SAF_COLOR_WHITE,
    .fill_color = SAF_COLOR_BLACK,
  };
  foe_types[FOE_WORM] = (foe_type_t){
    .type = FOE_WORM,
    .startup_images_count = WORM_STARTUP_IMAGES,
    .images_count = WORM_IMAGES,
    .images = worm_images,
    .masks = worm_images,
    .max_in_any_level = 0, /* Random, unlimited */
    .hit_points = 2,
    .damage = 1,
    .scoring = 1,
    .frames_resting = 5,
    .min_area = 0,
    .slow_down_factor = 3,
    .speed = 1,
    .border_color = SAF_COLOR_RGB(96, 64, 32),
    .fill_color = SAF_COLOR_BLACK,
  };
}

typedef struct {
  foe_type_id_t type;
  int8_t dx;
  int8_t dy;
  uint8_t is_colliding;
  uint8_t frames_resting;
  puppet_t puppet;
} foe_t;

foe_t foes[N_FOES];

uint8_t
is_valid_tile_for_foe(uint8_t x, uint8_t y, foe_type_id_t foe_type, uint8_t last_foe, uint8_t range) {
  if(
      is_tile_empty_floor(x, y)
    && !((player.puppet.x == x) && (player.puppet.y == y))
    && (maze.info[x][y].area >= foe_types[foe_type].min_area)
    && !((foe_type == FOE_WORM) && (((x - player.puppet.x) > range) || ((player.puppet.x - x) > range) || ((y - player.puppet.y) > range) || ((player.puppet.y - y) > range)))
    ) {
    for (uint8_t foe = 0; foe < last_foe; foe++) {
      if (foes[foe].puppet.hit_points_left && (foes[foe].puppet.x == x) && (foes[foe].puppet.y == y)) {
        return 0;
      }
    }
    return 1;
  } else {
    return 0;
  }
}

#define MAX_TRIES (64)

void
add_foe(foe_type_id_t type, uint8_t *next_foe) {
  uint8_t x;
  uint8_t y;
  uint8_t current_image_index;

  uint8_t tries = 0;
  uint8_t range = calculate_range(maze.tile_width);

  do {
    x = rnd(&foe_generation_rng) % maze.width;
    y = rnd(&foe_generation_rng) % maze.height;
    /* v Only random starting image if there's no startup sequence */
    current_image_index = (foe_types[type].startup_images_count == 0) * rnd(&foe_generation_rng) % foe_types[type].images_count;
  } while (!is_valid_tile_for_foe(x, y, type, *next_foe, range) && (++tries < MAX_TRIES));

  if (is_valid_tile_for_foe(x, y, type, *next_foe, range)) {
    foes[*next_foe] = (foe_t){
      .type = type,
        .puppet.x = x,
        .puppet.y = y,
        .puppet.closeup.current_image_index = current_image_index,
        .puppet.closeup.x = maze.tile_width / 2,
        .puppet.closeup.y = maze.tile_height / 2,
        .puppet.hit_points_left = foe_types[type].hit_points,
        .frames_resting = foe_types[type].frames_resting,
        .puppet.was_hit = 0,
        .puppet.direction = DOWN,
    };
    *next_foe = *next_foe + 1;
  }
}

void
add_foes(void) {
#ifdef NO_FOES
  return;
#else
  for (uint8_t i = 0; i < N_FOES; i++) {
    foes[i].puppet.hit_points_left = 0;
    foes[i].puppet.x = foes[i].puppet.y = 0;
  }
  if (player.depth == N_LEVELS) { /* Only petagolon at deepest level */
    return;
  }

  uint8_t next_foe = N_WORMS;

  for (foe_type_id_t ttype = FOE_ORC + 1; ttype > FOE_BAT; ttype--) {
    foe_type_id_t type = ttype - 1; /* Hack around unsignedness of enum */

    uint8_t n_foes = remaining_foes[player.depth][type];
    for (uint8_t foe = 0; foe < n_foes; foe++) {
      add_foe(type, &next_foe);
    }
  }
#endif
}

void
add_worm(void) {
#ifdef NO_FOES
  return;
#else
  static uint8_t next_worm = 0;

  if (player.depth == N_LEVELS) { /* Only petagolon at deepest level */
    return;
  }

  uint8_t range = calculate_range(maze.tile_width);
  for (uint8_t i = 0; i < N_WORMS; i++) {
    if (
        (foes[next_worm].puppet.hit_points_left == 0) ||
        (
         ((foes[next_worm].puppet.x - player.puppet.x) > range) ||
         ((player.puppet.x - foes[next_worm].puppet.x) > range) ||
         ((foes[next_worm].puppet.y - player.puppet.y) > range) ||
         ((player.puppet.y - foes[next_worm].puppet.y) > range))
        ) {
      add_foe(FOE_WORM, &next_worm);
      next_worm = next_worm % N_WORMS;
      break;
    }
    else {
      next_worm = (next_worm + 1) % N_WORMS;
    }
  }
#endif
}

#undef MAX_TRIES
