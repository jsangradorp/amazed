#define PLAYER_HIT_POINTS (100)

void
reset_player(void) {
    player.score = 0;
    player.just_teleported = 0;
    player.wants_level_map = 0;
    player.is_interrupted = 0;
    player.has_amulet = 0;
    player.has_ring = 0;
    player.has_torch = 0;
    player.is_slashing = 0;
    player.puppet.hit_points_left = PLAYER_HIT_POINTS;
    player.puppet.was_hit = 0;
    player.pieces_carried = 0;
    player.level_maps_carried = 0;
    player.corpses_buried = 0;
    player.frames_still = 0;
#ifdef DEBUG
    player.depth = 9;
    player.pieces_carried = (1 << 16) - 2; // - (1 << N_LEVELS);
    player.has_ring = 1;
    player.has_amulet = 1;
    player.has_torch = 1;
    //player.corpses_buried = 250;
    player.level_maps_carried = (1 << 16) - 2;
#else
    player.depth = 0; /* It will be immediately increased in descend_loop */
#endif
}

void
increase_score_by(uint16_t amount) {
  if ((0xffff - player.score) >= amount) {
    player.score += amount;
  } else {
    player.score = 0xffff;
  }
  if (player.score > highest_score) {
    highest_score = player.score;
  }
}

uint8_t
is_torch_lit(void) {
  return (player.depth > DEEPEST_LEVEL_WITH_TORCHES) && (player.depth < N_LEVELS);
}

uint8_t
count_pieces_carried(void) {
  uint8_t n = 0;
  for (uint8_t bit = 0; bit < 16; bit++) {
    if (player.pieces_carried & (1 << bit)) {
      ++n;
    }
  }
  return n;
}

